-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: 13-Jul-2018 às 19:52
-- Versão do servidor: 5.7.22
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aac`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `especialistas`
--

CREATE TABLE `especialistas` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `respostas`
--

CREATE TABLE `respostas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pergunta` text COLLATE utf8_unicode_ci NOT NULL,
  `especialista` int(10) UNSIGNED DEFAULT NULL,
  `resposta` text COLLATE utf8_unicode_ci NOT NULL,
  `aprovado` tinyint(1) NOT NULL DEFAULT '0',
  `respostas_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `avaliacao_clientes` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `respostas_avaliacoes`
--

CREATE TABLE `respostas_avaliacoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `cadastro_id` int(10) UNSIGNED NOT NULL,
  `resposta_id` int(10) UNSIGNED NOT NULL,
  `avaliacao` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `respostas_categorias`
--

CREATE TABLE `respostas_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `resposta_em_destaque`
--

CREATE TABLE `resposta_em_destaque` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pergunta` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `resposta_em_destaque`
--

INSERT INTO `resposta_em_destaque` (`id`, `imagem`, `pergunta`, `created_at`, `updated_at`) VALUES
(1, '', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `especialistas`
--
ALTER TABLE `especialistas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `especialistas_email_unique` (`email`);

--
-- Indexes for table `respostas`
--
ALTER TABLE `respostas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `respostas_especialista_foreign` (`especialista`),
  ADD KEY `respostas_respostas_categoria_id_foreign` (`respostas_categoria_id`);

--
-- Indexes for table `respostas_avaliacoes`
--
ALTER TABLE `respostas_avaliacoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `respostas_categorias`
--
ALTER TABLE `respostas_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resposta_em_destaque`
--
ALTER TABLE `resposta_em_destaque`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resposta_em_destaque_pergunta_foreign` (`pergunta`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `especialistas`
--
ALTER TABLE `especialistas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `respostas`
--
ALTER TABLE `respostas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `respostas_avaliacoes`
--
ALTER TABLE `respostas_avaliacoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `respostas_categorias`
--
ALTER TABLE `respostas_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resposta_em_destaque`
--
ALTER TABLE `resposta_em_destaque`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `respostas`
--
ALTER TABLE `respostas`
  ADD CONSTRAINT `respostas_especialista_foreign` FOREIGN KEY (`especialista`) REFERENCES `especialistas` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `respostas_respostas_categoria_id_foreign` FOREIGN KEY (`respostas_categoria_id`) REFERENCES `respostas_categorias` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `resposta_em_destaque`
--
ALTER TABLE `resposta_em_destaque`
  ADD CONSTRAINT `resposta_em_destaque_pergunta_foreign` FOREIGN KEY (`pergunta`) REFERENCES `respostas` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
