-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Tempo de geração: 26/06/2017 às 17:21
-- Versão do servidor: 5.7.17
-- Versão do PHP: 7.0.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `audicaoasclaras`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `cabecalho`
--

CREATE TABLE `cabecalho` (
  `id` int(10) UNSIGNED NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `cabecalho`
--

INSERT INTO `cabecalho` (`id`, `frase`, `imagem_1`, `imagem_2`, `imagem_3`, `imagem_4`, `imagem_5`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '', '', '', NULL, NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `cabecalho`
--
ALTER TABLE `cabecalho`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `cabecalho`
--
ALTER TABLE `cabecalho`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
