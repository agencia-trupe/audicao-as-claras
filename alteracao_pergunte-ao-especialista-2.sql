-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Tempo de geração: 23/07/2018 às 19:34
-- Versão do servidor: 8.0.2-dmr
-- Versão do PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `aac`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `aulas`
--

CREATE TABLE `aulas` (
  `id` int(10) UNSIGNED NOT NULL,
  `aulas_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada` text COLLATE utf8_unicode_ci NOT NULL,
  `informacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `aulas_categorias`
--

CREATE TABLE `aulas_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `aulas_downloads`
--

CREATE TABLE `aulas_downloads` (
  `id` int(10) UNSIGNED NOT NULL,
  `aula_id` int(10) UNSIGNED DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `avisos`
--

CREATE TABLE `avisos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `botao_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `botao_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `especialistas`
--

CREATE TABLE `especialistas` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profissao_cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cep` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `complemento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome_exibicao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_nome_fantasia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_razao_social` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_area_de_atuacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_contato_responsavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_cep` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_numero` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_complemento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_bairro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_cidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_uf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fornecedor_website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aprovado` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `password_resets_especialistas`
--

CREATE TABLE `password_resets_especialistas` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `respostas`
--

CREATE TABLE `respostas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pergunta` text COLLATE utf8_unicode_ci NOT NULL,
  `especialista` int(10) UNSIGNED DEFAULT NULL,
  `resposta` text COLLATE utf8_unicode_ci NOT NULL,
  `aprovado` tinyint(1) NOT NULL DEFAULT '0',
  `respostas_categoria_id` int(10) UNSIGNED DEFAULT NULL,
  `avaliacao_clientes` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `respostas_avaliacoes`
--

CREATE TABLE `respostas_avaliacoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `cadastro_id` int(10) UNSIGNED NOT NULL,
  `resposta_id` int(10) UNSIGNED NOT NULL,
  `avaliacao` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `respostas_categorias`
--

CREATE TABLE `respostas_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `resposta_em_destaque`
--

CREATE TABLE `resposta_em_destaque` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pergunta` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `resposta_em_destaque`
--

INSERT INTO `resposta_em_destaque` (`id`, `imagem`, `pergunta`, `created_at`, `updated_at`) VALUES
(1, '', NULL, NULL, NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `aulas`
--
ALTER TABLE `aulas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aulas_aulas_categoria_id_foreign` (`aulas_categoria_id`);

--
-- Índices de tabela `aulas_categorias`
--
ALTER TABLE `aulas_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `aulas_downloads`
--
ALTER TABLE `aulas_downloads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aulas_downloads_aula_id_foreign` (`aula_id`);

--
-- Índices de tabela `avisos`
--
ALTER TABLE `avisos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `especialistas`
--
ALTER TABLE `especialistas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `especialistas_email_unique` (`email`);

--
-- Índices de tabela `password_resets_especialistas`
--
ALTER TABLE `password_resets_especialistas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_especialistas_email_index` (`email`),
  ADD KEY `password_resets_especialistas_token_index` (`token`);

--
-- Índices de tabela `respostas`
--
ALTER TABLE `respostas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `respostas_especialista_foreign` (`especialista`),
  ADD KEY `respostas_respostas_categoria_id_foreign` (`respostas_categoria_id`);

--
-- Índices de tabela `respostas_avaliacoes`
--
ALTER TABLE `respostas_avaliacoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `respostas_categorias`
--
ALTER TABLE `respostas_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `resposta_em_destaque`
--
ALTER TABLE `resposta_em_destaque`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resposta_em_destaque_pergunta_foreign` (`pergunta`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `aulas`
--
ALTER TABLE `aulas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `aulas_categorias`
--
ALTER TABLE `aulas_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `aulas_downloads`
--
ALTER TABLE `aulas_downloads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `avisos`
--
ALTER TABLE `avisos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `especialistas`
--
ALTER TABLE `especialistas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `password_resets_especialistas`
--
ALTER TABLE `password_resets_especialistas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `respostas`
--
ALTER TABLE `respostas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `respostas_avaliacoes`
--
ALTER TABLE `respostas_avaliacoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `respostas_categorias`
--
ALTER TABLE `respostas_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `resposta_em_destaque`
--
ALTER TABLE `resposta_em_destaque`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `aulas`
--
ALTER TABLE `aulas`
  ADD CONSTRAINT `aulas_aulas_categoria_id_foreign` FOREIGN KEY (`aulas_categoria_id`) REFERENCES `aulas_categorias` (`id`) ON DELETE SET NULL;

--
-- Restrições para tabelas `aulas_downloads`
--
ALTER TABLE `aulas_downloads`
  ADD CONSTRAINT `aulas_downloads_aula_id_foreign` FOREIGN KEY (`aula_id`) REFERENCES `aulas` (`id`) ON DELETE CASCADE;

--
-- Restrições para tabelas `respostas`
--
ALTER TABLE `respostas`
  ADD CONSTRAINT `respostas_especialista_foreign` FOREIGN KEY (`especialista`) REFERENCES `especialistas` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `respostas_respostas_categoria_id_foreign` FOREIGN KEY (`respostas_categoria_id`) REFERENCES `respostas_categorias` (`id`) ON DELETE SET NULL;

--
-- Restrições para tabelas `resposta_em_destaque`
--
ALTER TABLE `resposta_em_destaque`
  ADD CONSTRAINT `resposta_em_destaque_pergunta_foreign` FOREIGN KEY (`pergunta`) REFERENCES `respostas` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
