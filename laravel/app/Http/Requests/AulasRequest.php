<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AulasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'aulas_categoria_id' => 'required',
            'titulo' => 'required',
            'capa' => 'required|image',
            'chamada' => 'required',
            'informacoes' => 'required',
            'video_tipo' => 'required',
            'video_codigo' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
