<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChamadasHomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'chamada_1_texto' => 'required',
            'chamada_1_botao' => 'required',
            'chamada_1_link' => 'required',
            'chamada_2_texto' => 'required',
            'chamada_2_botao' => 'required',
            'chamada_2_link' => 'required',
        ];
    }
}
