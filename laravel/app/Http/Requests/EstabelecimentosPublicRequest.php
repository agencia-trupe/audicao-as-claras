<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EstabelecimentosPublicRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'            => 'required',
            'email'           => 'required|email|max:255|unique:estabelecimentos',
            'razao_social'    => 'required',
            'cnpj'            => 'required',
            'area_de_atuacao' => 'required',
            'telefone'        => 'required',
            'responsavel'     => 'required',
            'cep'             => 'required',
            'endereco'        => 'required',
            'numero'          => 'required',
            'bairro'          => 'required',
            'cidade'          => 'required',
            'uf'              => 'required',
            'senha'           => 'required|confirmed|min:6',
        ];

        if ($this->method() != 'POST') {
            $rules['email']  = 'required|email|max:255|unique:estabelecimentos,email,'.\Auth::guard('estabelecimento')->user()->id;
            $rules['senha']  = 'confirmed|min:6';
        }

        return $rules;
    }
}
