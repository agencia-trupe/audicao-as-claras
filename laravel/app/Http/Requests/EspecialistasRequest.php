<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EspecialistasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'            => 'required',
            'email'           => 'required|email|max:255|unique:especialistas',
            'cpf'             => 'required',
            'profissao_cargo' => 'required',
            'telefone'        => 'required',
            'cep'             => 'required',
            'endereco'        => 'required',
            'numero'          => 'required',
            'bairro'          => 'required',
            'cidade'          => 'required',
            'uf'              => 'required',
            'nome_exibicao'   => 'required',
            'descricao'       => 'required',
            'imagem'          => 'image',
            'senha'           => 'required|confirmed|min:6',
        ];

        if ($this->method() != 'POST') {
            $rules['email']  = 'required|email|max:255|unique:especialistas,email,'.\Auth::guard('especialista')->user()->id;
            $rules['senha']  = 'confirmed|min:6';
        }

        return $rules;
    }
}
