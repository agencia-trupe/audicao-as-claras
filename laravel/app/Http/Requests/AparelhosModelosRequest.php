<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AparelhosModelosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'slug' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
