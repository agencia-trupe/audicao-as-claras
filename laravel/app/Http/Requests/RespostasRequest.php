<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RespostasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'respostas_categoria_id' => 'required',
            'especialista' => 'required',
            'pergunta' => 'required',
            'resposta' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
