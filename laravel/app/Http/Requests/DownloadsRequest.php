<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DownloadsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'  => 'required',
            'email' => 'email|required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required'  => 'preencha seu nome',
            'email.required' => 'insira um endereço de e-mail',
            'email.email'    => 'insira um endereço de e-mail válido',
        ];
    }
}
