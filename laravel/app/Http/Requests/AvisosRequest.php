<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AvisosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'texto' => 'required',
            'botao_link' => 'required_with:botao_texto',
            'botao_texto' => 'required_with:botao_link',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
