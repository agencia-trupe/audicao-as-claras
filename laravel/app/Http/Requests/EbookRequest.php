<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EbookRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo' => 'required',
            'arquivo' => 'required|mimes:pdf',
        ];
    }
}
