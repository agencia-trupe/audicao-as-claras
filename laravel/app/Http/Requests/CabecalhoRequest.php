<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CabecalhoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'frase' => 'required',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
            'imagem_4' => 'image',
            'imagem_5' => 'image',
        ];
    }
}
