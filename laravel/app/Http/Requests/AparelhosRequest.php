<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AparelhosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'marca_id' => 'required',
            'modelo_id' => 'required',
            'nome' => 'required',
            'imagem' => 'required|image',
            'ampliacao' => 'required|image',
            'caracteristicas' => 'required',
            'classificacao' => 'required',
            'conversar_com_uma_pessoa' => '',
            'ambientes_internos' => '',
            'conversar_em_grupo' => '',
            'falar_ao_telefone' => '',
            'assistir_tv' => '',
            'ambientes_ao_ar_livre' => '',
            'showsconcertos_e_teatro' => '',
            'restaurantes' => '',
            'festas_e_ambientes_muito_ruidosos' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
            $rules['ampliacao'] = 'image';
        }

        return $rules;
    }
}
