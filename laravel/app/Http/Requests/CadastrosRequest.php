<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CadastrosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'     => 'required',
            'email'    => 'required|email|unique:cadastros',
            'cep'      => 'required|cep',
            'senha'    => 'required|confirmed|min:6',
        ];
    }

    public function messages() {
        return [
            'nome.required'   => 'preencha seu nome',
            'email.required'  => 'insira um endereço de e-mail válido',
            'email.email'     => 'insira um endereço de e-mail válido',
            'email.unique'    => 'o e-mail inserido já está cadastrado',
            'senha.confirmed' => 'a confirmação de senha não confere',
            'senha.min'       => 'sua senha deve ter no mínimo 6 caracteres',
            'cep.required'    => 'insira um CEP válido',
            'cep.cep'         => 'insira um CEP válido',
        ];
    }
}
