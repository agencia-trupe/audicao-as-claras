<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EstabelecimentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'            => 'required',
            'email'           => 'required|email|max:255|unique:estabelecimentos,email,'.$this->route('estabelecimentos')->id,
            'razao_social'    => 'required',
            'cnpj'            => 'required',
            'area_de_atuacao' => 'required',
            'telefone'        => 'required',
            'responsavel'     => 'required',
            'cep'             => 'required',
            'endereco'        => 'required',
            'numero'          => 'required',
            'bairro'          => 'required',
            'cidade'          => 'required',
            'uf'              => 'required',
        ];

        return $rules;
    }
}
