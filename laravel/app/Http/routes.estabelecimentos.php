<?php

Route::group(['middleware' => ['web']], function () {
    Route::group([
        'prefix'     => 'estabelecimento',
        'middleware' => ['auth.estabelecimento']
    ], function () {
        Route::get('/', 'Estabelecimento\PainelController@index')->name('estabelecimento');
        Route::patch('meus-dados', 'Estabelecimento\PainelController@update')->name('estabelecimento.update');
        Route::get('avisos', 'Especialista\PainelController@avisos')->name('estabelecimento.avisos');
        Route::get('aulas/{aula_categoria?}', 'Especialista\PainelController@aulas')->name('estabelecimento.aulas');
        Route::get('aulas/{aula_categoria}/{aula_slug}', 'Especialista\PainelController@aula')->name('estabelecimento.aula');
    });

    Route::group([
        'prefix'    => 'estabelecimento',
        'namespace' => 'EstabelecimentoAuth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('estabelecimento.auth');
        Route::post('login', 'AuthController@login')->name('estabelecimento.login');
        Route::get('logout', 'AuthController@logout')->name('estabelecimento.logout');
        Route::get('cadastro', 'AuthController@showRegistrationForm')->name('estabelecimento.cadastro');
        Route::post('cadastro', 'AuthController@register')->name('estabelecimento.cadastrar');
        Route::get('esqueci-minha-senha', 'PasswordController@esqueci')->name('estabelecimento.esqueci');
        Route::post('redefinicao-de-senha', 'PasswordController@sendResetLinkEmail')->name('estabelecimento.redefinicao');
        Route::get('redefinicao-de-senha/{token}', 'PasswordController@showResetForm');
        Route::post('redefinir-senha', 'PasswordController@reset')->name('estabelecimento.redefinir');
    });
});
