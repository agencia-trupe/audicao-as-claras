<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ChamadasHome;
use App\Models\BlogPost;
use App\Models\Aparelho;
use App\Models\AparelhoAvaliacao;
use App\Models\Estabelecimento;
use App\Models\EstabelecimentoAvaliacao;
use App\Models\Resposta;
use App\Models\RespostaAvaliacao;
use App\Models\Especialista;
use App\Models\EspecialistaAvaliacao;

use App\Models\Newsletter;
use App\Http\Requests\NewsletterRequest;

class HomeController extends Controller
{
    public function index()
    {
        $chamadas = ChamadasHome::first();
        $posts    = BlogPost::ordenados()->take(3)->get();

        return view('frontend.home', compact('chamadas', 'posts'));
    }

    public function newsletter(NewsletterRequest $request, Newsletter $newsletter)
    {
        $newsletter->create($request->all());

        $response = [
            'status'  => 'success',
            'message' => 'cadastro efetuado com sucesso!'
        ];

        return response()->json($response);
    }

    public function selecaoLogin()
    {
        if (!request()->ajax()) {
            abort('404');
        }

        return view('frontend.common.selecao-login');
    }

    public function login()
    {
        if (!request()->ajax()) {
            abort('404');
        }

        return view('frontend.common.login');
    }

    public function cadastro()
    {
        if (!request()->ajax()) {
            abort('404');
        }

        return view('frontend.common.cadastro');
    }

    public function esqueci()
    {
        if (!request()->ajax()) {
            abort('404');
        }

        return view('frontend.common.esqueci');
    }

    public function avaliar($tipo = null, Request $request)
    {
        if (!$tipo) {
            abort('500');
        }

        $usuario = \Auth::guard('cadastro')->user();

        if ($request->get('avaliacao') < 1 || $request->get('avaliacao') > 5) {
            throw new \Exception('Ocorreu um erro.');
        }

        if ($tipo == 'aparelho') {
            $avaliacao = AparelhoAvaliacao::firstOrNew([
                'cadastro_id' => $usuario->id,
                'aparelho_id' => $request->get('id')
            ]);
            $avaliacao->avaliacao = intval($request->get('avaliacao'));
            $avaliacao->save();

            $avaliacoesAparelho = AparelhoAvaliacao::where('aparelho_id', $request->get('id'))
                ->lists('avaliacao')->toArray();
            $mediaDeAvaliacoes  = round(array_sum($avaliacoesAparelho) / count($avaliacoesAparelho));

            Aparelho::find($request->get('id'))->update([
                'avaliacao_clientes' => $mediaDeAvaliacoes
            ]);
        } elseif ($tipo == 'estabelecimento') {
            $avaliacao = EstabelecimentoAvaliacao::firstOrNew([
                'cadastro_id'        => $usuario->id,
                'estabelecimento_id' => $request->get('id')
            ]);
            $avaliacao->avaliacao = intval($request->get('avaliacao'));
            $avaliacao->save();

            $avaliacoesAparelho = EstabelecimentoAvaliacao::where('estabelecimento_id', $request->get('id'))
                ->lists('avaliacao')->toArray();
            $mediaDeAvaliacoes  = round(array_sum($avaliacoesAparelho) / count($avaliacoesAparelho));

            Estabelecimento::find($request->get('id'))->update([
                'avaliacao_clientes' => $mediaDeAvaliacoes
            ]);
        } elseif ($tipo == 'resposta') {
            $avaliacao = RespostaAvaliacao::firstOrNew([
                'cadastro_id' => $usuario->id,
                'resposta_id' => $request->get('id')
            ]);
            $avaliacao->avaliacao = intval($request->get('avaliacao'));
            $avaliacao->save();

            $avaliacoesResposta = RespostaAvaliacao::where('resposta_id', $request->get('id'))
                ->lists('avaliacao')->toArray();
            $mediaDeAvaliacoes  = round(array_sum($avaliacoesResposta) / count($avaliacoesResposta));

            Resposta::find($request->get('id'))->update([
                'avaliacao_clientes' => $mediaDeAvaliacoes
            ]);
        } elseif ($tipo == 'especialista') {
            $avaliacao = EspecialistaAvaliacao::firstOrNew([
                'cadastro_id'     => $usuario->id,
                'especialista_id' => $request->get('id')
            ]);
            $avaliacao->avaliacao = intval($request->get('avaliacao'));
            $avaliacao->save();

            $avaliacoesEspecialista = EspecialistaAvaliacao::where('especialista_id', $request->get('id'))
                ->lists('avaliacao')->toArray();
            $mediaDeAvaliacoes  = round(array_sum($avaliacoesEspecialista) / count($avaliacoesEspecialista));

            Especialista::find($request->get('id'))->update([
                'avaliacao_clientes' => $mediaDeAvaliacoes
            ]);
        }

        return response()->json([
            'success'   => 'true',
            'avaliacao' => $request->get('avaliacao')
        ]);
    }
}
