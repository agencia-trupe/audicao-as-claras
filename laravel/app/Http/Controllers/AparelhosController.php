<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\AparelhosTexto;
use App\Models\Aparelho;
use App\Models\AparelhoMarca;
use App\Models\AparelhoModelo;
use App\Models\AparelhoAvaliacao;
use App\Models\Estabelecimento;
use App\Models\EstabelecimentoAvaliacao;
use App\Models\Especialista;
use App\Models\EspecialistaAvaliacao;
use App\Models\Consulta;
use App\Models\Contato;

use App\Helpers\Tools;

use \Validator;

class AparelhosController extends Controller
{
    public function index(Request $request)
    {
        $texto = AparelhosTexto::first();

        return view('frontend.aparelhos.index', compact('texto'));
    }

    public function show($id)
    {
        $usuario          = auth('cadastro')->user();
        $aparelho         = Aparelho::findOrFail($id);
        $estabelecimentos = Estabelecimento::aprovados()
            ->with('avaliacoes')
            ->whereHas('marcas', function ($query) use ($aparelho) {
                $query->whereIn('marca_id', [$aparelho->marca->id]);
            })->get();

        $necessidadesBooleans = func_get_args();
        $linkVoltar           = route('aparelhos.consulta', [
            'conversar_com_uma_pessoa'          => $necessidadesBooleans[1],
            'ambientes_internos'                => $necessidadesBooleans[2],
            'conversar_em_grupo'                => $necessidadesBooleans[3],
            'falar_ao_telefone'                 => $necessidadesBooleans[4],
            'assistir_tv'                       => $necessidadesBooleans[5],
            'ambientes_ao_ar_livre'             => $necessidadesBooleans[6],
            'showsconcertos_e_teatro'           => $necessidadesBooleans[7],
            'restaurantes'                      => $necessidadesBooleans[8],
            'festas_e_ambientes_muito_ruidosos' => $necessidadesBooleans[9],
            'sent' => true
        ]);

        if (count($estabelecimentos)) {
            try {
                $coordenadas = Tools::fetchCoordinates($usuario->cep);
            } catch (\Exception $e) {
                $estabelecimentos = [];
                return view('frontend.aparelhos.caracteristicas', compact('aparelho', 'estabelecimentos', 'linkVoltar', 'avaliacao'));
            }

            $estabelecimentos = Tools::sortByCoordinates($estabelecimentos, $coordenadas)->take(12);
        }

        return view('frontend.aparelhos.caracteristicas', compact('aparelho', 'estabelecimentos', 'linkVoltar', 'avaliacao'));
    }

    public function telefone($id)
    {
        $estabelecimento = Estabelecimento::aprovados()->findOrFail($id);
        $estabelecimento->increment('exibicoes');

        $telefone = $estabelecimento->telefone;

        return response()->json($telefone);
    }

    public function telefoneEspecialista($id)
    {
        $especialista = Especialista::aprovados()->findOrFail($id);
        $especialista->increment('exibicoes');

        $telefone = $especialista->telefone;

        return response()->json($telefone);
    }

    public function post(Request $request)
    {
        $input = $request->except('_token');

        if (!count($input)) {
            return back();
        }

        if (auth('cadastro')->check()) {
            return redirect()->route('aparelhos.consulta', [
                array_key_exists('conversar_com_uma_pessoa', $input) ? 1 : 0,
                array_key_exists('ambientes_internos', $input) ? 1 : 0,
                array_key_exists('conversar_em_grupo', $input) ? 1 : 0,
                array_key_exists('falar_ao_telefone', $input) ? 1 : 0,
                array_key_exists('assistir_tv', $input) ? 1 : 0,
                array_key_exists('ambientes_ao_ar_livre', $input) ? 1 : 0,
                array_key_exists('showsconcertos_e_teatro', $input) ? 1 : 0,
                array_key_exists('restaurantes', $input) ? 1 : 0,
                array_key_exists('festas_e_ambientes_muito_ruidosos', $input) ? 1 : 0,
            ]);
        }

        return view('frontend.aparelhos.dados', compact('input'));
    }

    public function consulta(Request $request)
    {
        $sent = !!$request->get('sent');

        $necessidadesBooleans = func_get_args();
        $necessidades = [
            'conversar_com_uma_pessoa'          => $necessidadesBooleans[1],
            'ambientes_internos'                => $necessidadesBooleans[2],
            'conversar_em_grupo'                => $necessidadesBooleans[3],
            'falar_ao_telefone'                 => $necessidadesBooleans[4],
            'assistir_tv'                       => $necessidadesBooleans[5],
            'ambientes_ao_ar_livre'             => $necessidadesBooleans[6],
            'showsconcertos_e_teatro'           => $necessidadesBooleans[7],
            'restaurantes'                      => $necessidadesBooleans[8],
            'festas_e_ambientes_muito_ruidosos' => $necessidadesBooleans[9],
        ];

        $aparelhos = Aparelho::where(array_filter($necessidades, function ($boolean) {
            return !!$boolean;
        }))->ordenados()->get();

        $aparelhos->map(function ($aparelho) {
            $aparelho->classificacao = $aparelho->avaliacaoReal;
        });

        $marcasIds    = $aparelhos->unique('marca_id')->lists('marca_id');
        $marcas       = AparelhoMarca::whereIn('id', $marcasIds)->ordenados()->get();
        $modelosIds   = $aparelhos->unique('modelo_id')->lists('modelo_id');
        $modelos      = AparelhoModelo::whereIn('id', $modelosIds)->ordenados()->get();
        $aparelhosIds = $aparelhos->lists('id')->toArray();

        $usuario = auth('cadastro')->user();

        if (!$sent) {
            Consulta::create(array_merge([
                'nome'     => $usuario->nome,
                'email'    => $usuario->email,
                'telefone' => $usuario->telefone,
                'cep'      => $usuario->cep
            ], $necessidades));

            if (count($aparelhos)) {
                try {
                    $contato = Contato::first();
                    \Mail::send('emails.resultado', ['necessidades' => $necessidades, 'aparelhos' => $aparelhos], function ($message) use ($usuario, $contato) {
                        $message->to($usuario->email, $usuario->nome)
                                ->subject('Resultados da consulta de aparelhos auditivos')
                                ->replyTo($contato->email, 'Audição às Claras');
                    });
                } catch (\Exception $e) {
                }
            }
        }

        return view('frontend.aparelhos.resultado', compact('necessidades', 'aparelhos', 'marcas', 'modelos'));
    }

    public function estabelecimentos(Request $request)
    {
        if (! $request->get('cep') && ! $request->get('palavra-chave')) {
            return redirect()->route('aparelhos');
        }

        if ($cep = $request->get('cep')) {
            $estabelecimentos = Estabelecimento::with('marcas', 'avaliacoes')
                ->aprovados()->get();
            $especialistas = Especialista::with('avaliacoes')
                ->aprovados()->get();

            if (count($estabelecimentos) || count($especialistas)) {
                try {
                    $coordenadas = Tools::fetchCoordinates($cep);
                } catch (\Exception $e) {
                    $erro = 'CEP não encontrado. Tente novamente.';
                    return view('frontend.aparelhos.estabelecimentos', compact('erro'));
                }

                $estabelecimentos = Tools::sortByCoordinates($estabelecimentos, $coordenadas)->take(12);
                $especialistas    = Tools::sortByCoordinates($especialistas, $coordenadas)->take(12);
            }
        }

        if ($request->get('palavra-chave')) {
            $palavraChave = $request->get('palavra-chave');

            $estabelecimentos = Estabelecimento::with('marcas', 'avaliacoes')
                ->aprovados()->busca($palavraChave)->get();
            $especialistas = Especialista::with('avaliacoes')
                ->aprovados()->busca($palavraChave)->get();
        }

        return view('frontend.aparelhos.estabelecimentos', compact('estabelecimentos', 'especialistas'));
    }
}
