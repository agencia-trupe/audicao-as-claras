<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Pergunta;

class FaqController extends Controller
{
    public function index()
    {
        $perguntas = Pergunta::ordenados()->get();

        return view('frontend.faq', compact('perguntas'));
    }
}
