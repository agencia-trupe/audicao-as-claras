<?php

namespace App\Http\Controllers\EspecialistaAuth;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\Especialista;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\EspecialistasRequest;
use App\Models\AparelhoMarca;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/especialista';
    protected $guard      = 'especialista';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest.especialista', ['except' => 'logout']);
    }

    public function showRegistrationForm()
    {
        $marcas = AparelhoMarca::ordenados()->get();

        return view('especialista.cadastro', compact('marcas'));
    }

    public function register(EspecialistasRequest $request)
    {
        $input = $request->all();

        try {
            $endereco = "${input['endereco']} ${input['numero']} - ${input['bairro']}, ${input['cidade']} - ${input['uf']}";
            $coordenadas = \Tools::fetchCoordinates($endereco);
            $input['lat'] = $coordenadas->lat;
            $input['lng'] = $coordenadas->lng;
        } catch(\Exception $e) {
            return back()->withInput()->with('erroCoord', $e->getMessage());
        }

        $input['senha'] = bcrypt($input['senha']);

        if (isset($input['imagem'])) {
            $input['imagem'] = Especialista::upload_imagem();
        }

        $especialista = Especialista::create($input);

        return redirect()->route('home')->with('cadastroEspecialistaRedirect', true);
    }

    public function showLoginForm()
    {
        return view('especialista.login');
    }

    protected function login(Request $request)
    {
        if (Auth::guard('especialista')->attempt([
            'email'    => $request->input('email'),
            'password' => $request->input('password')
        ], true)) {
            if (!Auth::guard('especialista')->user()->aprovado) {
                Auth::guard('especialista')->logout();
                return redirect()->route('especialista.auth')->with('error', 'Seu cadastro está em processo de aprovação pelos administradores.');
            }
            return redirect()->intended('especialista');
        } else {
            return redirect()->route('especialista.auth')->withInput()->with('error', 'Usuário ou senha inválidos.');
        }
    }
}
