<?php

namespace App\Http\Controllers\Especialista;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Resposta;
use App\Models\Aviso;
use App\Models\AulaCategoria;
use App\Models\Aula;
use App\Models\Especialista;
use App\Http\Requests\EspecialistasRequest;
use App\Models\AparelhoMarca;

class PainelController extends Controller
{
    public function index()
    {
        $registro = \Auth::guard('especialista')->user();
        $marcas = AparelhoMarca::ordenados()->get();

        return view('especialista.meus-dados', compact('registro', 'marcas'));
    }

    public function update(EspecialistasRequest $request)
    {
        $registro = \Auth::guard('especialista')->user();

        $input = array_filter($request->all(), 'strlen');

        if ($input['endereco'] != $registro->endereco || $input['numero'] != $registro->numero || $input['bairro'] != $registro->bairro || $input['cidade'] != $registro->cidade || $input['uf'] != $registro->uf) {
            try {
                $endereco = "${input['endereco']} ${input['numero']} - ${input['bairro']}, ${input['cidade']} - ${input['uf']}";
                $coordenadas = \Tools::fetchCoordinates($endereco);
                $input['lat'] = $coordenadas->lat;
                $input['lng'] = $coordenadas->lng;
            } catch (\Exception $e) {
                return back()->with('erroCoord', $e->getMessage());
            }
        }

        if (isset($input['senha'])) {
            $input['senha'] = bcrypt($input['senha']);
        }

        if (isset($input['imagem'])) {
            $input['imagem'] = Especialista::upload_imagem();
        }

        $registro->update($input);

        return redirect()->back()->with('success', 'Dados atualizados com sucesso.');
    }

    public function avisos()
    {
        $avisos = Aviso::ordenados()->get();

        return view('especialista.avisos', compact('avisos'));
    }

    public function aulas(AulaCategoria $categoria)
    {
        $categorias = AulaCategoria::ordenados()->get();

        if (!$categoria->exists) {
            $categoria = AulaCategoria::ordenados()->first();
        }

        if (! $categoria) {
            if (auth('estabelecimento')->check()) {
                return redirect()->route('estabelecimento');
            }
            return redirect()->route('especialista');
        }

        $aulas = $categoria->aulas;

        return view('especialista.aulas.index', compact('categorias', 'categoria', 'aulas'));
    }

    public function aula(AulaCategoria $categoria, Aula $aula)
    {
        $categorias = AulaCategoria::ordenados()->get();

        return view('especialista.aulas.show', compact('categorias', 'aula'));
    }

    public function perguntas()
    {
        $perguntas = Resposta::where('aprovado', 0)
            ->where('especialista', null)
            ->orderBy('id', 'DESC')
            ->paginate(10);

        return view('especialista.perguntas.index', compact('perguntas'));
    }

    public function form($id)
    {
        $pergunta = Resposta::findOrFail($id);

        if ($pergunta->especialista != null) {
            abort('404');
        }

        return view('especialista.perguntas.form', compact('pergunta'));
    }

    public function post($id, Request $request)
    {
        $pergunta = Resposta::findOrFail($id);

        if ($pergunta->especialista != null) {
            abort('404');
        }

        $this->validate($request, [
            'resposta' => 'required',
        ]);

        try {
            $pergunta->update([
                'especialista' => \Auth::guard('especialista')->user()->id,
                'resposta'     => $request->get('resposta')
            ]);

            return redirect()->route('especialista.perguntas')->with('success', 'Resposta enviada com sucesso. Aguarde a validação do administrador.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);
        }
    }

    public function minhasRespostas()
    {
        $respostas = Resposta::where('aprovado', 1)
            ->where('especialista', \Auth::guard('especialista')->user()->id)
            ->orderBy('id', 'DESC')
            ->get();

        return view('especialista.perguntas.minhas-respostas', compact('respostas'));
    }
}
