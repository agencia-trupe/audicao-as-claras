<?php

namespace App\Http\Controllers\Estabelecimento;

use Illuminate\Http\Request;
use App\Http\Requests\EstabelecimentosPublicRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Aviso;
use App\Models\AulaCategoria;
use App\Models\Aula;
use App\Models\Especialista;
use App\Models\AparelhoMarca;

class PainelController extends Controller
{
    public function index()
    {
        $registro = \Auth::guard('estabelecimento')->user();
        $marcas = AparelhoMarca::ordenados()->get();

        return view('estabelecimento.meus-dados', compact('registro', 'marcas'));
    }

    public function update(EstabelecimentosPublicRequest $request)
    {
        $registro = \Auth::guard('estabelecimento')->user();

        $input = array_filter($request->except('marcas'), 'strlen');

        if ($input['endereco'] != $registro->endereco || $input['numero'] != $registro->numero || $input['bairro'] != $registro->bairro || $input['cidade'] != $registro->cidade || $input['uf'] != $registro->uf) {
            try {
                $endereco = "${input['endereco']} ${input['numero']} - ${input['bairro']}, ${input['cidade']} - ${input['uf']}";
                $coordenadas = \Tools::fetchCoordinates($endereco);
                $input['lat'] = $coordenadas->lat;
                $input['lng'] = $coordenadas->lng;
            } catch (\Exception $e) {
                return back()->with('erroCoord', $e->getMessage());
            }
        }

        if (isset($input['senha'])) {
            $input['senha'] = bcrypt($input['senha']);
        }

        $registro->update($input);
        $registro->marcas()->sync(request('marcas') ?: []);

        return redirect()->back()->with('success', 'Dados atualizados com sucesso.');
    }
}
