<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PerguntasRecebidasRequest;

use App\Models\Especialista;
use App\Models\Resposta;
use App\Models\RespostaCategoria;
use App\Models\RespostaEmDestaque;
use App\Models\RespostaAvaliacao;

use App\Models\Contato;

class PergunteController extends Controller
{
    public function index(RespostaCategoria $categoria)
    {
        $categorias    = RespostaCategoria::ordenados()->get();

        if (!$categoria->exists) {
            $categoria = RespostaCategoria::ordenados()->first();
        }

        if (!$categoria) {
            return abort('404');
        }

        $especialistas = Especialista::aprovados()->ordenados()->get();
        $destaque      = RespostaEmDestaque::first();
        $respostas     = $categoria->respostas()
            ->with('avaliacoes', 'especialistaModel')
            ->where('aprovado', 1)
            ->whereNotNull('especialista')
            ->orderBy('id', 'DESC')
            ->get();

        return view('frontend.pergunte', compact('especialistas', 'destaque', 'categorias', 'categoria', 'respostas'));
    }

    public function post(PerguntasRecebidasRequest $request, Resposta $perguntaRecebida)
    {
        $perguntaRecebida->create($request->all());

        /*** Envio de e-mail desabilitado,
         *** perguntas são exibidas no painel do especialista
        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.pergunta', $request->all(), function ($message) use ($request, $contato) {
                $message->to($contato->email, config('site.name'))
                        ->subject('[PERGUNTE AO ESPECIALISTA] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        } */

        $response = [
            'status'  => 'success',
            'message' => 'Pergunta enviada com sucesso!'
        ];

        return response()->json($response);
    }
}
