<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AulasDownloadsRequest;
use App\Http\Controllers\Controller;

use App\Models\Aula;
use App\Models\AulaDownload;

class AulasDownloadsController extends Controller
{
    public function index(Aula $aula)
    {
        $registros = AulaDownload::aula($aula->id)->ordenados()->get();

        return view('painel.aulas.downloads.index', compact('aula', 'registros'));
    }

    public function create(Aula $aula)
    {
        return view('painel.aulas.downloads.create', compact('aula'));
    }

    public function store(Aula $aula, AulasDownloadsRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = AulaDownload::upload_arquivo();
            }

            $aula->downloads()->create($input);

            return redirect()->route('painel.aulas.downloads.index', $aula->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);
        }
    }

    public function edit(Aula $aula, AulaDownload $registro)
    {
        return view('painel.aulas.downloads.edit', compact('aula', 'registro'));
    }

    public function update(Aula $aula, AulaDownload $registro, AulasDownloadsRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['arquivo'])) {
                $input['arquivo'] = AulaDownload::upload_arquivo();
            }

            $registro->update($input);

            return redirect()->route('painel.aulas.downloads.index', $aula->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }

    public function destroy(Aula $aula, AulaDownload $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.aulas.downloads.index', $aula->id)->with('success', 'Registro excluí­do com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);
        }
    }
}
