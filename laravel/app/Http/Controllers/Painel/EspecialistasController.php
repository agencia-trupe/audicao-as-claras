<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EspecialistasRequest;
use App\Http\Controllers\Controller;

use App\Models\Especialista;

class EspecialistasController extends Controller
{
    public function index(Request $request)
    {
        $countAprovacao = Especialista::where('aprovado', 0)->count();

        if ($request->get('aprovacao')) {
            $registros = Especialista::where('aprovado', 0)
                ->orderBy('id', 'ASC')->get();
        } else {
            $registros = Especialista::where('aprovado', 1)
                ->ordenados()->get();
        }

        return view('painel.especialistas.index', compact('registros', 'countAprovacao'));
    }

    public function show(Especialista $registro)
    {
        return view('painel.especialistas.show', compact('registro'));
    }

    public function aprovar(Especialista $registro)
    {
        $registro->update([
            'aprovado' => 1
        ]);

        \Mail::send('emails.aprovado', [], function ($message) use ($registro) {
            $message->to($registro->email, $registro->nome)
                    ->subject('Audição às Claras &middot; Cadastro Aprovado');
        });

        return redirect()->route('painel.especialistas.index')->with('success', 'Especialista aprovado com sucesso.');
    }

    public function destroy(Especialista $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.especialistas.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);
        }
    }

    public function exibicoes()
    {
        $especialistas = Especialista::where('aprovado', 1)->orderBy('exibicoes', 'DESC')->get();

        return view('painel.especialistas.exibicoes', compact('especialistas'));
    }
}
