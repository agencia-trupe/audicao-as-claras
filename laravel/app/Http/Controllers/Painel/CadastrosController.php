<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\Cadastro;
use Carbon\Carbon;

class CadastrosController extends Controller
{
    public function index(Request $request)
    {
        $registros = Cadastro::orderBy('id', 'DESC');
      
        if ($request->get('data_inicio') && Carbon::createFromFormat('d/m/Y', $request->get('data_inicio'))) {
            $inicio = Carbon::createFromFormat('d/m/Y', $request->get('data_inicio'))->format('Y-m-d');
            $registros = $registros->whereDate('created_at', '>=', $inicio);
        }
        if ($request->get('data_final') && Carbon::createFromFormat('d/m/Y', $request->get('data_final'))) {
            $final = Carbon::createFromFormat('d/m/Y', $request->get('data_final'))->format('Y-m-d');
            $registros = $registros->whereDate('created_at', '<=', $final);
        }
        if ($request->get('palavra_chave')) {
            $registros = $registros->where('nome', 'LIKE', '%'.$request->get('palavra_chave').'%')
            ->orWhere('email', 'LIKE', '%'.$request->get('palavra_chave').'%')
            ->orWhere('telefone', 'LIKE', '%'.$request->get('palavra_chave').'%')
            ->orWhere('cep', 'LIKE', '%'.$request->get('palavra_chave').'%');
        }
        
        $registros = $registros->paginate(20);

        return view('painel.cadastros.index', compact('registros'));
    }

    public function exportar()
    {
        $file = 'audicaoasclaras-cadastros_'.date('d-m-Y_His');

        \Excel::create($file, function($excel) {
            $excel->sheet('cadastros', function($sheet) {
                $sheet->fromModel(Cadastro::orderBy('id', 'DESC')->get([
                    'created_at as data_cadastro',
                    'nome',
                    'email',
                    'telefone',
                    'cep'
                ]));
            });
        })->download('csv');
    }

    public function destroy(Cadastro $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.cadastros.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
