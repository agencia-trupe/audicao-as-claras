<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EbookRequest;
use App\Http\Controllers\Controller;

use App\Models\Ebook;

class EbookController extends Controller
{
    public function index()
    {
        $registro = Ebook::first();

        return view('painel.ebook.edit', compact('registro'));
    }

    public function update(EbookRequest $request, Ebook $registro)
    {
        try {
            $input = $request->all();

            if ($request->hasFile('arquivo')) {
                $input['arquivo'] = Ebook::uploadFile();
            }

            $registro->update($input);

            return redirect()->route('painel.ebook.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }

    public function delete()
    {
        try {
            $ebook = Ebook::first();
            $ebook->update([
                'titulo'  => '',
                'arquivo' => ''
            ]);
            $ebook->save();

            return redirect()->route('painel.ebook.index')->with('success', 'Ebook excluído com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir ebook: '.$e->getMessage()]);
        }
    }
}
