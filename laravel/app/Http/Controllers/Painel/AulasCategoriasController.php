<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AulasCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\AulaCategoria;

class AulasCategoriasController extends Controller
{
    public function index()
    {
        $categorias = AulaCategoria::ordenados()->get();

        return view('painel.aulas.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.aulas.categorias.create');
    }

    public function store(AulasCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            AulaCategoria::create($input);
            return redirect()->route('painel.aulas.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(AulaCategoria $categoria)
    {
        return view('painel.aulas.categorias.edit', compact('categoria'));
    }

    public function update(AulasCategoriasRequest $request, AulaCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.aulas.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(AulaCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.aulas.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
