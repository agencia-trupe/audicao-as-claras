<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ChamadasHomeRequest;
use App\Http\Controllers\Controller;

use App\Models\ChamadasHome;

class ChamadasHomeController extends Controller
{
    public function index()
    {
        $registro = ChamadasHome::first();

        return view('painel.chamadas-home.edit', compact('registro'));
    }

    public function update(ChamadasHomeRequest $request, ChamadasHome $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.chamadas-home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
