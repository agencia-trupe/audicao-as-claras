<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AparelhosMarcasRequest;
use App\Http\Controllers\Controller;

use App\Models\AparelhoMarca;

class AparelhosMarcasController extends Controller
{
    public function index()
    {
        $registros = AparelhoMarca::ordenados()->get();

        return view('painel.aparelhos.marcas.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.aparelhos.marcas.create');
    }

    public function store(AparelhosMarcasRequest $request)
    {
        try {

            $input = $request->all();


            AparelhoMarca::create($input);
            return redirect()->route('painel.aparelhos.marcas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(AparelhoMarca $registro)
    {
        return view('painel.aparelhos.marcas.edit', compact('registro'));
    }

    public function update(AparelhosMarcasRequest $request, AparelhoMarca $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.aparelhos.marcas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(AparelhoMarca $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.aparelhos.marcas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
