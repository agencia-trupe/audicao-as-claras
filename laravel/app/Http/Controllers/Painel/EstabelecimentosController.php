<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EstabelecimentosRequest;
use App\Http\Controllers\Controller;

use App\Models\Estabelecimento;
use App\Models\AparelhoMarca;

class EstabelecimentosController extends Controller
{
    public function index(Request $request)
    {
        $countAprovacao = Estabelecimento::where('aprovado', 0)->count();

        if ($request->get('aprovacao')) {
            $registros = Estabelecimento::where('aprovado', 0)
                ->orderBy('id', 'ASC')->get();
        } else {
            $registros = Estabelecimento::where('aprovado', 1)
                ->orderBy('id', 'ASC')->get();
        }

        return view('painel.estabelecimentos.index', compact('registros', 'countAprovacao'));
    }

    public function edit(Estabelecimento $registro)
    {
        $marcas = AparelhoMarca::ordenados()->get();

        return view('painel.estabelecimentos.edit', compact('registro', 'marcas'));
    }

    public function update(EstabelecimentosRequest $request, Estabelecimento $registro)
    {
        try {

            $input = $request->except('marcas');

            if ($input['endereco'] != $registro->endereco || $input['numero'] != $registro->numero || $input['bairro'] != $registro->bairro || $input['cidade'] != $registro->cidade || $input['uf'] != $registro->uf)
            {
                $endereco = "${input['endereco']} ${input['numero']} - ${input['bairro']}, ${input['cidade']} - ${input['uf']}";
                $coordenadas = \Tools::fetchCoordinates($endereco);
                $input['lat'] = $coordenadas->lat;
                $input['lng'] = $coordenadas->lng;
            }

            $registro->update($input);

            $marcas = ($request->get('marcas') ?: []);
            $registro->marcas()->sync($marcas);

            $query = [];
            if (!$registro->aprovado) {
                $query['aprovacao'] = 1;
            }

            return redirect()->route('painel.estabelecimentos.index', $query)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()])->withInput();

        }
    }

    public function destroy(Estabelecimento $registro)
    {
        try {

            $registro->marcas()->detach();
            $registro->delete();

            return redirect()->route('painel.estabelecimentos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function aprovar(Estabelecimento $registro)
    {
        $registro->update([
            'aprovado' => 1
        ]);

        \Mail::send('emails.aprovado-estabelecimento', [], function ($message) use ($registro) {
            $message->to($registro->email, $registro->nome)
                    ->subject('Audição às Claras &middot; Cadastro Aprovado');
        });

        return redirect()->route('painel.estabelecimentos.index')->with('success', 'Estabelecimento aprovado com sucesso.');
    }

    public function exibicoes()
    {
        $estabelecimentos = Estabelecimento::where('aprovado', 1)->orderBy('exibicoes', 'DESC')->get();

        return view('painel.estabelecimentos.exibicoes', compact('estabelecimentos'));
    }
}
