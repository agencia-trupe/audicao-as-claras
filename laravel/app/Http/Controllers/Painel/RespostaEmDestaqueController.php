<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RespostaEmDestaqueRequest;
use App\Http\Controllers\Controller;

use App\Models\RespostaEmDestaque;
use App\Models\Resposta;

class RespostaEmDestaqueController extends Controller
{
    public function index()
    {
        $registro  = RespostaEmDestaque::first();
        $perguntas = Resposta::where('aprovado', 1)
            ->whereNotNull('especialista')
            ->lists('pergunta', 'id');

        return view('painel.resposta-em-destaque.edit', compact('registro', 'perguntas'));
    }

    public function update(RespostaEmDestaqueRequest $request, RespostaEmDestaque $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) {
                $input['imagem'] = RespostaEmDestaque::upload_imagem();
            }

            if ($input['pergunta'] == '') {
                $input['pergunta'] = null;
            }

            $registro->update($input);

            return redirect()->route('painel.resposta-em-destaque.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
