<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PerguntaRecebida;

class PerguntasRecebidasController extends Controller
{
    public function index()
    {
        $perguntasrecebidas = PerguntaRecebida::orderBy('created_at', 'DESC')->get();

        return view('painel.perguntas-recebidas.index', compact('perguntasrecebidas'));
    }

    public function show(PerguntaRecebida $pergunta)
    {
        $pergunta->update(['lido' => 1]);

        return view('painel.perguntas-recebidas.show', compact('pergunta'));
    }

    public function destroy(PerguntaRecebida $pergunta)
    {
        try {

            $pergunta->delete();
            return redirect()->route('painel.perguntas-recebidas.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(PerguntaRecebida $pergunta, Request $request)
    {
        try {

            $pergunta->update([
                'lido' => !$pergunta->lido
            ]);

            return redirect()->route('painel.perguntas-recebidas.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
