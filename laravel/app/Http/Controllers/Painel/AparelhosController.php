<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AparelhosRequest;
use App\Http\Controllers\Controller;

use App\Models\Aparelho;
use App\Models\AparelhoMarca;
use App\Models\AparelhoModelo;

class AparelhosController extends Controller
{
    public function __construct()
    {
        view()->share([
            'marcas'  => AparelhoMarca::ordenados()->lists('titulo', 'id'),
            'modelos' => AparelhoModelo::ordenados()->lists('titulo', 'id')
        ]);
    }

    public function index()
    {
        $registros = Aparelho::ordenados()->paginate(30);

        return view('painel.aparelhos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.aparelhos.create');
    }

    public function store(AparelhosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Aparelho::upload_imagem();
            if (isset($input['ampliacao'])) $input['ampliacao'] = Aparelho::upload_ampliacao();

            Aparelho::create($input);

            return redirect()->route('painel.aparelhos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Aparelho $registro)
    {
        return view('painel.aparelhos.edit', compact('registro'));
    }

    public function update(AparelhosRequest $request, Aparelho $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Aparelho::upload_imagem();
            if (isset($input['ampliacao'])) $input['ampliacao'] = Aparelho::upload_ampliacao();

            $registro->update($input);

            return redirect()->route('painel.aparelhos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Aparelho $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.aparelhos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
