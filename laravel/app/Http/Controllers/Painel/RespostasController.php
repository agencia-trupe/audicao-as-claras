<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RespostasRequest;
use App\Http\Controllers\Controller;

use App\Models\Resposta;
use App\Models\RespostaCategoria;
use App\Models\Especialista;

class RespostasController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = RespostaCategoria::ordenados()->lists('titulo', 'id');
        $this->especialistas = Especialista::ordenados()->lists('nome', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');
        
        $countAvaliacao = Resposta::where('aprovado', 0)
            ->whereNotNull('especialista')
            ->count();
        
        if ($request->get('avaliacao')) {
            $registros = Resposta::where('aprovado', 0)
            ->whereNotNull('especialista')
            ->get();
        } else {
            if (RespostaCategoria::find($filtro)) {
                $registros = Resposta::where('aprovado', 1)
                    ->whereNotNull('especialista')
                    ->where('respostas_categoria_id', $filtro)
                    ->orderBy('id', 'DESC')
                    ->get();
            } else {
                $registros = Resposta::leftJoin('respostas_categorias as cat', 'cat.id', '=', 'respostas_categoria_id')
                    ->where('aprovado', 1)
                    ->whereNotNull('especialista')
                    ->orderBy('cat.ordem', 'ASC')
                    ->orderBy('cat.id', 'DESC')
                    ->select('respostas.*')
                    ->orderBy('respostas.id', 'DESC')
                    ->get();
            }
        }

        return view('painel.respostas.index', compact('categorias', 'registros', 'filtro', 'countAvaliacao'));
    }

    public function create()
    {
        $categorias    = $this->categorias;
        $especialistas = $this->especialistas;

        return view('painel.respostas.create', compact('categorias', 'especialistas'));
    }

    public function store(RespostasRequest $request)
    {
        try {
            $input = $request->all();

            Resposta::create($input);

            return redirect()->route('painel.respostas.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);
        }
    }

    public function edit(Resposta $registro)
    {
        $categorias    = $this->categorias;
        $especialistas = $this->especialistas;

        if (!$registro->aprovado) {
            abort('404');
        }

        return view('painel.respostas.edit', compact('registro', 'categorias', 'especialistas'));
    }

    public function avaliar(Resposta $registro)
    {
        $categorias = $this->categorias;

        if ($registro->aprovado) {
            abort('404');
        }

        return view('painel.respostas.avaliar', compact('registro', 'categorias'));
    }

    public function rejeitar(Resposta $registro)
    {
        try {
            $registro->update([
                'especialista' => null,
                'resposta'     => ''
            ]);

            return redirect()->route('painel.respostas.index')->with('success', 'Resposta rejeitada com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }

    public function update(RespostasRequest $request, Resposta $registro)
    {
        try {
            $input = $request->all();

            $message = $request->get('aprovado') ? 'Resposta aprovada com sucesso.' : 'Registro alterado com sucesso.';

            $registro->update($input);

            return redirect()->route('painel.respostas.index')->with('success', $message);
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }

    public function destroy(Resposta $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.respostas.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);
        }
    }
}
