<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AvisosRequest;
use App\Http\Controllers\Controller;

use App\Models\Aviso;

class AvisosController extends Controller
{
    public function index()
    {
        $registros = Aviso::ordenados()->get();

        return view('painel.avisos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.avisos.create');
    }

    public function store(AvisosRequest $request)
    {
        try {

            $input = $request->all();

            Aviso::create($input);

            return redirect()->route('painel.avisos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Aviso $registro)
    {
        return view('painel.avisos.edit', compact('registro'));
    }

    public function update(AvisosRequest $request, Aviso $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.avisos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Aviso $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.avisos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
