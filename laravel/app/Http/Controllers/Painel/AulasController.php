<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AulasRequest;
use App\Http\Controllers\Controller;

use App\Models\Aula;
use App\Models\AulaCategoria;

class AulasController extends Controller
{
    private $categorias;

    public function __construct()
    {
        $this->categorias = AulaCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (AulaCategoria::find($filtro)) {
            $registros = Aula::where('aulas_categoria_id', $filtro)->ordenados()->get();
        } else {
            $registros = Aula::leftJoin('aulas_categorias as cat', 'cat.id', '=', 'aulas_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('aulas.*')
                ->ordenados()->get();
        }

        return view('painel.aulas.index', compact('categorias', 'registros', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.aulas.create', compact('categorias'));
    }

    public function store(AulasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Aula::upload_capa();

            Aula::create($input);

            return redirect()->route('painel.aulas.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Aula $registro)
    {
        $categorias = $this->categorias;

        return view('painel.aulas.edit', compact('registro', 'categorias'));
    }

    public function update(AulasRequest $request, Aula $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Aula::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.aulas.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Aula $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.aulas.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
