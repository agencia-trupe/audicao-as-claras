<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RespostasCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\RespostaCategoria;

class RespostasCategoriasController extends Controller
{
    public function index()
    {
        $categorias = RespostaCategoria::ordenados()->get();

        return view('painel.respostas.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.respostas.categorias.create');
    }

    public function store(RespostasCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            RespostaCategoria::create($input);
            return redirect()->route('painel.respostas.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(RespostaCategoria $categoria)
    {
        return view('painel.respostas.categorias.edit', compact('categoria'));
    }

    public function update(RespostasCategoriasRequest $request, RespostaCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.respostas.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(RespostaCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.respostas.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
