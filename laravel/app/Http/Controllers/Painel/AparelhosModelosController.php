<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AparelhosModelosRequest;
use App\Http\Controllers\Controller;

use App\Models\AparelhoModelo;

class AparelhosModelosController extends Controller
{
    public function index()
    {
        $registros = AparelhoModelo::ordenados()->get();

        return view('painel.aparelhos.modelos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.aparelhos.modelos.create');
    }

    public function store(AparelhosModelosRequest $request)
    {
        try {

            $input = $request->all();


            AparelhoModelo::create($input);
            return redirect()->route('painel.aparelhos.modelos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(AparelhoModelo $registro)
    {
        return view('painel.aparelhos.modelos.edit', compact('registro'));
    }

    public function update(AparelhosModelosRequest $request, AparelhoModelo $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.aparelhos.modelos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(AparelhoModelo $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.aparelhos.modelos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
