<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Download;

class DownloadsController extends Controller
{
    public function index()
    {
        $downloads = Download::orderBy('email', 'ASC')->paginate(20);

        return view('painel.downloads.index', compact('downloads'));
    }

    public function destroy(Download $download)
    {
        try {

            $download->delete();
            return redirect()->route('painel.downloads.index')->with('success', 'Dados excluídos com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir dados: '.$e->getMessage()]);

        }
    }

    public function exportar()
    {
        $file = 'audicaoasclaras-downloads_'.date('d-m-Y_His');

        \Excel::create($file, function($excel) {
            $excel->sheet('downloads', function($sheet) {
                $sheet->fromModel(Download::orderBy('email', 'ASC')->get(['nome', 'email']));
            });
        })->download('csv');
    }
}
