<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Consulta;

class ConsultasController extends Controller
{
    public function index()
    {
        $consultas = Consulta::orderBy('created_at', 'DESC')->paginate(20);

        return view('painel.consultas.index', compact('consultas'));
    }

    public function destroy(Consulta $consulta)
    {
        try {

            $consulta->delete();
            return redirect()->route('painel.consultas.index')->with('success', 'Dados excluídos com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir dados: '.$e->getMessage()]);

        }
    }

    public function exportar()
    {
        $file = 'audicaoasclaras-consultas_'.date('d-m-Y_His');

        $consultas = [];
        foreach (Consulta::orderBy('id', 'ASC')->get() as $row) {
            $consultas[] = [
                'Nome'                              => $row->nome,
                'Telefone'                          => $row->telefone,
                'E-mail'                            => $row->email,
                'CEP'                               => $row->cep,
                'Conversar com uma pessoa'          => $row->conversar_com_uma_pessoa ? 'x' : '',
                'Ambientes internos'                => $row->ambientes_internos ? 'x' : '',
                'Conversar em grupo'                => $row->conversar_em_grupo ? 'x' : '',
                'Falar ao telefone'                 => $row->falar_ao_telefone ? 'x' : '',
                'Assistir tv'                       => $row->assistir_tv ? 'x' : '',
                'Ambientes ao ar livre'             => $row->ambientes_ao_ar_livre ? 'x' : '',
                'Shows, concertos e teatro'         => $row->showsconcertos_e_teatro ? 'x' : '',
                'Restaurantes'                      => $row->restaurantes ? 'x' : '',
                'Festas e ambientes muito ruidosos' => $row->festas_e_ambientes_muito_ruidosos ? 'x' : '',
            ];
        }

        \Excel::create($file, function($excel) use ($consultas) {
            $excel->sheet('consultas', function($sheet) use ($consultas) {
                $sheet->fromArray($consultas);
            });
        })->download('csv');
    }
}
