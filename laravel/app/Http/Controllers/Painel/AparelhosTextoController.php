<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AparelhosTextoRequest;
use App\Http\Controllers\Controller;

use App\Models\AparelhosTexto;

class AparelhosTextoController extends Controller
{
    public function index()
    {
        $registro = AparelhosTexto::first();

        return view('painel.aparelhos-texto.edit', compact('registro'));
    }

    public function update(AparelhosTextoRequest $request, AparelhosTexto $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.aparelhos-texto.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
