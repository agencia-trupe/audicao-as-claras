<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\BlogComentariosRequest;

use App\Models\BlogPost;
use App\Models\BlogCategoria;

use App\Models\Ebook;
use App\Models\Download;

use App\Models\Contato;

class BlogController extends Controller
{
    public function __construct()
    {
        $categorias = BlogCategoria::ordenados()->get();
        $arquivos   = BlogPost::select(\DB::raw('YEAR(data) as ano, MONTH(data) as mes'))
                               ->groupBy('ano', 'mes')
                               ->orderBy('ano', 'DESC')
                               ->orderBy('mes', 'DESC')
                               ->get();
        $ebook      = Ebook::first();

        view()->share('categorias', $categorias);
        view()->share('arquivos', $arquivos);
        view()->share('ebook', $ebook);
    }

    public function index(BlogCategoria $categoria)
    {
        if (!$categoria->exists) {
            $posts = BlogPost::ordenados()->simplePaginate(10);
            $destaque = BlogPost::ordenados()->first();
            if (!$destaque) $destaque = null;
        } else {
            $posts = $categoria->posts()->simplePaginate(10);
            $destaque = null;
        }

        return view('frontend.blog.index', compact('categoria', 'posts', 'destaque'));
    }

    public function show(BlogCategoria $categoria, BlogPost $post)
    {
        $leiaTambem = BlogPost::ordenados()->where('id', '!=', $post->id)->take(4)->get();

        return view('frontend.blog.show', compact('categoria', 'post', 'leiaTambem'));
    }

    public function arquivo($ano = null, $mes = null)
    {
        if (!$ano || !$mes) abort('404');

        $posts = BlogPost::ordenados()
                   ->whereRaw("YEAR(data) = $ano AND MONTH(data) = $mes")
                   ->paginate(10);

        return view('frontend.blog.index', compact('posts', 'ano', 'mes'));
    }

    public function comentario(BlogComentariosRequest $request)
    {
        try {

            $post = BlogPost::findOrFail($request->get('post'));
            $post->comentarios()->create($request->except('post'));

            $contato = Contato::first();

            if (isset($contato->email)) {
                \Mail::send('emails.comentario', [
                    'comentario' => $request->all(),
                    'post'       => $post
                ], function($message) use ($request, $contato) {
                    $message->to($contato->email, config('site.name'))
                            ->subject('[NOVO COMENTÁRIO] '.config('site.name'));
                });
            }

        } catch (\Exception $e) {
            $response = [
                'message' => 'Ocorreu um erro. Tente novamente.'
            ];

            return response()->json($response);

        }

        $response = [
            'success' => true,
            'message' => 'Comentário enviado com sucesso. Sujeito à aprovação da moderação.'
        ];

        return response()->json($response);
    }

    public function ebook()
    {
        return view('frontend.blog.ebook');
    }

    public function ebookPost(Request $request)
    {
        $this->validate($request, [
            'nome'     => 'required',
            'email'    => 'required|email',
        ]);

        $ebook = Ebook::first();

        $input = $request->all();

        if (!Download::where('email', $input['email'])->exists()) {
            Download::create($input);
        }

        return response()->json([
            'link' => url('assets/ebook/'.$ebook->arquivo)
        ]);
    }
}
