<?php

namespace App\Http\Controllers\EstabelecimentoAuth;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\Estabelecimento;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\EstabelecimentosPublicRequest;
use App\Models\AparelhoMarca;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/estabelecimento';
    protected $guard      = 'estabelecimento';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest.estabelecimento', ['except' => 'logout']);
    }

    public function showRegistrationForm()
    {
        $marcas = AparelhoMarca::ordenados()->get();

        return view('estabelecimento.cadastro', compact('marcas'));
    }

    public function register(EstabelecimentosPublicRequest $request)
    {
        $input = $request->except('marcas');

        try {
            $endereco = "${input['endereco']} ${input['numero']} - ${input['bairro']}, ${input['cidade']} - ${input['uf']}";
            $coordenadas = \Tools::fetchCoordinates($endereco);
            $input['lat'] = $coordenadas->lat;
            $input['lng'] = $coordenadas->lng;
        } catch(\Exception $e) {
            return back()->withInput()->with('erroCoord', $e->getMessage());
        }

        $input['senha'] = bcrypt($input['senha']);

        $estabelecimento = Estabelecimento::create($input);
        $estabelecimento->marcas()->sync(request('marcas') ?: []);

        return redirect()->route('home')->with('cadastroEspecialistaRedirect', true);
    }

    public function showLoginForm()
    {
        return view('estabelecimento.login');
    }

    protected function login(Request $request)
    {
        if (Auth::guard('estabelecimento')->attempt([
            'email'    => $request->input('email'),
            'password' => $request->input('password')
        ], true)) {
            if (!Auth::guard('estabelecimento')->user()->aprovado) {
                Auth::guard('estabelecimento')->logout();
                return redirect()->route('estabelecimento.auth')->with('error', 'Seu cadastro está em processo de aprovação pelos administradores.');
            }
            return redirect()->intended('estabelecimento');
        } else {
            return redirect()->route('estabelecimento.auth')->withInput()->with('error', 'Usuário ou senha inválidos.');
        }
    }
}
