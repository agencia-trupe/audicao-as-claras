<?php

Route::group(['middleware' => ['web']], function () {
    Route::group([
        'prefix'     => 'especialista',
        'namespace'  => 'Especialista',
        'middleware' => ['auth.especialista']
    ], function () {
        Route::get('/', 'PainelController@index')->name('especialista');
        Route::patch('meus-dados', 'PainelController@update')->name('especialista.update');
        Route::get('avisos', 'PainelController@avisos')->name('especialista.avisos');
        Route::get('aulas/{aula_categoria?}', 'PainelController@aulas')->name('especialista.aulas');
        Route::get('aulas/{aula_categoria}/{aula_slug}', 'PainelController@aula')->name('especialista.aula');
        Route::get('responder-pergunta', 'PainelController@perguntas')->name('especialista.perguntas');
        Route::get('responder-pergunta/{id}', 'PainelController@form')->name('especialista.perguntas.form');
        Route::post('responder-pergunta/{id}', 'PainelController@post')->name('especialista.perguntas.post');
        Route::get('minhas-respostas', 'PainelController@minhasRespostas')->name('especialista.perguntas.minhas-respostas');
    });

    Route::group([
        'prefix'    => 'especialista',
        'namespace' => 'EspecialistaAuth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('especialista.auth');
        Route::post('login', 'AuthController@login')->name('especialista.login');
        Route::get('logout', 'AuthController@logout')->name('especialista.logout');
        Route::get('cadastro', 'AuthController@showRegistrationForm')->name('especialista.cadastro');
        Route::post('cadastro', 'AuthController@register')->name('especialista.cadastrar');
        Route::get('esqueci-minha-senha', 'PasswordController@esqueci')->name('especialista.esqueci');
        Route::post('redefinicao-de-senha', 'PasswordController@sendResetLinkEmail')->name('especialista.redefinicao');
        Route::get('redefinicao-de-senha/{token}', 'PasswordController@showResetForm');
        Route::post('redefinir-senha', 'PasswordController@reset')->name('especialista.redefinir');
    });
});
