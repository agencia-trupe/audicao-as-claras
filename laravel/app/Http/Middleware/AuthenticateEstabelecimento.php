<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateEstabelecimento
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'estabelecimento')
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('estabelecimento/login');
            }
        } elseif (!Auth::guard('estabelecimento')->user()->aprovado) {
            Auth::guard('estabelecimento')->logout();
            return redirect()->guest('estabelecimento/login');
        }

        return $next($request);
    }
}
