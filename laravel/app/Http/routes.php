<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');
    Route::get('quem-somos', 'EmpresaController@index')->name('empresa');
    Route::get('aparelhos-auditivos', 'AparelhosController@index')->name('aparelhos');
    Route::post('aparelhos-auditivos', 'AparelhosController@post')->name('aparelhos.post');
    Route::get('aparelhos-auditivos/estabelecimentos', 'AparelhosController@estabelecimentos')->name('aparelhos.estabelecimentos');
    Route::get('aparelhos-auditivos/telefone/{id}', 'AparelhosController@telefone')->name('aparelhos.telefone');
    Route::get('especialistas/telefone/{id}', 'AparelhosController@telefoneEspecialista')->name('especialistas.telefone');

    Route::group([
        'middleware' => ['auth.cadastro']
    ], function () {
        Route::get('aparelhos-auditivos/consulta/{n1}/{n2}/{n3}/{n4}/{n5}/{n6}/{n7}/{n8}/{n9}', 'AparelhosController@consulta')->name('aparelhos.consulta');
        Route::get('aparelhos-auditivos/detalhes/{id}/{n1}/{n2}/{n3}/{n4}/{n5}/{n6}/{n7}/{n8}/{n9}', 'AparelhosController@show')->name('aparelhos.show');
        Route::post('avaliacao/{tipo}', 'HomeController@avaliar')->name('avaliacao');
    });

    Route::get('faq', 'FaqController@index')->name('faq');
    Route::get('pergunte-ao-especialista/{respostas_categoria?}', 'PergunteController@index')->name('pergunte');
    Route::post('pergunte-ao-especialista', 'PergunteController@post')->name('pergunte.post');
    Route::get('blog/{blog_categoria?}', 'BlogController@index')->name('blog');
    Route::get('blog/{blog_categoria}/{post_slug}', 'BlogController@show')->name('blog.show');
    Route::get('blog/a/{ano}/{mes}', 'BlogController@arquivo')->name('blog.arquivo');
    Route::post('blog-comentario', 'BlogController@comentario');
    Route::get('ebook', 'BlogController@ebook')->name('ebook');
    Route::post('ebook', 'BlogController@ebookPost')->name('ebook.post');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');

    // Cadastro
    Route::get('selecao-login', 'HomeController@selecaoLogin')->name('selecaoLogin');
    Route::get('login', 'HomeController@login')->name('cadastro.login');
    Route::get('cadastro', 'HomeController@cadastro')->name('cadastro.cadastro');
    Route::post('cadastro', 'CadastroAuth\AuthController@create')->name('cadastro.cadastroPost');
    Route::post('login', 'CadastroAuth\AuthController@login')->name('cadastro.loginPost');
    Route::get('logout', 'CadastroAuth\AuthController@logout')->name('cadastro.logout');
    Route::get('esqueci-minha-senha', 'HomeController@esqueci')->name('cadastro.esqueci');
    Route::post('redefinicao-de-senha', 'CadastroAuth\PasswordController@sendResetLinkEmail')->name('cadastro.redefinicao');
    Route::get('redefinicao-de-senha/{token}', 'CadastroAuth\PasswordController@showResetForm');
    Route::post('redefinir-senha', 'CadastroAuth\PasswordController@reset')->name('cadastro.redefinir');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::resource('aulas/categorias', 'AulasCategoriasController', ['parameters' => ['categorias' => 'categorias_aulas']]);
        Route::resource('aulas', 'AulasController');
        Route::resource('aulas.downloads', 'AulasDownloadsController', ['parameters' => ['downloads' => 'downloads_aulas']]);
        Route::resource('avisos', 'AvisosController');
        Route::resource('cabecalho', 'CabecalhoController', ['only' => ['index', 'update']]);
        Route::resource('aparelhos-texto', 'AparelhosTextoController', ['only' => ['index', 'update']]);
        Route::get('ebook/delete', 'EbookController@delete')->name('painel.ebook.delete');
        Route::resource('ebook', 'EbookController', ['only' => ['index', 'update']]);
        Route::get('downloads/exportar', ['as' => 'painel.downloads.exportar', 'uses' => 'DownloadsController@exportar']);
        Route::resource('downloads', 'DownloadsController');
        Route::get('consultas/exportar', ['as' => 'painel.consultas.exportar', 'uses' => 'ConsultasController@exportar']);
        Route::resource('consultas', 'ConsultasController');
        Route::get('estabelecimentos/{estabelecimentos}/aprovar', 'EstabelecimentosController@aprovar')->name('painel.estabelecimentos.aprovar');
        Route::get('estabelecimentos/exibicoes', 'EstabelecimentosController@exibicoes')->name('painel.estabelecimentos.exibicoes');
        Route::resource('estabelecimentos', 'EstabelecimentosController', [
            'except' => ['create', 'store']
        ]);
        Route::resource('aparelhos/marcas', 'AparelhosMarcasController');
        Route::resource('aparelhos/modelos', 'AparelhosModelosController');
        Route::resource('aparelhos', 'AparelhosController');
        Route::resource('resposta-em-destaque', 'RespostaEmDestaqueController', ['only' => ['index', 'update']]);
        Route::resource('respostas/categorias', 'RespostasCategoriasController', ['parameters' => ['categorias' => 'categorias_respostas']]);
        Route::resource('respostas', 'RespostasController');
        Route::get('respostas/{respostas}/avaliar', 'RespostasController@avaliar')->name('painel.respostas.avaliar');
        Route::get('respostas/{respostas}/rejeitar', 'RespostasController@rejeitar')->name('painel.respostas.rejeitar');
        Route::get('especialistas/exibicoes', 'EspecialistasController@exibicoes')->name('painel.especialistas.exibicoes');
        Route::resource('especialistas', 'EspecialistasController', ['only' => ['index', 'show', 'destroy']]);
        Route::get('especialistas/{especialistas}/aprovar', 'EspecialistasController@aprovar')->name('painel.especialistas.aprovar');
        Route::get('perguntas-recebidas/{perguntas_recebidas}/toggle', ['as' => 'painel.perguntas-recebidas.toggle', 'uses' => 'PerguntasRecebidasController@toggle']);
        Route::resource('perguntas-recebidas', 'PerguntasRecebidasController');
        Route::resource('blog/categorias', 'BlogCategoriasController');
        Route::resource('blog', 'BlogPostsController');
        Route::get('blog/{blog}/comentarios/{comentarios}/aprovacao', 'BlogComentariosController@aprovacao')->name('painel.blog.comentarios.aprovacao');
        Route::resource('blog.comentarios', 'BlogComentariosController');
        Route::resource('chamadas-home', 'ChamadasHomeController', ['only' => ['index', 'update']]);
        Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
        Route::resource('faq', 'FaqController');

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
        Route::resource('newsletter', 'NewsletterController');
        Route::resource('usuarios', 'UsuariosController');

        Route::get('cadastros/exportar', ['as' => 'painel.cadastros.exportar', 'uses' => 'CadastrosController@exportar']);
        Route::resource('cadastros', 'CadastrosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth Painel
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
