<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Aula extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'aulas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('aulas_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\AulaCategoria', 'aulas_categoria_id');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 300,
            'height' => 185,
            'path'   => 'assets/img/aulas/'
        ]);
    }

    public function downloads()
    {
        return $this->hasMany('App\Models\AulaDownload', 'aula_id')->ordenados();
    }
}
