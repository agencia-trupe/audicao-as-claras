<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Helpers\CropImage;

class Especialista extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'especialistas';

    protected $guarded = ['id', 'senha_confirmation'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeAprovados($query)
    {
        return $query->where('aprovado', 1);
    }

    public function scopeBusca($query, $value)
    {
        return $query
            ->where('nome', 'LIKE', '%'.$value.'%')
            ->orWhere('endereco', 'LIKE', '%'.$value.'%')
            ->orWhere('bairro', 'LIKE', '%'.$value.'%')
            ->orWhere('cidade', 'LIKE', '%'.$value.'%')
            ->orWhere('uf', 'LIKE', '%'.$value.'%');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 175,
            'height' => 175,
            'path'   => 'assets/img/especialistas/'
        ]);
    }

    public function getAuthPassword()
    {
        return $this->senha;
    }

    public static function profissoes()
    {
        return [
            'Médico',
            'Especialista'
        ];
    }

    public function avaliacoes()
    {
        return $this->hasMany('App\Models\EspecialistaAvaliacao', 'especialista_id');
    }

    public function getAvaliacaoAttribute()
    {
        if (auth('cadastro')->check()) {
            return $this->avaliacoes
                ->where('cadastro_id', auth('cadastro')->user()->id)
                ->first();
        }
    }
}
