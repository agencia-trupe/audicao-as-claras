<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RespostaAvaliacao extends Model
{
    protected $table = 'respostas_avaliacoes';

    protected $guarded = ['id'];
}
