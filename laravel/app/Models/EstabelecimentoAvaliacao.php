<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstabelecimentoAvaliacao extends Model
{
    protected $table = 'estabelecimentos_avaliacoes';

    protected $guarded = ['id'];
}
