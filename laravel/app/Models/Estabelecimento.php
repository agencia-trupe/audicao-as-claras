<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Estabelecimento extends Model  implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'estabelecimentos';

    protected $guarded = ['id', 'senha_confirmation'];

    public function scopeAprovados($query)
    {
        return $query->where('aprovado', 1);
    }

    public function scopeBusca($query, $value)
    {
        return $query
            ->where('nome', 'LIKE', '%'.$value.'%')
            ->orWhere('endereco', 'LIKE', '%'.$value.'%')
            ->orWhere('bairro', 'LIKE', '%'.$value.'%')
            ->orWhere('cidade', 'LIKE', '%'.$value.'%')
            ->orWhere('uf', 'LIKE', '%'.$value.'%');
    }

    public function aparelhos()
    {
        return $this->belongsToMany('App\Models\Aparelho', 'estabelecimento_aparelho', 'estabelecimento_id', 'aparelho_id');
    }

    public function marcas()
    {
        return $this->belongsToMany('App\Models\AparelhoMarca', 'estabelecimento_marca', 'estabelecimento_id', 'marca_id');
    }

    public function avaliacoes()
    {
        return $this->hasMany('App\Models\EstabelecimentoAvaliacao', 'estabelecimento_id');
    }

    public function getAvaliacaoAttribute()
    {
        if (auth('cadastro')->check()) {
            return $this->avaliacoes
                ->where('cadastro_id', auth('cadastro')->user()->id)
                ->first();
        }
    }

    public function getAuthPassword()
    {
        return $this->senha;
    }
}
