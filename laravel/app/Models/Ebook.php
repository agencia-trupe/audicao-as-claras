<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Ebook extends Model
{
    protected $table = 'ebook';

    protected $guarded = ['id'];

    public static function uploadFile() {
        $file = request()->file('arquivo');

        $path = 'assets/ebook/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);

        return $name;
    }
}
