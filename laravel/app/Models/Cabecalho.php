<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cabecalho extends Model
{
    protected $table = 'cabecalho';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 400,
            'height' => 180,
            'path'   => 'assets/img/cabecalho/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 400,
            'height' => 180,
            'path'   => 'assets/img/cabecalho/'
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 400,
            'height' => 180,
            'path'   => 'assets/img/cabecalho/'
        ]);
    }

    public static function upload_imagem_4()
    {
        return CropImage::make('imagem_4', [
            'width'  => 400,
            'height' => 180,
            'path'   => 'assets/img/cabecalho/'
        ]);
    }

    public static function upload_imagem_5()
    {
        return CropImage::make('imagem_5', [
            'width'  => 400,
            'height' => 180,
            'path'   => 'assets/img/cabecalho/'
        ]);
    }

}
