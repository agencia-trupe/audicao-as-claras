<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class RespostaEmDestaque extends Model
{
    protected $table = 'resposta_em_destaque';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 480,
            'height' => 320,
            'path'   => 'assets/img/resposta-em-destaque/'
        ]);
    }

    public function respostaModel()
    {
        return $this->hasOne('App\Models\Resposta', 'id', 'pergunta');
    }
}
