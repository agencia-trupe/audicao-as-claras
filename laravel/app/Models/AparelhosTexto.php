<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class AparelhosTexto extends Model
{
    protected $table = 'aparelhos_texto';

    protected $guarded = ['id'];

}
