<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EspecialistaAvaliacao extends Model
{
    protected $table = 'especialistas_avaliacoes';

    protected $guarded = ['id'];
}
