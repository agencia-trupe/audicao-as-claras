<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Resposta extends Model
{
    protected $table = 'respostas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('respostas_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\RespostaCategoria', 'respostas_categoria_id');
    }

    public function especialistaModel()
    {
        return $this->belongsTo('App\Models\Especialista', 'especialista');
    }

    public function avaliacoes()
    {
        return $this->hasMany('App\Models\RespostaAvaliacao', 'resposta_id');
    }

    public function getAvaliacaoAttribute()
    {
        if (auth('cadastro')->check()) {
            return $this->avaliacoes
                ->where('cadastro_id', auth('cadastro')->user()->id)
                ->first();
        }
    }
}
