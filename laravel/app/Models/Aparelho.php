<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Aparelho extends Model
{
    protected $table = 'aparelhos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'   => 120,
            'height'  => 120,
            'bgcolor' => '#ffffff',
            'path'    => 'assets/img/aparelhos/'
        ]);
    }

    public static function upload_ampliacao()
    {
        return CropImage::make('ampliacao', [
            'width'   => 370,
            'height'  => 370,
            'bgcolor' => '#ffffff',
            'path'    => 'assets/img/aparelhos/ampliacao/'
        ]);
    }

    public function aparelhos()
    {
        return $this->belongsToMany('App\Models\Estabelecimento', 'estabelecimento_aparelho', 'aparelho_id', 'estabelecimento_id');
    }

    public function marca()
    {
        return $this->belongsTo('App\Models\AparelhoMarca', 'marca_id');
    }

    public function modelo()
    {
        return $this->belongsTo('App\Models\AparelhoModelo', 'modelo_id');
    }

    public function avaliacoes()
    {
        return $this->hasMany('App\Models\AparelhoAvaliacao', 'aparelho_id');
    }

    public function getAvaliacaoAttribute()
    {
        if (auth('cadastro')->check()) {
            return $this->avaliacoes
                ->where('cadastro_id', auth('cadastro')->user()->id)
                ->first();
        }
    }

    public function getAvaliacaoRealAttribute()
    {
        return $this->avaliacao_clientes ?: $this->classificacao;
    }
}
