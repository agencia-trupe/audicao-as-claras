<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AulaDownload extends Model
{
    protected $table = 'aulas_downloads';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_arquivo()
    {
        $file = request()->file('arquivo');

        $path = 'downloads/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);

        return $name;
    }

    public function scopeAula($query, $id)
    {
        return $query->where('aula_id', $id);
    }
}
