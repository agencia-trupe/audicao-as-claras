<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ChamadasHome extends Model
{
    protected $table = 'chamadas_home';

    protected $guarded = ['id'];

}
