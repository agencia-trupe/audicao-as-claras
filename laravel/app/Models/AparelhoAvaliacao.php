<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AparelhoAvaliacao extends Model
{
    protected $table = 'aparelhos_avaliacoes';

    protected $guarded = ['id'];
}
