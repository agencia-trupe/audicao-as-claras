<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('politica-de-privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('aulas', 'App\Models\Aula');
        $router->model('categorias_aulas', 'App\Models\AulaCategoria');
        $router->model('downloads_aulas', 'App\Models\AulaDownload');
        $router->model('avisos', 'App\Models\Aviso');
        $router->model('cabecalho', 'App\Models\Cabecalho');
        $router->model('aparelhos-texto', 'App\Models\AparelhosTexto');
        $router->model('ebook', 'App\Models\Ebook');
        $router->model('estabelecimentos', 'App\Models\Estabelecimento');
        $router->model('marcas', 'App\Models\AparelhoMarca');
        $router->model('modelos', 'App\Models\AparelhoModelo');
        $router->model('aparelhos', 'App\Models\Aparelho');
        $router->model('consultas', 'App\Models\Consulta');
        $router->model('resposta-em-destaque', 'App\Models\RespostaEmDestaque');
        $router->model('respostas', 'App\Models\Resposta');
        $router->model('categorias_respostas', 'App\Models\RespostaCategoria');
        $router->model('especialistas', 'App\Models\Especialista');
        $router->model('comentarios', 'App\Models\BlogComentario');
        $router->model('blog', 'App\Models\BlogPost');
        $router->model('categorias', 'App\Models\BlogCategoria');
        $router->model('chamadas-home', 'App\Models\ChamadasHome');
        $router->model('empresa', 'App\Models\Empresa');
        $router->model('faq', 'App\Models\Pergunta');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('downloads', 'App\Models\Download');
        $router->model('usuarios', 'App\Models\User');
        $router->model('cadastros', 'App\Models\Cadastro');

        $router->bind('respostas_categoria', function ($value) {
            return \App\Models\RespostaCategoria::whereSlug($value)->first() ?: abort('404');
        });

        $router->bind('blog_categoria', function ($value) {
            return \App\Models\BlogCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('post_slug', function ($value) {
            return \App\Models\BlogPost::whereSlug($value)->first() ?: abort('404');
        });

        $router->bind('aula_categoria', function ($value) {
            return \App\Models\AulaCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('aula_slug', function ($value) {
            return \App\Models\Aula::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
            require app_path('Http/routes.especialistas.php');
            require app_path('Http/routes.estabelecimentos.php');
        });
    }
}
