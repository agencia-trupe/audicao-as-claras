<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.common.template', function ($view) {
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer([
            'frontend.common.header',
            'frontend.home',
            'frontend.empresa'
        ], function ($view) {
            $view->with('cabecalho', \App\Models\Cabecalho::first());
        });

        view()->composer('painel.common.nav', function ($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });

        view()->composer('especialista.perguntas*', function ($view) {
            $view->with('emAprovacao', \App\Models\Resposta::where('aprovado', 0)->where('especialista', \Auth::guard('especialista')->user()->id)->count());
            $view->with('minhasRespostas', \App\Models\Resposta::where('aprovado', 1)->where('especialista', \Auth::guard('especialista')->user()->id)->count());
        });

        \Validator::extend('cep', function ($attribute, $value, $parameters, $validator) {
            return preg_match("/^[0-9]{5}-[0-9]{3}$/", trim($value)) || preg_match("/^[0-9]{8}$/", trim($value));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
