<?php

namespace App\Helpers;

use Location\Coordinate;
use Location\Distance\Haversine;

class Tools
{

    public static function loadJquery()
    {
        return '<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>'."\n\t".'<script>window.jQuery || document.write(\'<script src="' . asset("assets/vendor/jquery/dist/jquery.min.js") . '"><\/script>\')</script>';
    }

    public static function loadJs($path)
    {
        return '<script src="' . asset('assets/'.$path) .'"></script>';
    }

    public static function loadCss($path)
    {
        return '<link rel="stylesheet" href="' . asset('assets/'.$path) . '">';
    }

    public static function isActive($routeName)
    {
        return str_is($routeName, \Route::currentRouteName());
    }

    public static function formataData($data)
    {
        $meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        $meses = array_map(function($mes) {
            return substr(mb_strtoupper($mes), 0, 3);
        }, $meses);

        list($dia, $mes, $ano) = explode('/', $data);

        return $dia . ' ' . $meses[(int) $mes - 1] . ' ' . $ano;
    }

    public static function listaEstados()
    {
        return [
            'AC' => 'AC',
            'AL' => 'AL',
            'AP' => 'AP',
            'AM' => 'AM',
            'BA' => 'BA',
            'CE' => 'CE',
            'DF' => 'DF',
            'ES' => 'ES',
            'GO' => 'GO',
            'MA' => 'MA',
            'MT' => 'MT',
            'MS' => 'MS',
            'MG' => 'MG',
            'PA' => 'PA',
            'PB' => 'PB',
            'PR' => 'PR',
            'PE' => 'PE',
            'PI' => 'PI',
            'RJ' => 'RJ',
            'RN' => 'RN',
            'RS' => 'RS',
            'RO' => 'RO',
            'RR' => 'RR',
            'SC' => 'SC',
            'SP' => 'SP',
            'SE' => 'SE',
            'TO' => 'TO'
        ];
    }

    public static function formataArquivo($ano, $mes)
    {
        $meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        return mb_strtolower($meses[(int) $mes - 1]) . ' ' . $ano;
    }

    public static function geraRotaDetalhes($aparelhoId, $necessidades)
    {
        return route('aparelhos.show', [
            $aparelhoId,
            array_key_exists('conversar_com_uma_pessoa', $necessidades) ? $necessidades['conversar_com_uma_pessoa'] : 0,
            array_key_exists('ambientes_internos', $necessidades) ? $necessidades['ambientes_internos'] : 0,
            array_key_exists('conversar_em_grupo', $necessidades) ? $necessidades['conversar_em_grupo'] : 0,
            array_key_exists('falar_ao_telefone', $necessidades) ? $necessidades['falar_ao_telefone'] : 0,
            array_key_exists('assistir_tv', $necessidades) ? $necessidades['assistir_tv'] : 0,
            array_key_exists('ambientes_ao_ar_livre', $necessidades) ? $necessidades['ambientes_ao_ar_livre'] : 0,
            array_key_exists('showsconcertos_e_teatro', $necessidades) ? $necessidades['showsconcertos_e_teatro'] : 0,
            array_key_exists('restaurantes', $necessidades) ? $necessidades['restaurantes'] : 0,
            array_key_exists('festas_e_ambientes_muito_ruidosos', $necessidades) ? $necessidades['festas_e_ambientes_muito_ruidosos'] : 0,
        ]);
    }

    public static function fetchCoordinates($address)
    {
        $address = str_replace(' ', '+', $address);

        $url = 'https://maps.google.com/maps/api/geocode/json?key=AIzaSyDSMVsMnX4RDXTExCEney-SXnWhZkxtW2o&address='.urlencode($address);

        try {
            $responseGoogle = file_get_contents($url);
        } catch(\Exception $e) {
            throw new \Exception('Ocorreu um problema ao encontrar o endereço. Tente novamente.');
        }

        $responseGoogle = json_decode($responseGoogle);

        if (! count($responseGoogle->results)) {
            throw new \Exception('Endereço não encontrado. Tente novamente.');
        }

        $coordinates = $responseGoogle->results[0]->geometry->location;

        return $coordinates;
    }

    public static function sortByCoordinates($collection, $coordinates)
    {
        return $collection->sortBy(function($item) use ($coordinates) {
            $coordinate1 = new Coordinate($item->lat, $item->lng);
            $coordinate2 = new Coordinate($coordinates->lat, $coordinates->lng);
            return $coordinate1->getDistance($coordinate2, new Haversine());
        });
    }
}
