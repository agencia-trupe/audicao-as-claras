<?php

use Illuminate\Database\Seeder;
use App\Models\Especialista;

class EspecialistasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Especialista::class, 20)->create();
    }
}
