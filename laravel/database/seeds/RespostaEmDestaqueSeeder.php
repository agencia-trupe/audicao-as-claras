<?php

use Illuminate\Database\Seeder;

class RespostaEmDestaqueSeeder extends Seeder
{
    public function run()
    {
        DB::table('resposta_em_destaque')->insert([
            'imagem' => '',
            'pergunta' => null,
        ]);
    }
}
