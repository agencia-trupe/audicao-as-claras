<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(PoliticaDePrivacidadeSeeder::class);
        $this->call(CabecalhoSeeder::class);
        $this->call(AparelhosTextoSeeder::class);
        $this->call(EbookSeeder::class);
        $this->call(RespostaEmDestaqueSeeder::class);
        $this->call(ChamadasHomeSeeder::class);
        $this->call(EmpresaSeeder::class);
        $this->call(ContatoTableSeeder::class);
        // $this->call(EspecialistasTableSeeder::class);

        Model::reguard();
    }
}
