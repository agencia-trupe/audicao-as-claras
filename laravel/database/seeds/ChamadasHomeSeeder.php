<?php

use Illuminate\Database\Seeder;

class ChamadasHomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('chamadas_home')->insert([
            'titulo' => '',
            'chamada_1_texto' => '',
            'chamada_1_botao' => '',
            'chamada_1_link' => '',
            'chamada_2_texto' => '',
            'chamada_2_botao' => '',
            'chamada_2_link' => '',
        ]);
    }
}
