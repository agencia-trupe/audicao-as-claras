<?php

use Illuminate\Database\Seeder;

class CabecalhoSeeder extends Seeder
{
    public function run()
    {
        DB::table('cabecalho')->insert([
            'frase' => '',
            'imagem_1' => '',
            'imagem_2' => '',
            'imagem_3' => '',
            'imagem_4' => '',
            'imagem_5' => '',
        ]);
    }
}
