<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Especialista::class, function (Faker\Generator $faker) {
    return [
        'nome'                           => $faker->name,
        'email'                          => $faker->email,
        'cpf'                            => $faker->regexify('\d{3}\.\d{3}\.\d{3}-\d{2}'),
        'profissao_cargo'                => $faker->randomElement(['Médico', 'Especialista', 'Fornecedor']),
        'telefone'                       => $faker->phoneNumber,
        'cep'                            => $faker->regexify('\d{5}-\d{3}'),
        'endereco'                       => $faker->streetAddress,
        'numero'                         => $faker->randomNumber,
        'complemento'                    => $faker->secondaryAddress,
        'bairro'                         => $faker->word,
        'cidade'                         => $faker->city,
        'uf'                             => $faker->stateAbbr,
        'nome_exibicao'                  => $faker->firstName,
        'descricao'                      => $faker->paragraph,
        'imagem'                         => $faker->word,
        'senha'                          => bcrypt('123123'),

        'fornecedor_nome_fantasia'       => $faker->name,
        'fornecedor_razao_social'        => $faker->name,
        'fornecedor_cnpj'                => $faker->regexify('\d{3}\.\d{3}\.\d{3}-\d{2}'),
        'fornecedor_area_de_atuacao'     => $faker->word,
        'fornecedor_telefone'            => $faker->phoneNumber,
        'fornecedor_contato_responsavel' => $faker->firstName,
        'fornecedor_cep'                 => $faker->regexify('\d{5}-\d{3}'),
        'fornecedor_endereco'            => $faker->streetAddress,
        'fornecedor_numero'              => $faker->randomNumber,
        'fornecedor_complemento'         => $faker->secondaryAddress,
        'fornecedor_bairro'              => $faker->word,
        'fornecedor_cidade'              => $faker->city,
        'fornecedor_uf'                  => $faker->stateAbbr,
        'fornecedor_website'             => $faker->url,
    ];
});

$factory->define(App\Models\Cadastro::class, function (Faker\Generator $faker) {
    return [
        'nome'     => $faker->name,
        'email'    => $faker->email,
        'telefone' => $faker->phoneNumber,
        'cep'      => $faker->regexify('\d{5}-\d{3}'),
        'senha'    => bcrypt('123123'),
    ];
});