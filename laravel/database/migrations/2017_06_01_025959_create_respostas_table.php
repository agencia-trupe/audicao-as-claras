<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespostasTable extends Migration
{
    public function up()
    {
        Schema::create('respostas_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('respostas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->text('pergunta');

            $table->integer('especialista')->unsigned()->nullable();
            $table->foreign('especialista')->references('id')->on('especialistas')->onDelete('set null');
            $table->text('resposta');
            
            $table->boolean('aprovado')->default(0);
            $table->integer('respostas_categoria_id')->unsigned()->nullable();
            $table->foreign('respostas_categoria_id')->references('id')->on('respostas_categorias')->onDelete('set null');

            $table->integer('avaliacao_clientes')->default(0);
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('respostas');
        Schema::drop('respostas_categorias');
    }
}
