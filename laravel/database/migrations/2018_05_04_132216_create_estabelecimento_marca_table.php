<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstabelecimentoMarcaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estabelecimento_marca', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estabelecimento_id')->unsigned();
            $table->integer('marca_id')->unsigned();
            $table->timestamps();
            $table->foreign('estabelecimento_id')->references('id')->on('estabelecimentos');
            $table->foreign('marca_id')->references('id')->on('aparelhos_marcas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estabelecimento_marca');
    }
}
