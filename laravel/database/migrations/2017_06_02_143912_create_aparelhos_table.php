<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAparelhosTable extends Migration
{
    public function up()
    {
        Schema::create('aparelhos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('marca_id')->unsigned()->nullable();
            $table->integer('modelo_id')->unsigned()->nullable();
            $table->string('nome');
            $table->string('imagem');
            $table->string('ampliacao');
            $table->text('caracteristicas');
            $table->integer('classificacao');
            $table->boolean('conversar_com_uma_pessoa');
            $table->boolean('ambientes_internos');
            $table->boolean('conversar_em_grupo');
            $table->boolean('falar_ao_telefone');
            $table->boolean('assistir_tv');
            $table->boolean('ambientes_ao_ar_livre');
            $table->boolean('showsconcertos_e_teatro');
            $table->boolean('restaurantes');
            $table->boolean('festas_e_ambientes_muito_ruidosos');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('aparelhos');
    }
}
