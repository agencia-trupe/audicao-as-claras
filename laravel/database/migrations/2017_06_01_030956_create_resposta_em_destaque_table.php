<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespostaEmDestaqueTable extends Migration
{
    public function up()
    {
        Schema::create('resposta_em_destaque', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->integer('pergunta')->unsigned()->nullable();
            $table->foreign('pergunta')->references('id')->on('respostas')->onDelete('set null');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('resposta_em_destaque');
    }
}
