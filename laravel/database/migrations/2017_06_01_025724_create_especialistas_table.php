<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspecialistasTable extends Migration
{
    public function up()
    {
        Schema::create('especialistas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);

            $table->string('nome');
            $table->string('email')->unique();
            $table->string('cpf');
            $table->string('profissao_cargo');
            $table->string('telefone');
            $table->string('cep');
            $table->string('endereco');
            $table->string('numero');
            $table->string('complemento');
            $table->string('bairro');
            $table->string('cidade');
            $table->string('uf');
            $table->decimal('lat', 10, 7);
            $table->decimal('lng', 10, 7);
            $table->string('nome_exibicao');
            $table->text('descricao');
            $table->string('imagem');
            $table->string('senha', 60);
            $table->integer('exibicoes')->default(0);

            $table->boolean('aprovado')->default(0);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('especialistas');
    }
}
