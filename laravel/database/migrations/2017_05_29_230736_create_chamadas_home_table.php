<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadasHomeTable extends Migration
{
    public function up()
    {
        Schema::create('chamadas_home', function (Blueprint $table) {
            $table->increments('id');
            $table->text('titulo');
            $table->text('chamada_1_texto');
            $table->string('chamada_1_botao');
            $table->string('chamada_1_link');
            $table->text('chamada_2_texto');
            $table->string('chamada_2_botao');
            $table->string('chamada_2_link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('chamadas_home');
    }
}
