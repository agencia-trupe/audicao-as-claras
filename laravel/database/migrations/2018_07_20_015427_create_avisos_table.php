<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvisosTable extends Migration
{
    public function up()
    {
        Schema::create('avisos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->text('texto');
            $table->string('botao_link');
            $table->string('botao_texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('avisos');
    }
}
