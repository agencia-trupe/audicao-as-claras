<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('telefone');
            $table->string('email');
            $table->string('cep');
            $table->boolean('conversar_com_uma_pessoa');
            $table->boolean('ambientes_internos');
            $table->boolean('conversar_em_grupo');
            $table->boolean('falar_ao_telefone');
            $table->boolean('assistir_tv');
            $table->boolean('ambientes_ao_ar_livre');
            $table->boolean('showsconcertos_e_teatro');
            $table->boolean('restaurantes');
            $table->boolean('festas_e_ambientes_muito_ruidosos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consultas');
    }
}
