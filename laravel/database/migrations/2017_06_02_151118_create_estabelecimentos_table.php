<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstabelecimentosTable extends Migration
{
    public function up()
    {
        Schema::create('estabelecimentos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome');
            $table->string('email')->unique();;
            $table->string('razao_social');
            $table->string('cnpj');
            $table->string('area_de_atuacao');
            $table->string('telefone');
            $table->string('responsavel');
            $table->string('cep');
            $table->string('endereco');
            $table->string('numero');
            $table->string('complemento');
            $table->string('bairro');
            $table->string('cidade');
            $table->string('uf');
            $table->string('website');
            $table->string('senha', 60);
            $table->decimal('lat', 10, 7);
            $table->decimal('lng', 10, 7);
            $table->integer('exibicoes')->default(0);

            $table->boolean('aprovado')->default(0);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('estabelecimentos');
    }
}
