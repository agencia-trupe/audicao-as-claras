<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEspecialistasAvaliacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialistas_avaliacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cadastro_id')->unsigned();
            $table->integer('especialista_id')->unsigned();
            $table->integer('avaliacao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('especialistas_avaliacoes');
    }
}
