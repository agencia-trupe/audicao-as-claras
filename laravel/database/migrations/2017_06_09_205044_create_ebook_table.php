<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbookTable extends Migration
{
    public function up()
    {
        Schema::create('ebook', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('ebook');
    }
}
