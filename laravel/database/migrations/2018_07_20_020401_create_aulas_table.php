<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAulasTable extends Migration
{
    public function up()
    {
        Schema::create('aulas_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('aulas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aulas_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('capa');
            $table->text('chamada');
            $table->text('informacoes');
            $table->string('video_tipo');
            $table->string('video_codigo');
            $table->foreign('aulas_categoria_id')->references('id')->on('aulas_categorias')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('aulas_downloads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('aula_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('arquivo');
            $table->foreign('aula_id')->references('id')->on('aulas')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('aulas_downloads');
        Schema::drop('aulas');
        Schema::drop('aulas_categorias');
    }
}
