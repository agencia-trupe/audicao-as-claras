<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabecalhoTable extends Migration
{
    public function up()
    {
        Schema::create('cabecalho', function (Blueprint $table) {
            $table->increments('id');
            $table->string('frase');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->string('imagem_5');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cabecalho');
    }
}
