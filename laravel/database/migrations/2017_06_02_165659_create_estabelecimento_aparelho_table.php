<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstabelecimentoAparelhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estabelecimento_aparelho', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estabelecimento_id')->unsigned();
            $table->integer('aparelho_id')->unsigned();
            $table->timestamps();
            $table->foreign('estabelecimento_id')->references('id')->on('estabelecimentos');
            $table->foreign('aparelho_id')->references('id')->on('aparelhos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estabelecimento_aparelho');
    }
}
