export default function SelecaoNecessidades() {
    var verificaSelecao = function() {
        return !!$('.aparelhos-index input:checked').length;
    };

    var $botao = $('.aparelhos-index input[type=submit]');

    $botao.attr('disabled', !verificaSelecao());

    $('.aparelhos-index input').change(function(event) {
            $botao.attr('disabled', !verificaSelecao());
    });
};
