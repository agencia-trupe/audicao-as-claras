export default function FiltroAparelhos() {
    $('select[name=filtro-aparelhos-marca]').change(function() {
        var marcaId  = $(this).val(),
            modeloId = parseInt($('select[name=filtro-aparelhos-modelo]').val()),
            $wrapper = $('.aparelhos-thumbs');

        if (marcaId) {
            $wrapper.each(function(index, el) {
                var $el = $(el);

                if (isNaN(modeloId)) {
                    var $matches = $el.find('a[data-marca='+ marcaId +']');
                    $el.find('a[data-marca!='+ marcaId +']').hide();
                    console.log('filtrando aqui');
                } else {
                    var $matches = $el.find('a[data-marca='+ marcaId +'][data-modelo='+ modeloId +']');
                    $el.find('a[data-marca!='+ marcaId +']').hide();
                    $el.find('a[data-modelo!='+ modeloId +']').hide();
                }

                $matches.show();

                if (!$matches.length) {
                    $el.prev().hide();
                } else {
                    $el.prev().show();
                }
            });
        } else {
            if (isNaN(modeloId)) {
                $wrapper.prev().show();
                $wrapper.find('a').show();
            } else {
                $('select[name=filtro-aparelhos-modelo]').trigger('change');
            }
        }
    });

    $('select[name=filtro-aparelhos-modelo]').change(function() {
            var modeloId = $(this).val(),
                marcaId  = parseInt($('select[name=filtro-aparelhos-marca]').val()),
                $wrapper = $('.aparelhos-thumbs');

            if (modeloId) {
                $wrapper.each(function(index, el) {
                    var $el = $(el);

                    if (isNaN(marcaId)) {
                        var $matches = $el.find('a[data-modelo='+ modeloId +']');
                        $el.find('a[data-modelo!='+ modeloId +']').hide();
                        console.log('filtrando aqui');
                    } else {
                        var $matches = $el.find('a[data-modelo='+ modeloId +'][data-marca='+ marcaId +']');
                        $el.find('a[data-modelo!='+ modeloId +']').hide();
                        $el.find('a[data-marca!='+ marcaId +']').hide();
                    }

                    $matches.show();

                    if (!$matches.length) {
                        $el.prev().hide();
                    } else {
                        $el.prev().show();
                    }
                });
            } else {
                if (isNaN(marcaId)) {
                    $wrapper.prev().show();
                    $wrapper.find('a').show();
                } else {
                    $('select[name=filtro-aparelhos-marca]').trigger('change');
                }
            }
        });
};
