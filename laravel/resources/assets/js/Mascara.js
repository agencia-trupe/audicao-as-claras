export default function Mascara() {
    $('.cep-mask').inputmask('99999-999', { jitMasking: true });
    $('.cpf-mask').inputmask('999.999.999-99', { jitMasking: true });
    $('.cnpj-mask').inputmask('99.999.999/9999-99', { jitMasking: true });
};
