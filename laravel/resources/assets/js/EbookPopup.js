export default function CatalogoPopup() {
    $('.ebook').fancybox({
        type: 'ajax',
        padding: 0
    });

    var envioEbook = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-ebook-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/ebook',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
            },
            success: function(data) {
                $('.ebook-download-link a').attr('href', data.link);
                $form.fadeOut(function() {
                    $('.ebook-download-link').fadeIn();
                });
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    $('body').on('submit', '#form-ebook', envioEbook);
};
