export default function Autenticacao() {
    $('.login-lightbox').fancybox({
        type: 'ajax',
        padding: 0
    });

    // LOGIN
    var envioLogin = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-login-lightbox-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/login',
            data: {
                email: $('#login-lightbox-email').val(),
                senha: $('#login-lightbox-senha').val(),
            },
            success: function(data) {
                location.reload();
            },
            error: function(data) {
                $response.fadeOut().text('Usuário ou senha inválidos.').fadeIn('slow');
                $.fancybox.update();
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };
    $(document).on('submit', '#form-login-lightbox', envioLogin);

    // CADASTRO
    var envioCadastro = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-cadastro-lightbox-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/cadastro',
            data: {
                nome: $('#cadastro-lightbox-nome').val(),
                email: $('#cadastro-lightbox-email').val(),
                telefone: $('#cadastro-lightbox-telefone').val(),
                cep: $('#cadastro-lightbox-cep').val(),
                senha: $('#cadastro-lightbox-senha').val(),
                senha_confirmation: $('#cadastro-lightbox-senha-confirmation').val(),
            },
            success: function(data) {
                location.reload();
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = '';

                    var lastKey = Object.keys(data.responseJSON)[Object.keys(data.responseJSON).length - 1];

                    for (var key in data.responseJSON) {
                        error += data.responseJSON[key];
                        if (key != lastKey) {
                            error += '<br>';
                        }
                    }

                    $response.fadeOut().html(error.replace(',', '<br>')).fadeIn('slow');
                    $.fancybox.update();
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };
    $(document).on('submit', '#form-cadastro-lightbox', envioCadastro);

    // ESQUECI MINHA SENHA
    var envioEsqueci = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-esqueci-lightbox-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/redefinicao-de-senha',
            data: {
                email: $('#esqueci-lightbox-email').val(),
            },
            success: function(data) {
                if (data.status == 'not-found') {
                    $response.fadeOut().text(data.response).fadeIn('slow');
                } else if (data.status == 'sent') {
                    $response.fadeOut(function() {
                        $form.find('input').fadeOut(function() {
                            $response.text(data.response).fadeIn('slow');
                        });
                    });
                }
            },
            error: function(data) {
                if (!data.responseJSON) return;

                var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                $response.fadeOut().text(error).fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };
    $(document).on('submit', '#form-esqueci-lightbox', envioEsqueci);

    // LOGIN APARELHOS
    var envioLoginAparelhos = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-login-aparelhos-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/login',
            data: {
                email: $('#login-aparelhos-email').val(),
                senha: $('#login-aparelhos-senha').val()
            },
            success: function(data) {
                var necessidades = [
                    $('#conversar_com_uma_pessoa').val() ? 1 : 0,
                    $('#ambientes_internos').val() ? 1 : 0,
                    $('#conversar_em_grupo').val() ? 1 : 0,
                    $('#falar_ao_telefone').val() ? 1 : 0,
                    $('#assistir_tv').val() ? 1 : 0,
                    $('#ambientes_ao_ar_livre').val() ? 1 : 0,
                    $('#showsconcertos_e_teatro').val() ? 1 : 0,
                    $('#restaurantes').val() ? 1 : 0,
                    $('#festas_e_ambientes_muito_ruidosos').val() ? 1 : 0
                ];

                var parameters = necessidades.reduce(function(prev, curr) {
                    return prev + '/' + curr;
                }, '');

                var url = $('base').attr('href') + '/aparelhos-auditivos/consulta' + parameters;

                location.href = url;
            },
            error: function(data) {
                console.log('errow');
                $response.fadeOut().text('Usuário ou senha inválidos.').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };
    $(document).on('submit', '#form-login-aparelhos', envioLoginAparelhos);

    // CADASTRO APARELHOS
    var envioCadastroAparelhos = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-cadastro-aparelhos-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/cadastro',
            data: {
                nome: $('#cadastro-aparelhos-nome').val(),
                email: $('#cadastro-aparelhos-email').val(),
                telefone: $('#cadastro-aparelhos-telefone').val(),
                cep: $('#cadastro-aparelhos-cep').val(),
                senha: $('#cadastro-aparelhos-senha').val(),
                senha_confirmation: $('#cadastro-aparelhos-senha-confirmation').val(),
            },
            success: function(data) {
                var necessidades = [
                    $('#conversar_com_uma_pessoa').val() ? 1 : 0,
                    $('#ambientes_internos').val() ? 1 : 0,
                    $('#conversar_em_grupo').val() ? 1 : 0,
                    $('#falar_ao_telefone').val() ? 1 : 0,
                    $('#assistir_tv').val() ? 1 : 0,
                    $('#ambientes_ao_ar_livre').val() ? 1 : 0,
                    $('#showsconcertos_e_teatro').val() ? 1 : 0,
                    $('#restaurantes').val() ? 1 : 0,
                    $('#festas_e_ambientes_muito_ruidosos').val() ? 1 : 0
                ];

                var parameters = necessidades.reduce(function(prev, curr) {
                    return prev + '/' + curr;
                }, '');

                var url = $('base').attr('href') + '/aparelhos-auditivos/consulta' + parameters;

                location.href = url;
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = '';

                    var lastKey = Object.keys(data.responseJSON)[Object.keys(data.responseJSON).length - 1];

                    for (var key in data.responseJSON) {
                        error += data.responseJSON[key];
                        if (key != lastKey) {
                            error += '<br>';
                        }
                    }

                    $response.fadeOut().html(error.replace(',', '<br>')).fadeIn('slow');
                    $.fancybox.update();
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };
    $(document).on('submit', '#form-cadastro-aparelhos', envioCadastroAparelhos);

    // MOSTRA FORMULÁRIO DE LOGIN - APARELHOS
    $('.btn-entrar').click(function(event) {
        event.preventDefault();

        $(this).fadeOut(function() {
            $(this).next().fadeIn();
        });
    });

};
