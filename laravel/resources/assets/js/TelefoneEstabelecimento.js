export default function TelefoneEstabelecimento() {
    $('.telefone-handle').click(function() {
        var $handle = $(this);

        if ($handle.hasClass('active')) return;

        $.get($handle.data('url'), function(data) {
            $handle.text(data).addClass('active');
        });
    });
};
