export default function Avaliacao() {
    $('.avaliacao .estrelas-avaliar span').mouseenter(function(){
        var $estrelas = $(this).siblings('span').andSelf();
        var id        = $(this).index();

        $estrelas.removeClass('selecionado');
        $estrelas.slice(0, id + 1).addClass('selecionado');
    });

    $('.avaliacao .estrelas-avaliar').mouseleave(function() {
        var $estrelas = $(this).find('span');

        $estrelas.removeClass('selecionado');
    });

    $('.avaliacao .estrelas-avaliar span').click(function(event) {
        event.preventDefault();

        var $parent = $(this).parent();

        if ($parent.hasClass('sending')) return false;

        $parent.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/avaliacao/' + $parent.data('tipo'),
            data: {
                id: $parent.data('id'),
                avaliacao: $(this).data('avaliacao'),
            },
            success: function(data) {
                var $estrelas = $parent.find('span');

                $estrelas.removeClass('selecionado avaliado');
                $estrelas.slice(0, data.avaliacao).addClass('selecionado avaliado');
            },
            error: function(data) {
                console.log('Ocorreu um erro.');
            },
            dataType: 'json'
        })
        .always(function() {
            $parent.removeClass('sending');
        });
    });
};
