export default function FaqToggle() {
    $('.pergunta .handle').click(function(event) {
        event.preventDefault();

        $(this).next().slideToggle().parent().toggleClass('active');
    });
};
