export default function Especialistas() {
    // consulta CEP
    $('.cep-consulta a').click(function(event) {
        event.preventDefault();

        var cep = $(this).prev().val().replace(/\D/g, '');

        if (cep != '') {
            if (!/^[0-9]{8}$/.test(cep)) {
                alert('CEP inválido.');
                return;
            }

            var $col = $(this).parent().parent();

            $col.find('[name$=endereco], [name$=bairro], [name$=cidade], [name$=uf]').attr('disabled', true).val('...');

            $.getJSON('https://viacep.com.br/ws/'+ cep +'/json/?callback=?', function(dados) {
                $col.find('[name$=endereco], [name$=bairro], [name$=cidade], [name$=uf]').attr('disabled', false);

                if (!('erro' in dados)) {
                    $col.find('[name$=endereco]').val(dados.logradouro);
                    $col.find('[name$=bairro]').val(dados.bairro);
                    $col.find('[name$=cidade]').val(dados.localidade);
                    $col.find('[name$=uf]').val(dados.uf);
                    $col.find('[name$=numero]').focus();
                } else {
                    $col.find('[name$=endereco], [name$=bairro], [name$=cidade], [name$=uf]').val('');
                    alert('CEP não encontrado.');
                }
            });
        }
    });
    
    // input foto
    var defaultLabel = $('.foto-input span').html();
    $('.foto-input input').change(function(event) {
        var fileName     = '',
            $label       = $(this).prev('span');

        fileName = event.target.files.length
            ? `<em>${event.target.files[0].name}</em>`
            : defaultLabel;
        
        $label.html(fileName);
    });

    // masonry Avisos
    var $grid = $('.especialistas-avisos');
    if (!$grid.length) return;

    $grid.waitForImages(function() {
        $grid.masonry({
            itemSelector: '.aviso',
            columnWidth: '.grid-sizer',
            gutter: '.gutter-sizer'
        });
    });
};
