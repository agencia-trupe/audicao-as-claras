export default function FormularioNewsletter() {
    var envioNewsletter = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-newsletter-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/newsletter',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                var errorMsg = data.responseJSON.nome ? data.responseJSON.nome : data.responseJSON.email;
                $response.hide().text(errorMsg).fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    $('#form-newsletter').on('submit', envioNewsletter);
};
