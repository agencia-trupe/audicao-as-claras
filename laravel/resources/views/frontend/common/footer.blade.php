    <footer>
        <div class="center">
            <div class="links">
                <a href="{{ route('home') }}">HOME</a>
                <a href="{{ route('empresa') }}">QUEM SOMOS</a>
                <a href="{{ route('aparelhos') }}">APARELHOS AUDITIVOS</a>
                <a href="{{ route('faq') }}">FAQ</a>
                <a href="{{ route('pergunte') }}">PERGUNTE AO ESPECIALISTA</a>
                <a href="{{ route('blog') }}">BLOG</a>
                <a href="{{ route('contato') }}">CONTATO</a>
            </div>
            <div class="info">
                <img src="{{ asset('assets/img/layout/logo-rodape.png') }}" alt="">
                <p class="telefone">{{ $contato->telefone }}</p>
                {{--<p>{!! $contato->endereco !!}</p>--}}
            </div>
            <div class="copyright">
                <p>
                    © {{ date('Y') }} {{ config('site.name') }}<br>
                    Todos os direitos reservados.
                </p>
                <p>
                    <a href="{{ route('politica-de-privacidade') }}">Política de Privacidade</a>
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:<br>
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
