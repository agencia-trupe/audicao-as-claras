<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="{{ config('site.description') }}">
    <meta name="keywords" content="{{ config('site.keywords') }}">

    <meta property="og:title" content="{{ config('site.title') }}">
    <meta property="og:description" content="{{ config('site.description') }}">
    <meta property="og:site_name" content="{{ config('site.title') }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:image" content="{{ asset('assets/img/layout/'.config('site.shareImg')) }}">

    <title>{{ config('site.title') }}</title>

    {!! Tools::loadCss('css/vendor.main.css') !!}
    {!! Tools::loadCss('vendor/fancybox/source/jquery.fancybox.css') !!}
    {!! Tools::loadCss('css/main.css') !!}
</head>
<body>
    @include('frontend.common.header')
    @yield('content')
    @include('frontend.common.footer')

    {!! Tools::loadJquery() !!}
    {!! Tools::loadJs('js/vendor.main.js') !!}
    {!! Tools::loadJs('vendor/fancybox/source/jquery.fancybox.pack.js') !!}
    {!! Tools::loadJs('vendor/waitForImages/dist/jquery.waitforimages.min.js') !!}
    {!! Tools::loadJs('vendor/masonry-layout/dist/masonry.pkgd.min.js') !!}
    {!! Tools::loadJs('js/main.js') !!}

@if(config('site.analytics'))
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ config("site.analytics") }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', '{{ config("site.analytics") }}');
    </script>
@endif
</body>
</html>
