<div class="lightbox-forms lightbox-selecao-login">
    <h2>LOGIN</h2>

    <div class="selecao-login">
        <a href="{{ route('cadastro.login') }}" class="login-lightbox">
            <p>
                <span>PARA</span>
                PACIENTES<br>
                USUÁRIOS
            </p>
        </a>
        <a href="{{ route('especialista.auth') }}">
            <p>
                <span>PARA</span>
                MÉDICOS<br>
                ESPECIALISTAS
            </p>
        </a>
        <a href="{{ route('estabelecimento.auth') }}">
            <p>
                <span>PARA</span>
                ESTABELECIMENTOS<br>
                FORNECEDORES
            </p>
        </a>
    </div>
</div>

