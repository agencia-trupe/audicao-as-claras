<div class="lightbox-forms">
    <h2>PRIMEIRO CADASTRO</h2>

    <form action="{{ route('cadastro.cadastroPost') }}" id="form-cadastro-lightbox" method="POST">
        <div id="form-cadastro-lightbox-response" class="lightbox-response"></div>
        <input type="text" name="nome" id="cadastro-lightbox-nome" placeholder="nome" required>
        <input type="email" name="email" id="cadastro-lightbox-email" placeholder="e-mail" required>
        <input type="text" name="telefone" id="cadastro-lightbox-telefone" placeholder="telefone">
        <input type="text" name="cep" id="cadastro-lightbox-cep" placeholder="CEP" required>
        <input type="password" name="senha" id="cadastro-lightbox-senha" placeholder="senha" required>
        <input type="password" name="senha_confirmation" id="cadastro-lightbox-senha-confirmation" placeholder="repetir senha" required>
        <input type="submit" value="CADASTRAR">
    </form>
</div>

<script>
    $('#cadastro-lightbox-cep').inputmask('99999-999', { jitMasking: true });
</script>
