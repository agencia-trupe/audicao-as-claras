    @if(session('senhaRedefinidaComSucesso'))
    <div class="aviso-header">
        <div class="center">
            SENHA REDEFINIDA COM SUCESSO!
        </div>
    </div>
    @elseif(session('cadastroEspecialistaRedirect'))
    <div class="aviso-header">
        <div class="center">
            CADASTRO EFETUADO COM SUCESSO. AGUARDE A APROVAÇÃO DOS NOSSOS ADMINISTRADORES.
        </div>
    </div>
    @endif

    <header>
        <nav>
            <div class="center">
                @if(Tools::isActive('especialista*') || Tools::isActive('estabelecimento*'))
                    <a href="{{ route('home') }}">VOLTAR PARA O SITE</a>
                    @if(\Auth::guard('especialista')->check() && Tools::isActive('especialista*'))
                        <a href="{{ route('especialista') }}" @if(Tools::isActive('especialista')) class="active" @endif>MEUS DADOS</a>
                        <a href="{{ route('especialista.avisos') }}" @if(Tools::isActive('especialista.avisos')) class="active" @endif>AVISOS</a>
                        <a href="{{ route('especialista.aulas') }}" @if(Tools::isActive('especialista.aula*')) class="active" @endif>AULAS</a>
                        <a href="{{ route('especialista.perguntas') }}" @if(Tools::isActive('especialista.perguntas*')) class="active" @endif>PERGUNTE AO ESPECIALISTA</a>
                        <div class="social"></div>
                        <a href="{{ route('especialista.logout') }}" class="logout">
                            <span class="nome">
                                Olá {{ explode(' ', \Auth::guard('especialista')->user()->nome)[0] }}!
                            </span>
                            <span class="x"></span>
                        </a>
                    @endif
                    @if(\Auth::guard('estabelecimento')->check() && Tools::isActive('estabelecimento*'))
                        <a href="{{ route('estabelecimento') }}" @if(Tools::isActive('estabelecimento')) class="active" @endif>MEUS DADOS</a>
                        <a href="{{ route('estabelecimento.avisos') }}" @if(Tools::isActive('estabelecimento.avisos')) class="active" @endif>AVISOS</a>
                        <a href="{{ route('estabelecimento.aulas') }}" @if(Tools::isActive('estabelecimento.aula*')) class="active" @endif>AULAS</a>
                        <div class="social"></div>
                        <a href="{{ route('estabelecimento.logout') }}" class="logout">
                            <span class="nome">
                                Olá {{ explode(' ', \Auth::guard('estabelecimento')->user()->nome)[0] }}!
                            </span>
                            <span class="x"></span>
                        </a>
                    @endif
                @else
                    @include('frontend.common.nav')

                    <div class="social">
                        @foreach(['facebook', 'instagram'] as $s)
                            @if($contato->{$s})
                            <a href="{{ $contato->{$s} }}" class="{{ $s }}">{{ $s }}</a>
                            @endif
                        @endforeach
                    </div>

                    @if(\Auth::guard('cadastro')->check())
                        <a href="{{ route('cadastro.logout') }}" class="logout">
                            <span class="nome">
                                Olá {{ explode(' ', \Auth::guard('cadastro')->user()->nome)[0] }}!
                            </span>
                            <span class="x"></span>
                        </a>
                    @else
                        <a href="{{ route('selecaoLogin') }}" class="login login-lightbox">
                            LOGIN
                        </a>
                    @endif
                @endif
            </div>
        </nav>
        <div class="logo">
            <div class="center">
                <a href="{{ route('home') }}">{{ config('site.name') }}</a>
                <button id="mobile-toggle" type="button" role="button">
                    <span class="lines"></span>
                </button>
                <h2>{{ $cabecalho->frase }}</h2>
            </div>
        </div>
    </header>
