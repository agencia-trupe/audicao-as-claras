<div class="lightbox-forms">
    <h2>ESQUECI MINHA SENHA</h2>

    <form action="{{ route('cadastro.redefinicao') }}" id="form-esqueci-lightbox" method="POST">
        <div id="form-esqueci-lightbox-response" class="lightbox-response"></div>
        <input type="email" name="email" id="esqueci-lightbox-email" placeholder="e-mail" required>
        <input type="submit" value="REDEFINIR SENHA">
    </form>
</div>

