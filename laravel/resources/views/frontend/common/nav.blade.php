<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>HOME</a>
<a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>QUEM SOMOS</a>
<a href="{{ route('aparelhos') }}" @if(Tools::isActive('aparelhos*')) class="active" @endif>APARELHOS AUDITIVOS</a>
<a href="{{ route('faq') }}" @if(Tools::isActive('faq')) class="active" @endif>FAQ</a>
<a href="{{ route('pergunte') }}" @if(Tools::isActive('pergunte*')) class="active" @endif>PERGUNTE AO ESPECIALISTA</a>
<a href="{{ route('blog') }}" @if(Tools::isActive('blog*')) class="active" @endif>BLOG</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>CONTATO</a>
