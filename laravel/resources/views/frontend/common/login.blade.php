<div class="lightbox-forms">
    <h2>LOGIN</h2>

    <form action="{{ route('cadastro.loginPost') }}" id="form-login-lightbox" method="POST">
        <div id="form-login-lightbox-response" class="lightbox-response"></div>
        <input type="email" name="email" id="login-lightbox-email" placeholder="e-mail" required>
        <input type="password" name="senha" id="login-lightbox-senha" placeholder="senha" required>
        <input type="submit" value="ENTRAR">
        <a href="{{ route('cadastro.esqueci') }}" class="login-lightbox btn-esqueci">esqueci minha senha &raquo;</a>
    </form>

    <a href="{{ route('cadastro.cadastro') }}" class="login-lightbox btn-cadastro">NÃO TENHO CADASTRO</a>
</div>

