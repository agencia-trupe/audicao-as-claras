@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <div class="info">
                <p class="telefone">{{ $contato->telefone }}</p>
                {{--<p>{!! $contato->endereco !!}</p>--}}
            </div>

            <form action="" id="form-contato" method="POST">
                <h2>Fale Conosco</h2>
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <div id="form-contato-response"></div>
                <input type="submit" value="ENVIAR" style="margin-bottom:0">
            </form>
        </div>

        {{--
        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
        --}}
    </div>

@endsection
