@extends('frontend.common.template')

@section('content')

    <div class="main faq">
        <div class="titulo">
            <div class="center">
                <h1>Perguntas frequentes</h1>
                <p>Frase explicativa sobre as perguntas que serão apresentadas a seguir</p>
            </div>
        </div>

        <div class="center">
            @foreach($perguntas as $pergunta)
            <div class="pergunta">
                <a href="#" class="handle">
                    {{ $pergunta->pergunta }}
                </a>
                <div class="resposta">{!! $pergunta->resposta !!}</div>
            </div>
            @endforeach
        </div>
    </div>

@endsection
