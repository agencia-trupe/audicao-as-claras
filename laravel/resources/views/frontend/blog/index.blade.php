@extends('frontend.common.template')

@section('content')

    <div class="main blog">
        <div class="center">
            <div class="blog-index">
                @foreach($posts as $post)
                    @if(isset($categoria) && !$categoria->exists && $destaque->slug == $post->slug)
                        <div class="thumb-destaque">
                            <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}">
                                <div class="imagem">
                                    <img src="{{ asset('assets/img/blog/recente/'.$post->capa) }}" alt="">
                                    <div class="leia-mais">
                                        <span>{{ $post->categoria->titulo }} | {{ Tools::formataData($post->data) }}</span>
                                        <span>leia mais</span>
                                    </div>
                                </div>
                                <h2>{{ $post->titulo }}</h2>
                                <p>{{ mb_strimwidth(strip_tags($post->texto), 0, 200, '...') }}</p>
                            </a>
                        </div>
                    @else
                        <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}" class="thumb-normal">
                            <img src="{{ asset('assets/img/blog/thumb/'.$post->capa) }}" alt="">
                            <div class="texto">
                                <h3>{{ $post->titulo }}</h3>
                                <p>{{ $post->categoria->titulo }} | {{ Tools::formataData($post->data) }}</p>
                            </div>
                        </a>
                    @endif
                @endforeach

                {{ $posts->render() }}
            </div>

            <aside>
                <h2>Blog</h2>
                <div class="categorias">
                    <h4>Categorias</h4>
                    @foreach($categorias as $c)
                    <a href="{{ route('blog', $c->slug) }}" @if(isset($categoria) && $c->slug == $categoria->slug) class="active" @endif>
                        {{ $c->titulo }}
                    </a>
                    @endforeach
                </div>
                <div class="arquivo">
                    <h4>Arquivo</h4>
                    @foreach($arquivos as $a)
                    <a href="{{ route('blog.arquivo', [$a->ano, $a->mes]) }}" @if(isset($ano) && isset($mes) && ($a->ano == $ano) && ($a->mes == $mes)) class="active" @endif>
                        {{ Tools::formataArquivo($a->ano, $a->mes) }}
                    </a>
                    @endforeach
                </div>
                @if($ebook->arquivo)
                <a href="{{ route('ebook') }}" class="ebook">
                    <span>Download do e-Book</span>
                    <p>{{ $ebook->titulo }}</p>
                </a>
                @endif
            </aside>
        </div>
    </div>

@endsection
