<div class="popup-ebook">
    <h2>
        <span>Download do e-Book</span>
        {{ $ebook->titulo }}
    </h2>
    <form action="" id="form-ebook" method="POST">
        <input type="text" name="nome" id="nome" placeholder="nome" required>
        <input type="email" name="email" id="email" placeholder="e-mail" required>
        <div id="form-ebook-response"></div>
        <input type="submit" value="ENVIAR">
    </form>
    <div class="ebook-download-link">
        <a href="#" target="_blank">FAZER DOWNLOAD</a>
    </div>
</div>
