@extends('frontend.common.template')

@section('content')

    <div class="main blog">
        <div class="center">
            <div class="blog-show">
                <div class="show-imagem">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/blog/capa/'.$post->capa) }}" alt="">
                        <div class="info">
                            <span>{{ $post->categoria->titulo }} | {{ Tools::formataData($post->data) }}</span>
                            <span>{{ count($post->comentariosAprovados) }} comentário{{ count($post->comentariosAprovados) == 1 ? '' : 's' }}</span>
                        </div>
                    </div>
                </div>

                <h1>{{ $post->titulo }}</h1>
                {!! $post->texto !!}

                <div class="comentarios">
                    @if(count($post->comentariosAprovados))
                        <h5 class="balao">COMENTÁRIOS</h5>
                        @foreach($post->comentariosAprovados as $comentario)
                        <div class="comentario">
                            <p>{{ $comentario->comentario }}</p>
                            <p>
                                {{ $comentario->nome }}
                                <span>{{ Tools::formataData($comentario->created_at) }}</span>
                            </p>
                        </div>
                        @endforeach
                    @endif

                    <h5>COMENTE VOCÊ TAMBÉM</h5>
                    <form action="" id="form-comentario">
                        <input type="hidden" name="post" id="post" value="{{ $post->id }}">
                        <input type="text" name="nome" id="nome" placeholder="nome" @if(\Auth::guard('cadastro')->check()) value="{{ \Auth::guard('cadastro')->user()->nome }}" @endif required>
                        <input type="email" name="email" id="email" placeholder="e-mail" @if(\Auth::guard('cadastro')->check()) value="{{ \Auth::guard('cadastro')->user()->email }}" @endif required>
                        <textarea name="comentario" id="comentario" placeholder="comentário" required></textarea>
                        <div id="form-comentario-response"></div>
                        <input type="submit" value="ENVIAR">
                    </form>
                </div>
            </div>

            <aside>
                <h2>Blog</h2>
                <div class="categorias">
                    <h4>Categorias</h4>
                    @foreach($categorias as $c)
                    <a href="{{ route('blog', $c->slug) }}" @if(isset($categoria) && $c->slug == $categoria->slug) class="active" @endif>
                        {{ $c->titulo }}
                    </a>
                    @endforeach
                </div>
                @if(count($leiaTambem))
                <div class="leia-tambem">
                    <h3>Leia também</h3>
                    @foreach($leiaTambem as $outroPost)
                    <a href="{{ route('blog.show', [$outroPost->categoria->slug, $outroPost->slug]) }}">
                        <img src="{{ asset('assets/img/blog/thumb/'.$outroPost->capa) }}" alt="">
                        <span>{{ $outroPost->titulo }}</span>
                    </a>
                    @endforeach
                </div>
                @endif
                @if($ebook->arquivo)
                <a href="{{ route('ebook') }}" class="ebook">
                    <span>Download do e-Book</span>
                    <p>{{ $ebook->titulo }}</p>
                </a>
                @endif
            </aside>
        </div>
    </div>

@endsection
