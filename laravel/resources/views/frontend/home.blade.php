@extends('frontend.common.template')

@section('content')

    <div class="main home">
        <div class="banner">
            @foreach(range(1, 5) as $i)
            <img src="{{ asset('assets/img/cabecalho/'.$cabecalho->{'imagem_'.$i}) }}" alt="">
            @endforeach
        </div>

        <div class="aparelhos-index">
            <div class="center">
                <h1>Saiba qual o aparelho auditivo que melhor atende sua necessidade e onde encontrá-lo</h1>
                <p>SELECIONE OS AMBIENTES QUE VOCÊ COSTUMA FREQUENTAR:</p>
                <form action="{{ route('aparelhos.post') }}" method="POST">
                    {!! csrf_field() !!}
                    <label for="conversar_com_uma_pessoa">
                        <img src="{{ asset('assets/img/layout/selecao1.png') }}" alt="">
                        <input type="checkbox" name="conversar_com_uma_pessoa" id="conversar_com_uma_pessoa" value="1">
                        <span class="tooltip">CONVERSAR COM UMA PESSOA</span>
                    </label>
                    <label for="ambientes_internos">
                        <img src="{{ asset('assets/img/layout/selecao2.png') }}" alt="">
                        <input type="checkbox" name="ambientes_internos" id="ambientes_internos" value="1">
                        <span class="tooltip">AMBIENTES INTERNOS</span>
                    </label>
                    <label for="conversar_em_grupo">
                        <img src="{{ asset('assets/img/layout/selecao3.png') }}" alt="">
                        <input type="checkbox" name="conversar_em_grupo" id="conversar_em_grupo" value="1">
                        <span class="tooltip">CONVERSAR EM GRUPO</span>
                    </label>
                    <label for="falar_ao_telefone">
                        <img src="{{ asset('assets/img/layout/selecao4.png') }}" alt="">
                        <input type="checkbox" name="falar_ao_telefone" id="falar_ao_telefone" value="1">
                        <span class="tooltip">FALAR AO TELEFONE</span>
                    </label>
                    <label for="assistir_tv">
                        <img src="{{ asset('assets/img/layout/selecao5.png') }}" alt="">
                        <input type="checkbox" name="assistir_tv" id="assistir_tv" value="1">
                        <span class="tooltip">ASSISTIR TV</span>
                    </label>
                    <label for="ambientes_ao_ar_livre">
                        <img src="{{ asset('assets/img/layout/selecao6.png') }}" alt="">
                        <input type="checkbox" name="ambientes_ao_ar_livre" id="ambientes_ao_ar_livre" value="1">
                        <span class="tooltip">AMBIENTES AO AR LIVRE</span>
                    </label>
                    <label for="showsconcertos_e_teatro">
                        <img src="{{ asset('assets/img/layout/selecao7.png') }}" alt="">
                        <input type="checkbox" name="showsconcertos_e_teatro" id="showsconcertos_e_teatro" value="1">
                        <span class="tooltip">SHOWS, CONCERTOS E TEATRO</span>
                    </label>
                    <label for="restaurantes">
                        <img src="{{ asset('assets/img/layout/selecao8.png') }}" alt="">
                        <input type="checkbox" name="restaurantes" id="restaurantes" value="1">
                        <span class="tooltip">RESTAURANTES</span>
                    </label>
                    <label for="festas_e_ambientes_muito_ruidosos">
                        <img src="{{ asset('assets/img/layout/selecao9.png') }}" alt="">
                        <input type="checkbox" name="festas_e_ambientes_muito_ruidosos" id="festas_e_ambientes_muito_ruidosos" value="1">
                        <span class="tooltip">FESTAS E AMBIENTES MUITO RUIDOSOS</span>
                    </label>
                    <input type="submit" value="LOCALIZAR" disabled>
                </form>
            </div>
        </div>

        <div class="chamadas">
            <div class="center">
                <div class="col">
                    <h2>{!! $chamadas->titulo !!}</h2>
                </div>
                <div class="col">
                    <p>{{ $chamadas->chamada_1_texto }}</p>
                    <a href="{{ $chamadas->chamada_1_link }}">{{ $chamadas->chamada_1_botao }}</a>
                </div>
                <div class="col">
                    <p>{{ $chamadas->chamada_2_texto }}</p>
                    <a href="{{ $chamadas->chamada_2_link }}">{{ $chamadas->chamada_2_botao }}</a>
                </div>
            </div>
        </div>

        <div class="center">
            <div class="blog">
                <h3>Últimas do blog</h3>
                <div>
                    @foreach($posts as $post)
                    <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}">
                        <img src="{{ asset('assets/img/blog/thumb/'.$post->capa) }}" alt="">
                        <h4>{{ $post->titulo }}</h4>
                        <p>{{ mb_strimwidth(strip_tags($post->texto), 0, 200, '...') }}</p>
                        <span class="seta"></span>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="newsletter">
            <div class="center">
                <form action="" id="form-newsletter" method="POST">
                    <p>CADASTRE-SE PARA RECEBER NOVIDADES:</p>
                    <input type="name" name="nome" id="nome" placeholder="nome" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <div class="botao">
                        <input type="submit" value="">
                    </div>
                    <div id="form-newsletter-response"></div>
                </form>
            </div>
        </div>
    </div>

@endsection
