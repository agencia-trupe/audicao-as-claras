@extends('frontend.common.template')

@section('content')

    <div class="main empresa">
        <div class="banner">
            @foreach(range(1, 5) as $i)
            <img src="{{ asset('assets/img/cabecalho/'.$cabecalho->{'imagem_'.$i}) }}" alt="">
            @endforeach
        </div>

        <div class="center">
            <div class="texto">{!! $empresa->texto !!}</div>
            <div class="imagem">
                <img src="{{ asset('assets/img/empresa/'.$empresa->imagem) }}" alt="">
            </div>
        </div>
    </div>

@endsection
