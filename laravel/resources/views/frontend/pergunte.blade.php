@extends('frontend.common.template')

@section('content')

    <div class="main pergunte">
        <div class="titulo">
            <div class="center">
                <h1>Pergunte ao Especialista</h1>
                <p>O usuário envia suas perguntas, a equipe do Audição às Claras analisa e encaminha ao especialista melhor indicado para responder e depois publica as perguntas e respostas como conteúdo aqui no site.</p>
            </div>
        </div>

        <div class="center">
            @if(\Auth::guard('cadastro')->check())
                <form action="" id="form-pergunte" method="POST">
                    <input type="hidden" name="nome" id="nome" value="{{ \Auth::guard('cadastro')->user()->nome }}">
                    <input type="hidden" name="email" id="email" value="{{ \Auth::guard('cadastro')->user()->email }}">
                    <input type="hidden" name="telefone" id="telefone" value="{{ \Auth::guard('cadastro')->user()->telefone }}">
                    <textarea name="pergunta" id="pergunta" placeholder="Olá {{ explode(' ', \Auth::guard('cadastro')->user()->nome)[0] }},&#10;Envie sua pergunta aqui." required></textarea>
                    <input type="submit" value="ENVIAR">
                    <div id="form-pergunte-response"></div>
                </form>
            @else
                <a href="{{ route('cadastro.login') }}" class="login-lightbox login-pergunte">
                    Clique aqui para fazer login e enviar sua pergunta
                </a>
            @endif

            <div class="especialistas">
                <h2>Conheça os Especialistas</h2>
                @foreach($especialistas as $especialista)
                <div class="especialista">
                    @if($especialista->imagem)
                    <img src="{{ asset('assets/img/especialistas/'.$especialista->imagem) }}" alt="">
                    @else
                    <img src="{{ asset('assets/img/layout/especialista.png') }}" alt="">
                    @endif
                    <span>{{ $especialista->nome_exibicao }}</span>
                </div>
                @endforeach
            </div>
        </div>

        <div class="respostas">
            <div class="center">
                <h2>Respostas dos Especialistas</h2>

                @if($destaque->pergunta)
                <div class="destaque">
                    <img src="{{ asset('assets/img/resposta-em-destaque/'.$destaque->imagem) }}" alt="">
                    <div class="texto">
                        @if($categorias->find($destaque->respostaModel->categoria))
                        <h4>{{ $categorias->find($destaque->respostaModel->categoria)->titulo }}</h4>
                        @endif
                        <h3>{{ $destaque->respostaModel->pergunta }}</h3>
                        {!! $destaque->respostaModel->resposta !!}

                        @if(\Auth::guard('cadastro')->check())
                        <div class="avaliacao">
                            <p>CLASSIFIQUE A RESPOSTA:</p>
                            <div class="estrelas estrelas-avaliar" data-tipo="resposta" data-id="{{ $destaque->respostaModel->id }}">
                                @if($destaque->respostaModel->avaliacao)
                                    @foreach(range(1, 5) as $estrela)
                                    <span data-avaliacao="{{ $estrela }}" @if($destaque->respostaModel->avaliacao->avaliacao >= $estrela) class="avaliado" @endif></span>
                                    @endforeach
                                @else
                                    <span data-avaliacao="1"></span>
                                    <span data-avaliacao="2"></span>
                                    <span data-avaliacao="3"></span>
                                    <span data-avaliacao="4"></span>
                                    <span data-avaliacao="5"></span>
                                @endif
                            </div>
                        </div>
                        @endif

                        <div class="especialista">
                            @if($destaque->respostaModel->especialistaModel->imagem)
                            <img src="{{ asset('assets/img/especialistas/'.$destaque->respostaModel->especialistaModel->imagem) }}" alt="">
                            @else
                            <img src="{{ asset('assets/img/layout/especialista.png') }}" alt="">
                            @endif
                            <div class="texto">
                                <h4>{{ $destaque->respostaModel->especialistaModel->nome_exibicao }}</h4>
                                <p>{{ $destaque->respostaModel->especialistaModel->descricao }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                <div class="categorias">
                    <h3>Categorias</h3>
                    @foreach($categorias as $cat)
                    <a href="{{ route('pergunte', $cat->slug) }}" @if($cat->slug == $categoria->slug) class="active" @endif>{{ $cat->titulo }}</a>
                    @endforeach
                </div>

                <div class="categoria-selecionada">
                    <h3>{{ $categoria->titulo }}</h3>
                    @foreach($respostas as $resposta)
                    <div class="pergunta">
                        <a href="#" class="handle">
                            {{ $resposta->pergunta }}
                        </a>
                        <div class="resposta">
                            {!! $resposta->resposta !!}
                            @if(\Auth::guard('cadastro')->check())
                            <div class="avaliacao">
                                <p>CLASSIFIQUE A RESPOSTA:</p>
                                <div class="estrelas estrelas-avaliar" data-tipo="resposta" data-id="{{ $resposta->id }}">
                                    @if($resposta->avaliacao)
                                        @foreach(range(1, 5) as $estrela)
                                        <span data-avaliacao="{{ $estrela }}" @if($resposta->avaliacao->avaliacao >= $estrela) class="avaliado" @endif></span>
                                        @endforeach
                                    @else
                                        <span data-avaliacao="1"></span>
                                        <span data-avaliacao="2"></span>
                                        <span data-avaliacao="3"></span>
                                        <span data-avaliacao="4"></span>
                                        <span data-avaliacao="5"></span>
                                    @endif
                                </div>
                            </div>
                            @endif
                            <div class="especialista">
                                @if($resposta->especialistaModel->imagem)
                                <img src="{{ asset('assets/img/especialistas/'.$resposta->especialistaModel->imagem) }}" alt="">
                                @else
                                <img src="{{ asset('assets/img/layout/especialista.png') }}" alt="">
                                @endif
                                <div class="texto">
                                    <h4>{{ $resposta->especialistaModel->nome }}</h4>
                                    <p>{{ $resposta->especialistaModel->descricao }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
