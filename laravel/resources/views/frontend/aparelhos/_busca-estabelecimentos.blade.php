<div class="busca-estabelecimento">
    <div class="center">
        <p>{{ $frase or 'NOVA BUSCA POR ESTABELECIMENTO / PROFISSIONAL:' }}</p>
        <form action="{{ route('aparelhos.estabelecimentos') }}" method="GET">
            <input type="text" name="cep" class="cep-mask" placeholder="informe o CEP" required>
            <button>LOCALIZAR POR PROXIMIDADE</button>
        </form>
        <form action="{{ route('aparelhos.estabelecimentos') }}" method="GET">
            <input type="text" name="palavra-chave" placeholder="palavra-chave" required>
            <button>LOCALIZAR POR NOME</button>
        </form>
    </div>
</div>
