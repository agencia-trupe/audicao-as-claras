@extends('frontend.common.template')

@section('content')

    <div class="main aparelhos">
        <div class="aparelhos-interna">
            <div class="aparelhos-estabelecimentos">
                @if(isset($erro))
                    <div class="center">
                        <p>{{ $erro }}</p>
                    </div>
                @else
                    @if(count($estabelecimentos) || count($especialistas))
                    <div class="aparelhos-estabelecimentos-box">
                        <div class="center">
                            <p>ESTABELECIMENTOS E PROFISSIONAIS:</p>
                            @if(count($estabelecimentos))
                            <div class="mapa-wrapper">
                                <div id="mapa-estabelecimentos"></div>
                                <script>
                                    function initMap() {
                                        var map;
                                        var bounds = new google.maps.LatLngBounds();
                                        var mapOptions = {
                                            mapTypeId: 'roadmap'
                                        };

                                        map = new google.maps.Map(document.getElementById('mapa-estabelecimentos'), mapOptions);
                                        map.setTilt(45);

                                        var markers = [
                                            @foreach($estabelecimentos as $e)
                                            ['{{ $e->nome }}', {{ $e->lat }}, {{ $e->lng }}],
                                            @endforeach
                                        ];

                                        var infoWindowContent = [
                                            @foreach($estabelecimentos as $e)
                                            ['<div class="info_content"><h3>{{ $e->nome }}</h3><p>{{ $e->endereco }}<br>{{ $e->bairro }}<br>{{ $e->cep }} - {{ $e->cidade }} - {{ $e->estado }}</p></div>'],
                                            @endforeach
                                        ];

                                        var infoWindow = new google.maps.InfoWindow(), marker, i;

                                        for( i = 0; i < markers.length; i++ ) {
                                            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                                            bounds.extend(position);
                                            marker = new google.maps.Marker({
                                                position: position,
                                                map: map,
                                                title: markers[i][0]
                                            });

                                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                                return function() {
                                                    infoWindow.setContent(infoWindowContent[i][0]);
                                                    infoWindow.open(map, marker);
                                                }
                                            })(marker, i));

                                            map.fitBounds(bounds);
                                        }

                                        google.maps.event.addDomListener(window, 'resize', function() {
                                            map.fitBounds(bounds);
                                        });
                                    }
                                </script>
                                <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBT3CH62ku8FLHfDRO392Pc4ozZ5qJSvVs&callback=initMap"></script>
                            </div>
                            @endif
                        </div>
                    </div>
                    @if(count($estabelecimentos))
                    <div class="center">
                    <div>
                        @foreach($estabelecimentos as $estabelecimento)
                        <div class="estabelecimento">
                            <h3>{{ $estabelecimento->nome }}</h3>
                            <span class="telefone-handle" data-url="{{ route('aparelhos.telefone', $estabelecimento->id) }}">VER TELEFONE &raquo;</span>
                            <p>
                                {{ $estabelecimento->endereco }}, {{ $estabelecimento->numero }}{{ $estabelecimento->complemento ? ' - '.$estabelecimento->complemento : '' }}<br>
                                {{ $estabelecimento->bairro }}<br>
                                {{ $estabelecimento->cep }} - {{ $estabelecimento->cidade }} - {{ $estabelecimento->uf }}
                            </p>
                            @if($estabelecimento->website)
                            <a href="{{ $estabelecimento->website }}" target="_blank">ir para o site</a>
                            @endif
                            @if(\Auth::guard('cadastro')->check())
                            <div class="avaliacao">
                                <p>CLASSIFIQUE:</p>
                                <div class="estrelas estrelas-avaliar" data-tipo="estabelecimento" data-id="{{ $estabelecimento->id }}">
                                    @if($estabelecimento->avaliacao)
                                        @foreach(range(1, 5) as $estrela)
                                        <span data-avaliacao="{{ $estrela }}" @if($estabelecimento->avaliacao->avaliacao >= $estrela) class="avaliado" @endif></span>
                                        @endforeach
                                    @else
                                        <span data-avaliacao="1"></span>
                                        <span data-avaliacao="2"></span>
                                        <span data-avaliacao="3"></span>
                                        <span data-avaliacao="4"></span>
                                        <span data-avaliacao="5"></span>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(count($estabelecimento->marcas))
                            <div class="estabelecimento-marcas">
                                <span>MARCAS:</span>
                                <div>
                                @foreach($estabelecimento->marcas as $marca)
                                    <p>{{ $marca->titulo }}</p>
                                @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    </div>
                    @endif

                    @if(count($estabelecimentos) && count($especialistas))
                    <div class="estabelecimentos-divider"></div>
                    @endif

                    @if(count($especialistas))
                    <div class="center">
                    <div>
                        @foreach($especialistas as $especialista)
                        <div class="estabelecimento">
                            <h3>{{ $especialista->nome }}</h3>
                            <span class="telefone-handle" data-url="{{ route('especialistas.telefone', $especialista->id) }}">VER TELEFONE &raquo;</span>
                            <p>
                                {{ $especialista->endereco }}, {{ $especialista->numero }}{{ $especialista->complemento ? ' - '.$especialista->complemento : '' }}<br>
                                {{ $especialista->bairro }}<br>
                                {{ $especialista->cep }} - {{ $especialista->cidade }} - {{ $especialista->uf }}
                            </p>
                            @if(\Auth::guard('cadastro')->check())
                            <div class="avaliacao">
                                <p>CLASSIFIQUE:</p>
                                <div class="estrelas estrelas-avaliar" data-tipo="especialista" data-id="{{ $especialista->id }}">
                                    @if($especialista->avaliacao)
                                        @foreach(range(1, 5) as $estrela)
                                        <span data-avaliacao="{{ $estrela }}" @if($especialista->avaliacao->avaliacao >= $estrela) class="avaliado" @endif></span>
                                        @endforeach
                                    @else
                                        <span data-avaliacao="1"></span>
                                        <span data-avaliacao="2"></span>
                                        <span data-avaliacao="3"></span>
                                        <span data-avaliacao="4"></span>
                                        <span data-avaliacao="5"></span>
                                    @endif
                                </div>
                            </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    </div>
                    @endif

                    @else
                    <div class="center">
                        <p>NENHUM ESTABELECIMENTO OU PROFISSIONAL ENCONTRADO</p>
                    </div>
                    @endif
                @endif
            </div>

            @include('frontend.aparelhos._busca-estabelecimentos')
        </div>
    </div>

@endsection
