@extends('frontend.common.template')

@section('content')

    <div class="main aparelhos">
        <div class="aparelhos-interna">
            <div class="descritivo">
                <div class="center">
                    @if($aparelho->ampliacao)
                    <img src="{{ asset('assets/img/aparelhos/ampliacao/'.$aparelho->ampliacao) }}" alt="">
                    @else
                    <img src="{{ asset('assets/img/aparelhos/'.$aparelho->imagem) }}" alt="">
                    @endif

                    <div class="texto">
                        <h1>
                            <span>{{ $aparelho->marca ? $aparelho->marca->titulo : '' }}</span>
                            <span>{{ $aparelho->nome }}</span>
                        </h1>
                        {!! $aparelho->caracteristicas !!}
                    </div>

                    <div class="avaliacao">
                        <p>Dê sua nota para este aparelho:</p>
                        <div class="estrelas estrelas-avaliar" data-tipo="aparelho" data-id="{{ $aparelho->id }}">
                            @if($aparelho->avaliacao)
                                @foreach(range(1, 5) as $estrela)
                                <span data-avaliacao="{{ $estrela }}" @if($aparelho->avaliacao->avaliacao >= $estrela) class="avaliado" @endif></span>
                                @endforeach
                            @else
                                <span data-avaliacao="1"></span>
                                <span data-avaliacao="2"></span>
                                <span data-avaliacao="3"></span>
                                <span data-avaliacao="4"></span>
                                <span data-avaliacao="5"></span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="aparelhos-estabelecimentos">
                <div class="center">
                    @if(count($estabelecimentos))
                    <p>VOCÊ ENCONTRARÁ ESSES MODELOS NOS SEGUINTES ESTABELECIMENTOS:</p>
                    <div class="mapa-wrapper">
                        <div id="mapa-estabelecimentos"></div>
                        <script>
                            function initMap() {
                                var map;
                                var bounds = new google.maps.LatLngBounds();
                                var mapOptions = {
                                    mapTypeId: 'roadmap'
                                };

                                map = new google.maps.Map(document.getElementById('mapa-estabelecimentos'), mapOptions);
                                map.setTilt(45);

                                var markers = [
                                    @foreach($estabelecimentos as $e)
                                    ['{{ $e->nome }}', {{ $e->lat }}, {{ $e->lng }}],
                                    @endforeach
                                ];

                                var infoWindowContent = [
                                    @foreach($estabelecimentos as $e)
                                    ['<div class="info_content"><h3>{{ $e->nome }}</h3><p>{{ $e->endereco }}<br>{{ $e->bairro }}<br>{{ $e->cep }} - {{ $e->cidade }} - {{ $e->estado }}</p></div>'],
                                    @endforeach
                                ];

                                var infoWindow = new google.maps.InfoWindow(), marker, i;

                                for( i = 0; i < markers.length; i++ ) {
                                    var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                                    bounds.extend(position);
                                    marker = new google.maps.Marker({
                                        position: position,
                                        map: map,
                                        title: markers[i][0]
                                    });

                                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                        return function() {
                                            infoWindow.setContent(infoWindowContent[i][0]);
                                            infoWindow.open(map, marker);
                                        }
                                    })(marker, i));

                                    map.fitBounds(bounds);
                                }

                                google.maps.event.addDomListener(window, 'resize', function() {
                                    map.fitBounds(bounds);
                                });
                            }
                        </script>
                        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBT3CH62ku8FLHfDRO392Pc4ozZ5qJSvVs&callback=initMap"></script>
                    </div>
                    <div>
                        @foreach($estabelecimentos as $estabelecimento)
                        <div class="estabelecimento">
                            <h3>{{ $estabelecimento->nome }}</h3>
                            <span class="telefone-handle" data-url="{{ route('aparelhos.telefone', $estabelecimento->id) }}">VER TELEFONE &raquo;</span>
                            <p>
                                {{ $estabelecimento->endereco }}, {{ $estabelecimento->numero }}{{ $estabelecimento->complemento ? '- '.$estabelecimento->complemento : '' }}<br>
                                {{ $estabelecimento->bairro }}<br>
                                {{ $estabelecimento->cep }} - {{ $estabelecimento->cidade }} - {{ $estabelecimento->uf }}
                            </p>
                            @if($estabelecimento->website)
                            <a href="{{ $estabelecimento->website }}" target="_blank">ir para o site</a>
                            @endif
                            <div class="avaliacao">
                                <p>CLASSIFIQUE:</p>
                                <div class="estrelas estrelas-avaliar" data-tipo="estabelecimento" data-id="{{ $estabelecimento->id }}">
                                    @if($estabelecimento->avaliacao)
                                        @foreach(range(1, 5) as $estrela)
                                        <span data-avaliacao="{{ $estrela }}" @if($estabelecimento->avaliacao->avaliacao >= $estrela) class="avaliado" @endif></span>
                                        @endforeach
                                    @else
                                        <span data-avaliacao="1"></span>
                                        <span data-avaliacao="2"></span>
                                        <span data-avaliacao="3"></span>
                                        <span data-avaliacao="4"></span>
                                        <span data-avaliacao="5"></span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @else
                    <p>NENHUM ESTABELECIMENTO ENCONTRADO</p>
                    @endif
                </div>
            </div>

            <div class="aparelhos-voltar">
                <div class="center">
                    <a href="{{ $linkVoltar }}">Veja mais detalhes de outros aparelhos</a>
                </div>
            </div>
        </div>
    </div>

@endsection
