@extends('frontend.common.template')

@section('content')

    <div class="main aparelhos">
        <div class="aparelhos-index">
            <div class="center">
                <h1>Qual aparelho auditivo para você e onde encontrá-lo?</h1>
                <p>SELECIONE SUAS NECESSIDADES DE ACORDO COM OS AMBIENTES QUE MAIS FREQUENTA:</p>
                <form action="{{ route('aparelhos.post') }}" method="POST">
                    {!! csrf_field() !!}
                    <label for="conversar_com_uma_pessoa">
                        <img src="{{ asset('assets/img/layout/selecao1.png') }}" alt="">
                        <input type="checkbox" name="conversar_com_uma_pessoa" id="conversar_com_uma_pessoa" value="1">
                        <span class="tooltip">CONVERSAR COM UMA PESSOA</span>
                    </label>
                    <label for="ambientes_internos">
                        <img src="{{ asset('assets/img/layout/selecao2.png') }}" alt="">
                        <input type="checkbox" name="ambientes_internos" id="ambientes_internos" value="1">
                        <span class="tooltip">AMBIENTES INTERNOS</span>
                    </label>
                    <label for="conversar_em_grupo">
                        <img src="{{ asset('assets/img/layout/selecao3.png') }}" alt="">
                        <input type="checkbox" name="conversar_em_grupo" id="conversar_em_grupo" value="1">
                        <span class="tooltip">CONVERSAR EM GRUPO</span>
                    </label>
                    <label for="falar_ao_telefone">
                        <img src="{{ asset('assets/img/layout/selecao4.png') }}" alt="">
                        <input type="checkbox" name="falar_ao_telefone" id="falar_ao_telefone" value="1">
                        <span class="tooltip">FALAR AO TELEFONE</span>
                    </label>
                    <label for="assistir_tv">
                        <img src="{{ asset('assets/img/layout/selecao5.png') }}" alt="">
                        <input type="checkbox" name="assistir_tv" id="assistir_tv" value="1">
                        <span class="tooltip">ASSISTIR TV</span>
                    </label>
                    <label for="ambientes_ao_ar_livre">
                        <img src="{{ asset('assets/img/layout/selecao6.png') }}" alt="">
                        <input type="checkbox" name="ambientes_ao_ar_livre" id="ambientes_ao_ar_livre" value="1">
                        <span class="tooltip">AMBIENTES AO AR LIVRE</span>
                    </label>
                    <label for="showsconcertos_e_teatro">
                        <img src="{{ asset('assets/img/layout/selecao7.png') }}" alt="">
                        <input type="checkbox" name="showsconcertos_e_teatro" id="showsconcertos_e_teatro" value="1">
                        <span class="tooltip">SHOWS, CONCERTOS E TEATRO</span>
                    </label>
                    <label for="restaurantes">
                        <img src="{{ asset('assets/img/layout/selecao8.png') }}" alt="">
                        <input type="checkbox" name="restaurantes" id="restaurantes" value="1">
                        <span class="tooltip">RESTAURANTES</span>
                    </label>
                    <label for="festas_e_ambientes_muito_ruidosos">
                        <img src="{{ asset('assets/img/layout/selecao9.png') }}" alt="">
                        <input type="checkbox" name="festas_e_ambientes_muito_ruidosos" id="festas_e_ambientes_muito_ruidosos" value="1">
                        <span class="tooltip">FESTAS E AMBIENTES MUITO RUIDOSOS</span>
                    </label>
                    <input type="submit" value="LOCALIZAR" disabled>
                </form>
            </div>
        </div>
        <div class="aparelhos-texto">
            <div class="center">
                {!! $texto->texto !!}
            </div>
        </div>
    </div>

@endsection
