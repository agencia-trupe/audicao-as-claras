@extends('frontend.common.template')

@section('content')

    <div class="main aparelhos">
        <div class="aparelhos-dados">
            <div class="center">
                <h1>Saiba qual o aparelho auditivo que melhor atende sua necessidade e onde encontrá-lo</h1>
                <p>INFORME SEUS DADOS PARA ENVIARMOS O RESULTADO DESSA CONSULTA:</p>
                <div class="dados">
                    <div class="col">
                        <form action="{{ route('cadastro.cadastroPost') }}" id="form-cadastro-aparelhos" method="POST">
                            <input type="text" name="nome" id="cadastro-aparelhos-nome" placeholder="nome" required>
                            <input type="email" name="email" id="cadastro-aparelhos-email" placeholder="e-mail" required>
                            <input type="text" name="telefone" id="cadastro-aparelhos-telefone" placeholder="telefone">
                            <input type="text" name="cep" id="cadastro-aparelhos-cep" class="cep-mask" placeholder="CEP" required>
                            <input type="password" name="senha" id="cadastro-aparelhos-senha" placeholder="cadastre uma senha" required>
                            <input type="password" name="senha_confirmation" id="cadastro-aparelhos-senha-confirmation" placeholder="repetir senha" required>
                            <input type="submit" value="CADASTRAR">
                            <div id="form-cadastro-aparelhos-response" class="response"></div>
                        </form>
                    </div>
                    <div class="col">
                        <div class="wrapper-login">
                            <a href="#" class="btn-entrar">
                                <p>JÁ SOU CADASTRADO</p>
                                <span>ENTRAR</span>
                            </a>
                            <div class="login-aparelhos">
                                <form action="{{ route('cadastro.loginPost') }}" id="form-login-aparelhos" method="POST">
                                    <p>LOGIN</p>
                                    <input type="email" name="email" id="login-aparelhos-email" placeholder="e-mail" required>
                                    <input type="password" name="senha" id="login-aparelhos-senha" placeholder="senha" required>
                                    <input type="submit" value="ENTRAR">
                                    <a href="{{ route('cadastro.esqueci') }}" class="login-lightbox btn-esqueci">esqueci minha senha &raquo;</a>
                                </form>
                            </div>
                        </div>
                        <div id="form-login-aparelhos-response" class="response"></div>
                    </div>
                </div>
                <p>DE ACORDO COM AS NECESSIDADES APONTADAS:</p>
                <div class="necessidades">
                    @if(array_key_exists('conversar_com_uma_pessoa', $input))
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao1.png') }}" alt="">
                        <input type="hidden" name="conversar_com_uma_pessoa" id="conversar_com_uma_pessoa" value="1">
                        <span>CONVERSAR COM UMA PESSOA</span>
                    </label>
                    @endif
                    @if(array_key_exists('ambientes_internos', $input))
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao2.png') }}" alt="">
                        <input type="hidden" name="ambientes_internos" id="ambientes_internos" value="1">
                        <span>AMBIENTES INTERNOS</span>
                    </label>
                    @endif
                    @if(array_key_exists('conversar_em_grupo', $input))
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao3.png') }}" alt="">
                        <input type="hidden" name="conversar_em_grupo" id="conversar_em_grupo" value="1">
                        <span>CONVERSAR EM GRUPO</span>
                    </label>
                    @endif
                    @if(array_key_exists('falar_ao_telefone', $input))
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao4.png') }}" alt="">
                        <input type="hidden" name="falar_ao_telefone" id="falar_ao_telefone" value="1">
                        <span>FALAR AO TELEFONE</span>
                    </label>
                    @endif
                    @if(array_key_exists('assistir_tv', $input))
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao5.png') }}" alt="">
                        <input type="hidden" name="assistir_tv" id="assistir_tv" value="1">
                        <span>ASSISTIR TV</span>
                    </label>
                    @endif
                    @if(array_key_exists('ambientes_ao_ar_livre', $input))
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao6.png') }}" alt="">
                        <input type="hidden" name="ambientes_ao_ar_livre" id="ambientes_ao_ar_livre" value="1">
                        <span>AMBIENTES AO AR LIVRE</span>
                    </label>
                    @endif
                    @if(array_key_exists('showsconcertos_e_teatro', $input))
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao7.png') }}" alt="">
                        <input type="hidden" name="showsconcertos_e_teatro" id="showsconcertos_e_teatro" value="1">
                        <span>SHOWS, CONCERTOS E TEATRO</span>
                    </label>
                    @endif
                    @if(array_key_exists('restaurantes', $input))
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao8.png') }}" alt="">
                        <input type="hidden" name="restaurantes" id="restaurantes" value="1">
                        <span>RESTAURANTES</span>
                    </label>
                    @endif
                    @if(array_key_exists('festas_e_ambientes_muito_ruidosos', $input))
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao9.png') }}" alt="">
                        <input type="hidden" name="festas_e_ambientes_muito_ruidosos" id="festas_e_ambientes_muito_ruidosos" value="1">
                        <span>FESTAS E AMBIENTES MUITO RUIDOSOS</span>
                    </label>
                    @endif
                </div>
            </div>
        </div>

        @include('frontend.aparelhos._busca-estabelecimentos', [
            'frase' => 'OU BUSQUE POR FORNECEDOR / PROFISSIONAL:'
        ])
    </div>

@endsection
