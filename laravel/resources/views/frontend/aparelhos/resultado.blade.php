@extends('frontend.common.template')

@section('content')

    <div class="main aparelhos">
        <div class="aparelhos-dados">
            <div class="center">
                <h1>Saiba qual o aparelho auditivo que melhor atende sua necessidade e onde encontrá-lo</h1>
                <p>DE ACORDO COM AS NECESSIDADES APONTADAS:</p>
                <div class="necessidades">
                    @if(array_key_exists('conversar_com_uma_pessoa', $necessidades) && $necessidades['conversar_com_uma_pessoa'])
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao1.png') }}" alt="">
                        <input type="hidden" name="conversar_com_uma_pessoa" id="conversar_com_uma_pessoa" value="1">
                        <span>CONVERSAR COM UMA PESSOA</span>
                    </label>
                    @endif
                    @if(array_key_exists('ambientes_internos', $necessidades) && $necessidades['ambientes_internos'])
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao2.png') }}" alt="">
                        <input type="hidden" name="ambientes_internos" id="ambientes_internos" value="1">
                        <span>AMBIENTES INTERNOS</span>
                    </label>
                    @endif
                    @if(array_key_exists('conversar_em_grupo', $necessidades) && $necessidades['conversar_em_grupo'])
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao3.png') }}" alt="">
                        <input type="hidden" name="conversar_em_grupo" id="conversar_em_grupo" value="1">
                        <span>CONVERSAR EM GRUPO</span>
                    </label>
                    @endif
                    @if(array_key_exists('falar_ao_telefone', $necessidades) && $necessidades['falar_ao_telefone'])
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao4.png') }}" alt="">
                        <input type="hidden" name="falar_ao_telefone" id="falar_ao_telefone" value="1">
                        <span>FALAR AO TELEFONE</span>
                    </label>
                    @endif
                    @if(array_key_exists('assistir_tv', $necessidades) && $necessidades['assistir_tv'])
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao5.png') }}" alt="">
                        <input type="hidden" name="assistir_tv" id="assistir_tv" value="1">
                        <span>ASSISTIR TV</span>
                    </label>
                    @endif
                    @if(array_key_exists('ambientes_ao_ar_livre', $necessidades) && $necessidades['ambientes_ao_ar_livre'])
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao6.png') }}" alt="">
                        <input type="hidden" name="ambientes_ao_ar_livre" id="ambientes_ao_ar_livre" value="1">
                        <span>AMBIENTES AO AR LIVRE</span>
                    </label>
                    @endif
                    @if(array_key_exists('showsconcertos_e_teatro', $necessidades) && $necessidades['showsconcertos_e_teatro'])
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao7.png') }}" alt="">
                        <input type="hidden" name="showsconcertos_e_teatro" id="showsconcertos_e_teatro" value="1">
                        <span>SHOWS, CONCERTOS E TEATRO</span>
                    </label>
                    @endif
                    @if(array_key_exists('restaurantes', $necessidades) && $necessidades['restaurantes'])
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao8.png') }}" alt="">
                        <input type="hidden" name="restaurantes" id="restaurantes" value="1">
                        <span>RESTAURANTES</span>
                    </label>
                    @endif
                    @if(array_key_exists('festas_e_ambientes_muito_ruidosos', $necessidades) && $necessidades['festas_e_ambientes_muito_ruidosos'])
                    <label>
                        <img src="{{ asset('assets/img/layout/selecao9.png') }}" alt="">
                        <input type="hidden" name="festas_e_ambientes_muito_ruidosos" id="festas_e_ambientes_muito_ruidosos" value="1">
                        <span>FESTAS E AMBIENTES MUITO RUIDOSOS</span>
                    </label>
                    @endif
                </div>
                @if(count($aparelhos))
                <p>OS APARELHOS INDICADOS SÃO:</p>

                <div class="filtros">
                    {!! Form::select('filtro-aparelhos-modelo', $modelos->lists('titulo', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Filtrar por MODELO (Selecione)']) !!}
                    {!! Form::select('filtro-aparelhos-marca', $marcas->lists('titulo', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Filtrar por MARCA (Selecione)']) !!}
                </div>

                @foreach($aparelhos->lists('classificacao')->unique()->sort()->reverse() as $c)
                <div class="classificacao">
                    @for ($i = 0; $i < $c; $i++)
                        <span class="estrela"></span>
                    @endfor
                    @for ($i = 0; $i < 5 - $c; $i++)
                        <span class="vazio"></span>
                    @endfor
                </div>
                <div class="aparelhos-thumbs">
                    @foreach($aparelhos->where('classificacao', $c) as $aparelho)
                    <a href="{{ Tools::geraRotaDetalhes($aparelho->id, $necessidades) }}" data-marca="{{ $aparelho->marca_id }}" data-modelo="{{ $aparelho->modelo_id }}">
                        <img src="{{ asset('assets/img/aparelhos/'.$aparelho->imagem) }}" alt="">
                        <span>{{ $aparelho->marca ? $aparelho->marca->titulo : '' }}<br>{{ $aparelho->nome }}</span>
                    </a>
                    @endforeach
                </div>
                @endforeach
                @else
                <p>NENHUM APARELHO ENCONTRADO</p>
                @endif
            </div>

            @include('frontend.aparelhos._busca-estabelecimentos', [
                'frase' => 'OU BUSQUE POR FORNECEDOR / PROFISSIONAL:'
            ])
        </div>
    </div>

@endsection
