@extends('frontend.common.template')

@section('content')

    <div class="main politica-de-privacidade">
        <div class="titulo">
            <div class="center">
                <h1>Política de Privacidade</h1>
            </div>
        </div>

        <div class="center">
            <div class="texto">
                {!! $politica->texto !!}
            </div>
        </div>
    </div>

@endsection
