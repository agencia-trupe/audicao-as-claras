@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Perguntas Recebidas</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $pergunta->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $pergunta->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">
            <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $pergunta->email }}" style="margin-right:5px;border:0;transition:background .3s">
                <span class="glyphicon glyphicon-copy"></span>
            </button>
            {{ $pergunta->email }}
        </div>
    </div>

@if($pergunta->telefone)
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $pergunta->telefone }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Pergunta</label>
        <div class="well">{{ $pergunta->pergunta }}</div>
    </div>

    <a href="{{ route('painel.perguntas-recebidas.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
