@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::textarea('titulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_1_texto', 'Chamada 1 Texto') !!}
            {!! Form::textarea('chamada_1_texto', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_1_botao', 'Chamada 1 Botão') !!}
            {!! Form::text('chamada_1_botao', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_1_link', 'Chamada 1 Link') !!}
            {!! Form::text('chamada_1_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('chamada_2_texto', 'Chamada 2 Texto') !!}
            {!! Form::textarea('chamada_2_texto', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_2_botao', 'Chamada 2 Botão') !!}
            {!! Form::text('chamada_2_botao', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('chamada_2_link', 'Chamada 2 Link') !!}
            {!! Form::text('chamada_2_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
