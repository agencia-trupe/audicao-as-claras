@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Chamadas Home</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.chamadas-home.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.chamadas-home.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
