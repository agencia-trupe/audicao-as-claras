@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Avisos /</small> Adicionar Aviso</h2>
    </legend>

    {!! Form::open(['route' => 'painel.avisos.store', 'files' => true]) !!}

        @include('painel.avisos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
