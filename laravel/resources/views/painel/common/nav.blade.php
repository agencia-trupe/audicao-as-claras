<style>
    .navbar-nav > li > a { padding-left: 12px !important; padding-right: 12px !important; }
</style>
<ul class="nav navbar-nav">
    <li class="dropdown @if(str_is('painel.cabecalho*', Route::currentRouteName()) || str_is('painel.chamadas-home*', Route::currentRouteName()) || str_is('painel.empresa*', Route::currentRouteName()) || str_is('painel.faq*', Route::currentRouteName()) || str_is('painel.politica-de-privacidade*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Páginas
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.cabecalho*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.cabecalho.index') }}">Cabeçalho</a>
            </li>
            <li @if(str_is('painel.chamadas-home*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.chamadas-home.index') }}">Home</a>
            </li>
            <li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.empresa.index') }}">Empresa</a>
            </li>
            <li @if(str_is('painel.faq*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.faq.index') }}">FAQ</a>
            </li>
            <li class="divider"></li>
            <li @if(str_is('painel.politica-de-privacidade*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.politica-de-privacidade.index') }}">Política de Privacidade</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.aparelhos*', Route::currentRouteName()) || str_is('painel.consultas*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Aparelhos
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.aparelhos*', Route::currentRouteName())&& !str_is('painel.aparelhos-texto*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.aparelhos.index') }}">Aparelhos</a>
            </li>
            <li @if(str_is('painel.aparelhos-texto*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.aparelhos-texto.index') }}">Texto</a>
            </li>
            <li @if(str_is('painel.consultas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.consultas.index') }}">Consultas</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.estabelecimentos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.estabelecimentos.index') }}">Estabelecimentos</a>
    </li>
    <li class="dropdown @if(str_is('painel.especialistas*', Route::currentRouteName()) || str_is('painel.resposta*', Route::currentRouteName()) || str_is('painel.perguntas-recebidas.*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Especialistas
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.especialistas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.especialistas.index') }}">Especialistas</a>
            </li>
            <li @if(str_is('painel.respostas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.respostas.index') }}">Respostas</a>
            </li>
            <li @if(str_is('painel.resposta-em-destaque*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.resposta-em-destaque.index') }}">Resposta em destaque</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.avisos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.avisos.index') }}">Avisos</a>
    </li>
    <li @if(str_is('painel.aulas*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.aulas.index') }}">Aulas</a>
    </li>
    <li @if(str_is('painel.blog*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.blog.index') }}">Blog</a>
    </li>
    <li class="dropdown @if(str_is('painel.ebook*', Route::currentRouteName()) || str_is('painel.downloads*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Ebook
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
        	<li @if(str_is('painel.ebook*', Route::currentRouteName())) class="active" @endif>
        		<a href="{{ route('painel.ebook.index') }}">Ebook</a>
        	</li>
            <li @if(str_is('painel.downloads*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.downloads.index') }}">Downloads</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName()) || str_is('painel.newsletter*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.contato.index', Route::currentRouteName())) class="active" @endif><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li @if(str_is('painel.contato.recebidos.*', Route::currentRouteName())) class="active" @endif><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
            <li class="divider"></li>
            <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
            </li>
        </ul>
        <li @if(str_is('painel.cadastros*', Route::currentRouteName())) class="active" @endif>
            <a href="{{ route('painel.cadastros.index') }}">Cadastros</a>
        </li>
    </li>
</ul>
