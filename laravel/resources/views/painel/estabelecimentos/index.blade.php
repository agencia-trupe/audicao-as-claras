@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Estabelecimentos
            <div class="btn-group pull-right">
                <a href="{{ route('painel.estabelecimentos.exibicoes') }}" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-star" style="margin-right:10px;"></span>Exibições</a>
            </div>
        </h2>
    </legend>

    <div class="btn-group">
        <a href="{{ route('painel.estabelecimentos.index') }}" class="btn btn-default  @if(!Request::get('aprovacao')) active @endif">Aprovados</a>
        <a href="{{ route('painel.estabelecimentos.index', ['aprovacao' => 1]) }}" class="btn btn-default @if(Request::get('aprovacao')) active @endif">
            Aguardando aprovação
            @if($countAprovacao >= 1)
            <span class="label label-info" style="margin-left:5px;">
                {{ $countAprovacao }}
            </span>
            @endif
        </a>
    </div>

    <hr>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Nome</th>
                @if(!Request::get('aprovacao'))
                <th style="text-align: center">Avaliação</th>
                @endif
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
                @if(!Request::get('aprovacao'))
                <td style="text-align: center">
                    {{ $registro->avaliacao_clientes }}<br>
                    <small>({{ $registro->avaliacoes->count() }} {{ $registro->avaliacoes->count() == 1 ? 'avaliação' : 'avaliações' }})</small>
                </td>
                @endif
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.estabelecimentos.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.estabelecimentos.edit', $registro->id ) }}" class="btn btn-info btn-sm pull-left">
                            <span class="glyphicon glyphicon-user" style="margin-right:10px;"></span>Ver dados
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
