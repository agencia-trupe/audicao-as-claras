@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.estabelecimentos.index') }}" class="btn btn-default btn-sm">
        &larr; Voltar para Estabelecimentos
    </a>

    <legend>
        <h2><small>Estabelecimentos / </small> Exibições</h2>
    </legend>


    @if(!count($estabelecimentos))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Exibições</th>
            </tr>
        </thead>

        <tbody>
        @foreach ($estabelecimentos as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
                <td>{{ $registro->exibicoes }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
