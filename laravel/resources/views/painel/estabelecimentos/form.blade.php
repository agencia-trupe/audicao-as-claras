@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('razao_social', 'Razão Social') !!}
    {!! Form::text('razao_social', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cnpj', 'CNPJ') !!}
    {!! Form::text('cnpj', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('area_de_atuacao', 'Área de Atuação') !!}
    {!! Form::text('area_de_atuacao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('responsavel', 'Responsável') !!}
    {!! Form::text('responsavel', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cep', 'CEP') !!}
    {!! Form::text('cep', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('numero', 'Número') !!}
    {!! Form::text('numero', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('complemento', 'Complemento') !!}
    {!! Form::text('complemento', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('bairro', 'Bairro') !!}
    {!! Form::text('bairro', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cidade', 'Cidade') !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('uf', 'UF') !!}
    {!! Form::text('uf', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('website', 'Website') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::label('marcas', 'Marcas com que trabalha') !!}
<div class="well">
    <div class="row">
        @foreach($marcas as $marca)
        <div class="col-md-3">
            <label style="font-weight:normal">
                <input type="checkbox" name="marcas[]" value="{{ $marca->id }}" @if($registro->marcas->contains($marca->id)) checked @endif>
                <span>{{ $marca->titulo }}</span>
            </label>
        </div>
        @endforeach
    </div>
</div>


@if($registro->aprovado)
    {!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
    <a href="{{ route('painel.estabelecimentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
@else
    <a href="{{ route('painel.estabelecimentos.aprovar', $registro->id) }}" class="btn btn-success">
        <span class="glyphicon glyphicon-ok" style="margin-right:10px;"></span>
        Aprovar
    </a>
    {!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
    <a href="{{ route('painel.estabelecimentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
@endif
