@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Estabelecimentos /</small> Adicionar Estabelecimento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.estabelecimentos.store', 'files' => true]) !!}

        @include('painel.estabelecimentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
