@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Ebook</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.ebook.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.ebook.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
