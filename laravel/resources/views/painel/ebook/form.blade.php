@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('arquivo', 'Arquivo') !!}
    @if($registro->arquivo)
    <p>
        <a href="{{ url('assets/ebook/'.$registro->arquivo) }}" target="_blank" style="display:block;">{{ $registro->arquivo }}</a>
        <a href="{{ route('painel.ebook.delete') }}" class="btn-delete btn-delete-link label label-danger" style="display:inline-block;margin-bottom:10px">
            <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
            REMOVER
        </a>
    </p>
    @endif
    {!! Form::file('arquivo', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
