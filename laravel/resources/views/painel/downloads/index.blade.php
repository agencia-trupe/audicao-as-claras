@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Downloads
            @if(count($downloads))
            <a href="{{ route('painel.downloads.exportar') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>Exportar CSV</a>
            @endif
        </h2>
    </legend>

    @if(!count($downloads))
    <div class="alert alert-warning" role="alert">Nenhum download cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover ">
        <thead>
            <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($downloads as $download)

            <tr class="tr-row">
                <td>{{ $download->nome }}</td>
                <td>{{ $download->email }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.downloads.destroy', $download->id],
                        'method' => 'delete'
                    ]) !!}

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {!! $downloads->render() !!}
    @endif

@stop
