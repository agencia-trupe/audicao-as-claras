@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Aparelhos Texto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aparelhos-texto.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aparelhos-texto.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
