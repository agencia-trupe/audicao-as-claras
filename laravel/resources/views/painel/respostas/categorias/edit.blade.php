@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Respostas /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.respostas.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.respostas.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
