@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Respostas /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.respostas.categorias.store']) !!}

        @include('painel.respostas.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
