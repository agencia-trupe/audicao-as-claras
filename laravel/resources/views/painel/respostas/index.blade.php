@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Respostas</h2>
    </legend>

    <div class="btn-group">
        <a href="{{ route('painel.respostas.index') }}" class="btn btn-default  @if(!Request::get('avaliacao')) active @endif">Publicadas</a>
        <a href="{{ route('painel.respostas.index', ['avaliacao' => 1]) }}" class="btn btn-default @if(Request::get('avaliacao')) active @endif">
            Para avaliação
            @if($countAvaliacao >= 1)
            <span class="label label-info" style="margin-left:5px;">
                {{ $countAvaliacao }}
            </span>
            @endif
        </a>
    </div>

    <hr>

    @if(!Request::get('avaliacao'))
    <div class="row" style="margin-bottom:20px">
        <div class="form-group col-sm-4">
            {!! Form::select('filtro', $categorias, Request::get('filtro'), ['class' => 'form-control', 'id' => 'filtro-select', 'placeholder' => 'Todas as Categorias', 'data-route' => 'painel/respostas']) !!}
        </div>
        <div class="col-sm-4" style="padding-left:0">
        <a href="{{ route('painel.respostas.categorias.index') }}" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span><small>Editar Categorias</small></a>
        </div>
    </div>
    @endif

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @elseif(Request::get('avaliacao'))
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Especialista</th>
                <th>Pergunta</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->especialistaModel->nome }}</td>
                <td>{{ $registro->pergunta }}</td>
                <td class="crud-actions">
                    <a href="{{ route('painel.respostas.avaliar', $registro->id ) }}" class="btn btn-success btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Avaliar
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                @if(!$filtro)<th>Categoria</th>@endif
                <th>Especialista</th>
                <th>Pergunta</th>
                <th style="text-align:center">Avaliação</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                @if(!$filtro)
                <td>
                    @if($registro->categoria)
                    {{ $registro->categoria->titulo }}
                    @else
                    <span class="label label-warning">sem categoria</span>
                    @endif
                </td>
                @endif
                <td>
                    @if($registro->especialistaModel)
                    {{ $registro->especialistaModel->nome }}
                    @else
                    <span class="label label-warning">sem especialista</span>
                    @endif
                </td>
                <td>{{ $registro->pergunta }}</td>
                <td style="text-align:center">
                    {{ $registro->avaliacao_clientes }}<br>
                    <small>({{ $registro->avaliacoes->count() }} {{ $registro->avaliacoes->count() == 1 ? 'avaliação' : 'avaliações' }})</small>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.respostas.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.respostas.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
