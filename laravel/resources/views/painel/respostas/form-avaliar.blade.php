@include('painel.common.flash')

<div class="well">
    <p style="margin-bottom:0">
        <strong>Nome:</strong>
        {{ $registro->nome }}<br>
        <strong>E-mail:</strong>
        {{ $registro->email }}<br>
        <strong>Telefone:</strong>
        {{ $registro->telefone }}<br>
    </p>
</div>

<div class="form-group">
    {!! Form::label('respostas_categoria_id', 'Categoria') !!}
    {!! Form::select('respostas_categoria_id', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('especialista', 'Especialista') !!}
    <input type="hidden" name="especialista" value="{{ $registro->especialista }}">
    <p>{{ $registro->especialistaModel->nome }}</p>
</div>

<div class="form-group">
    {!! Form::label('pergunta', 'Pergunta') !!}
    {!! Form::textarea('pergunta', null, ['class' => 'form-control', 'style' => 'height:150px']) !!}
</div>

<div class="form-group">
    {!! Form::label('resposta', 'Resposta') !!}
    {!! Form::textarea('resposta', nl2br($registro->resposta), ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
</div>

<input type="hidden" name="aprovado" value="1">

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
<a href="{{ route('painel.respostas.rejeitar', $registro->id) }}" class="btn btn-danger">Rejeitar</a><br><br>

<a href="{{ route('painel.respostas.index', ['avaliacao' => 1]) }}" class="btn btn-default btn-voltar">Voltar</a>
