@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Respostas /</small> Editar Resposta</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.respostas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.respostas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
