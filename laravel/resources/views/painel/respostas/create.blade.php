@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Respostas /</small> Adicionar Resposta</h2>
    </legend>

    {!! Form::open(['route' => 'painel.respostas.store', 'files' => true]) !!}

        @include('painel.respostas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
