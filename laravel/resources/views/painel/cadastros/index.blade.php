@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Cadastros
            @if(count($registros))
            <a href="{{ route('painel.cadastros.exportar') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>Exportar CSV</a>
            @endif
        </h2>
    </legend>

    <form action="{{ route('painel.cadastros.index') }}" method="GET">
        <div class="well">
            <div class="row">
                <div class="col-md-4">
                    <label>Palavra-chave:</label>
                    <input type="text" name="palavra_chave" value="{{ Request::get('palavra_chave') }}" placeholder="palavra-chave" class="form-control">
                </div>
                <div class="col-md-3">
                    <label>De:</label>
                    <input type="text" name="data_inicio" value="{{ Request::get('data_inicio') }}" class="datepicker datepicker-no-default form-control" placeholder="data inicial">
                </div>
                <div class="col-md-3">
                    <label>Até:</label>
                    <input type="text" name="data_final" value="{{ Request::get('data_final') }}" class="datepicker datepicker-no-default form-control" placeholder="data final">
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <button class="btn btn-block btn-info">
                        <span class="glyphicon glyphicon-search" style="margin-right:10px"></span>
                        FILTRAR
                    </button>
                </div>
            </div>
        </div>
    </form>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Data de Cadastro</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>CEP</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->data }}</td>
                <td>{{ $registro->nome }}</td>
                <td>{{ $registro->email }}</td>
                <td>{{ $registro->telefone }}</td>
                <td>{{ $registro->cep }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.cadastros.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $registros->appends($_GET)->links() !!}
    @endif

@endsection
