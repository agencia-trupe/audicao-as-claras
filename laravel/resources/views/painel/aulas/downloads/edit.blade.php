@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / {{ $aula->titulo }} / Downloads /</small> Editar Download</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.downloads.update', $aula->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aulas.downloads.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
