@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / {{ $aula->titulo }} / Downloads /</small> Adicionar Download</h2>
    </legend>

    {!! Form::open(['route' => ['painel.aulas.downloads.store', $aula->id], 'files' => true]) !!}

        @include('painel.aulas.downloads.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
