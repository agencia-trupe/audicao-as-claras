@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.aulas.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.aulas.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
