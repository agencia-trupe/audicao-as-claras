@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aulas.categorias.store']) !!}

        @include('painel.aulas.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
