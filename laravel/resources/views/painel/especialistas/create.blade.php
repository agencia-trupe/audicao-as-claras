@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Especialistas /</small> Adicionar Especialista</h2>
    </legend>

    {!! Form::open(['route' => 'painel.especialistas.store', 'files' => true]) !!}

        @include('painel.especialistas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
