@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.especialistas.index') }}" class="btn btn-default btn-sm">
        &larr; Voltar para Especialistas
    </a>

    <legend>
        <h2><small>Especialistas / </small> Exibições</h2>
    </legend>


    @if(!count($especialistas))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Exibições</th>
            </tr>
        </thead>

        <tbody>
        @foreach ($especialistas as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
                <td>{{ $registro->exibicoes }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
