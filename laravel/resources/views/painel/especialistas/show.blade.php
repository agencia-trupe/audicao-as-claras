@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Especialistas /</small> Dados do Especialista</h2>
    </legend>

    <div class="form-group">
        <label>Nome Completo</label>
        <div class="well">{{ $registro->nome }}</div>
    </div>
    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $registro->email }}</div>
    </div>
    <div class="form-group">
        <label>CPF</label>
        <div class="well">{{ $registro->cpf }}</div>
    </div>
    <div class="form-group">
        <label>Profissão/Cargo</label>
        <div class="well">{{ $registro->profissao_cargo }}</div>
    </div>
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $registro->telefone }}</div>
    </div>
    <div class="form-group">
        <label>CEP</label>
        <div class="well">{{ $registro->cep }}</div>
    </div>
    <div class="form-group">
        <label>Endereço</label>
        <div class="well">{{ $registro->endereco }}</div>
    </div>
    <div class="form-group">
        <label>Número</label>
        <div class="well">{{ $registro->numero }}</div>
    </div>
    @if($registro->complemento)
    <div class="form-group">
        <label>Complemento</label>
        <div class="well">{{ $registro->complemento }}</div>
    </div>
    @endif
    <div class="form-group">
        <label>Bairro</label>
        <div class="well">{{ $registro->bairro }}</div>
    </div>
    <div class="form-group">
        <label>Cidade</label>
        <div class="well">{{ $registro->cidade }}</div>
    </div>
    <div class="form-group">
        <label>UF</label>
        <div class="well">{{ $registro->uf }}</div>
    </div>
    <div class="form-group">
        <label>Nome para exibição</label>
        <div class="well">{{ $registro->nome_exibicao }}</div>
    </div>
    <div class="form-group">
        <label>Descrição</label>
        <div class="well">{{ $registro->descricao }}</div>
    </div>
    <div class="form-group">
        <label>Foto</label>
        <div class="well">
            @if($registro->imagem)
            <img src="{{ asset('assets/img/especialistas/'.$registro->imagem) }}" alt="">
            @else
            <img src="{{ asset('assets/img/layout/especialista.png') }}" alt="">
            @endif
        </div>
    </div>

    @if($registro->aprovado)
        <a href="{{ route('painel.especialistas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
    @else
        <a href="{{ route('painel.especialistas.aprovar', $registro->id) }}" class="btn btn-success">
            <span class="glyphicon glyphicon-ok" style="margin-right:10px;"></span>
            Aprovar
        </a>
        <a href="{{ route('painel.especialistas.index', ['aprovacao' => 1]) }}" class="btn btn-default btn-voltar">Voltar</a>
    @endif

@stop
