@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Especialistas /</small> Editar Especialista</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.especialistas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.especialistas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
