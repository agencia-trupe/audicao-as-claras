@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('frase', 'Frase') !!}
    {!! Form::text('frase', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
            <img src="{{ url('assets/img/cabecalho/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
            <img src="{{ url('assets/img/cabecalho/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
            <img src="{{ url('assets/img/cabecalho/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4 col-md-offset-2">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4') !!}
            <img src="{{ url('assets/img/cabecalho/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_5', 'Imagem 5') !!}
            <img src="{{ url('assets/img/cabecalho/'.$registro->imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_5', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
