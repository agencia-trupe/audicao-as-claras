@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Cabeçalho</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cabecalho.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cabecalho.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
