@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>FAQ /</small> Editar Pergunta</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.faq.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.faq.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
