@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem)
    <img src="{{ url('assets/img/resposta-em-destaque/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('pergunta', 'Pergunta (se nenhuma pergunta for selecionada o destaque não será exibido)') !!}
    {!! Form::select('pergunta', $perguntas, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
