@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Resposta em destaque</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.resposta-em-destaque.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.resposta-em-destaque.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
