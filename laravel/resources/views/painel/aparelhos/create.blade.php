@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aparelhos /</small> Adicionar Aparelho</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aparelhos.store', 'files' => true]) !!}

        @include('painel.aparelhos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
