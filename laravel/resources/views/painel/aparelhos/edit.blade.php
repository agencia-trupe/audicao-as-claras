@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aparelhos /</small> Editar Aparelho</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aparelhos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aparelhos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
