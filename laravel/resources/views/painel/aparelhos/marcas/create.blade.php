@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aparelhos / Marcas /</small> Adicionar Marca</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aparelhos.marcas.store', 'files' => true]) !!}

        @include('painel.aparelhos.marcas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
