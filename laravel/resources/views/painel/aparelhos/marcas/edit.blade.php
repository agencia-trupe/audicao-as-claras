@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aparelhos / Marcas /</small> Editar Marca</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aparelhos.marcas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aparelhos.marcas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
