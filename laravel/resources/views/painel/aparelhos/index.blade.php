@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Aparelhos
            <div class="btn-group pull-right">
                <a href="{{ route('painel.aparelhos.marcas.index') }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Editar Marcas</a>
                <a href="{{ route('painel.aparelhos.modelos.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Editar Modelos</a>
                <a href="{{ route('painel.aparelhos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Aparelho</a>
            </div>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Nome</th>
                <th>Imagem</th>
                <th style="text-align: center">Avaliação</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{!! $registro->marca ? $registro->marca->titulo : '<span class="label label-danger">sem marca</span>' !!}</td>
                <td>{!! $registro->modelo ? $registro->modelo->titulo : '<span class="label label-danger">sem modelo</span>' !!}</td>
                <td>{{ $registro->nome }}</td>
                <td><img src="{{ asset('assets/img/aparelhos/'.$registro->imagem) }}" style="width: 100%; max-width:80px;" alt=""></td>
                <td style="text-align: center">
                    {{ $registro->avaliacao_clientes }}<br>
                    <small>({{ $registro->avaliacoes->count() }} {{ $registro->avaliacoes->count() == 1 ? 'avaliação' : 'avaliações' }})</small>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.aparelhos.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.aparelhos.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $registros->render() !!}
    @endif

@endsection
