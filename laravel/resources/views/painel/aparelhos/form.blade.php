@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('marca_id', 'Marca') !!}
            {!! Form::select('marca_id', $marcas, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('modelo_id', 'Modelo') !!}
            {!! Form::select('modelo_id', $modelos, null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/aparelhos/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('ampliacao', 'Ampliação') !!}
@if($submitText == 'Alterar')
    @if(!$registro->ampliacao)
    <p class="alert alert-danger small" style="margin-bottom: 15px; height:45px; padding: 12px 15px;">
        <span class="glyphicon glyphicon-info-sign" style="margin-right:10px;"></span>
        Cadastre uma ampliação para este aparelho.
    </p>
    @else
    <img src="{{ url('assets/img/aparelhos/ampliacao/'.$registro->ampliacao) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
@endif
    {!! Form::file('ampliacao', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('caracteristicas', 'Características') !!}
    {!! Form::textarea('caracteristicas', null, ['class' => 'form-control ckeditor', 'data-editor' => 'caracteristicas']) !!}
</div>

<div class="form-group">
    {!! Form::label('classificacao', 'Classificação (esta seleção será utilizada apenas se o aparelho não tiver recebido nenhuma nota dos usuários)') !!}
    <br>
    @foreach(range(0, 5) as $i)
    <label class="radio-inline">
        <input type="radio" name="classificacao" value="{{ $i }}" @if(isset($registro) && $registro->classificacao == $i) checked @endif> {{ $i }}
    </label>
    @endforeach
</div>

<hr>

<div class="well">
    <label>Necessidades:</label>

    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="hidden" name="conversar_com_uma_pessoa" value="0">
                {!! Form::checkbox('conversar_com_uma_pessoa', 1) !!} Conversar com uma pessoa
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="hidden" name="ambientes_internos" value="0">
                {!! Form::checkbox('ambientes_internos', 1) !!} Ambientes internos
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="hidden" name="conversar_em_grupo" value="0">
                {!! Form::checkbox('conversar_em_grupo', 1) !!} Conversar em grupo
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="hidden" name="falar_ao_telefone" value="0">
                {!! Form::checkbox('falar_ao_telefone', 1) !!} Falar ao telefone
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="hidden" name="assistir_tv" value="0">
                {!! Form::checkbox('assistir_tv', 1) !!} Assistir TV
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="hidden" name="ambientes_ao_ar_livre" value="0">
                {!! Form::checkbox('ambientes_ao_ar_livre', 1) !!} Ambientes ao ar livre
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="hidden" name="showsconcertos_e_teatro" value="0">
                {!! Form::checkbox('showsconcertos_e_teatro', 1) !!} Shows/concertos e teatro
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="hidden" name="restaurantes" value="0">
                {!! Form::checkbox('restaurantes', 1) !!} Restaurantes
            </label>
        </div>
    </div>

    <div class="form-group" style="margin-bottom:0">
        <div class="checkbox" style="margin-bottom:0">
            <label>
                <input type="hidden" name="festas_e_ambientes_muito_ruidosos" value="0">
                {!! Form::checkbox('festas_e_ambientes_muito_ruidosos', 1) !!} Festas e ambientes muito ruidosos
            </label>
        </div>
    </div>
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.aparelhos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
