@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aparelhos / Modelos /</small> Editar Modelo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aparelhos.modelos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aparelhos.modelos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
