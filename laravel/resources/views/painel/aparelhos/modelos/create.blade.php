@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aparelhos / Modelos /</small> Adicionar Modelo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.aparelhos.modelos.store', 'files' => true]) !!}

        @include('painel.aparelhos.modelos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
