@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Consultas
            @if(count($consultas))
            <a href="{{ route('painel.consultas.exportar') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>Exportar CSV</a>
            @endif
        </h2>
    </legend>

    @if(!count($consultas))
    <div class="alert alert-warning" role="alert">Nenhuma consulta cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover ">
        <thead>
            <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>CEP</th>
                <th>Necessidades</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($consultas as $consulta)

            <tr class="tr-row">
                <td>{{ $consulta->nome }}</td>
                <td>{{ $consulta->email }}</td>
                <td>{{ $consulta->telefone }}</td>
                <td>{{ $consulta->cep }}</td>
                <td>
                    <p style="margin:0;font-size:.8em">
                        @if($consulta->conversar_com_uma_pessoa)
                            <span style="display:block">- conversar com uma pessoa</span>
                        @endif
                        @if($consulta->ambientes_internos)
                            <span style="display:block">- ambientes internos</span>
                        @endif
                        @if($consulta->conversar_em_grupo)
                            <span style="display:block">- conversar em grupo</span>
                        @endif
                        @if($consulta->falar_ao_telefone)
                            <span style="display:block">- falar ao telefone</span>
                        @endif
                        @if($consulta->assistir_tv)
                            <span style="display:block">- assistir tv</span>
                        @endif
                        @if($consulta->ambientes_ao_ar_livre)
                            <span style="display:block">- ambientes ao ar livre</span>
                        @endif
                        @if($consulta->showsconcertos_e_teatro)
                            <span style="display:block">- shows, concertos e teatro</span>
                        @endif
                        @if($consulta->restaurantes)
                            <span style="display:block">- restaurantes</span>
                        @endif
                        @if($consulta->festas_e_ambientes_muito_ruidosos)
                            <span style="display:block">- festas e ambientes muito ruidosos</span>
                        @endif
                    </p>
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.consultas.destroy', $consulta->id],
                        'method' => 'delete'
                    ]) !!}

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {!! $consultas->render() !!}
    @endif

@stop
