@extends('frontend.common.template')

@section('content')

<div class="especialistas-wrapper">
    <div class="center">
        <h2>PERGUNTE AO ESPECIALISTA</h2>
        
        <div class="pergunte-nav">
            <a href="{{ route('especialista.perguntas') }}" @if(Tools::isActive('especialista.perguntas') || Tools::isActive('especialista.perguntas.form')) class="active" @endif>
                Responder Pergunta
                @if($emAprovacao >= 1)
                <span class="label label-warning" style="margin-left:5px;">{{ $emAprovacao }} aguardando validação</span>
                @endif
            </a>
            <a href="{{ route('especialista.perguntas.minhas-respostas') }}" @if(Tools::isActive('especialista.perguntas.minhas-respostas')) class="active" @endif>
                Minhas Respostas
                @if($minhasRespostas >= 1)
                <span class="label label-success" style="margin-left:5px;">{{ $minhasRespostas }} </span>
                @endif
            </a>
        </div>
            
        @include('painel.common.flash')

        @if(!count($respostas))
        <div class="alert">Nenhuma resposta encontrada.</div>
        @else
        <div class="lista-respostas">
            @foreach($respostas as $pergunta)
            <div class="resposta">
                <span>
                    {!! nl2br($pergunta->pergunta) !!}
                    <strong>{!! nl2br($pergunta->resposta) !!}</strong>
                </span>
                <span>
                    nota: {{ $pergunta->avaliacao_clientes }}<br>
                    <small>({{ $pergunta->avaliacoes->count() }} {{ $pergunta->avaliacoes->count() == 1 ? 'avaliação' : 'avaliações' }})</small>
                </span>
            </div>
            @endforeach
        </div>
        @endif
    </div>
</div>

@endsection
