@extends('frontend.common.template')

@section('content')

<div class="especialistas-wrapper">
    <div class="center">
        <h2>PERGUNTE AO ESPECIALISTA</h2>
        
        <div class="pergunte-nav">
            <a href="{{ route('especialista.perguntas') }}" @if(Tools::isActive('especialista.perguntas') || Tools::isActive('especialista.perguntas.form')) class="active" @endif>
                Responder Pergunta
                @if($emAprovacao >= 1)
                <span class="label label-warning" style="margin-left:5px;">{{ $emAprovacao }} aguardando validação</span>
                @endif
            </a>
            <a href="{{ route('especialista.perguntas.minhas-respostas') }}" @if(Tools::isActive('especialista.perguntas.minhas-respostas')) class="active" @endif>
                Minhas Respostas
                @if($minhasRespostas >= 1)
                <span class="label label-success" style="margin-left:5px;">{{ $minhasRespostas }} </span>
                @endif
            </a>
        </div>
            
        @include('painel.common.flash')

        @if(!count($perguntas))
        <div class="alert">Nenhuma pergunta encontrada.</div>
        @else
        <div class="lista-perguntas">
            @foreach($perguntas as $pergunta)
            <a href="{{ route('especialista.perguntas.form', $pergunta->id) }}">
                {!! nl2br($pergunta->pergunta) !!}
            </a>
            @endforeach
        </div>
    
        {!! $perguntas->render() !!}
        @endif
    </div>
</div>

@endsection
