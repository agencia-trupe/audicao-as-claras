@extends('frontend.common.template')

@section('content')

<div class="especialistas-wrapper">
    <div class="center">
        <h2>PERGUNTE AO ESPECIALISTA</h2>
        
        <div class="pergunte-nav">
            <a href="{{ route('especialista.perguntas') }}" @if(Tools::isActive('especialista.perguntas') || Tools::isActive('especialista.perguntas.form')) class="active" @endif>
                Responder Pergunta
                @if($emAprovacao >= 1)
                <span class="label label-warning" style="margin-left:5px;">{{ $emAprovacao }} aguardando validação</span>
                @endif
            </a>
            <a href="{{ route('especialista.perguntas.minhas-respostas') }}" @if(Tools::isActive('especialista.perguntas.minhas-respostas')) class="active" @endif>
                Minhas Respostas
                @if($minhasRespostas >= 1)
                <span class="label label-success" style="margin-left:5px;">{{ $minhasRespostas }} </span>
                @endif
            </a>
        </div>
            
        {!! Form::open(['route' => [
            'especialista.perguntas.post', 
            $pergunta->id
        ], 'class' => 'form-pergunta']) !!}
        
            @include('painel.common.flash')

            <h1>
                <small>Pergunta:</small>
                {{ $pergunta->pergunta }}
            </h1>

            {!! Form::textarea('resposta', null, ['placeholder' => 'Resposta']) !!}

            {!! Form::submit('Enviar Resposta') !!}

            <a href="{{ route('especialista.perguntas') }}" class="btn-voltar">Voltar</a>

        {!! Form::close() !!}
    </div>
</div>

@endsection
