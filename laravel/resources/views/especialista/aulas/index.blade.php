@extends('frontend.common.template')

@section('content')

    <?php
        $route = auth('estabelecimento')->check() ? 'estabelecimento' : 'especialista';
    ?>

    <div class="especialistas-wrapper">
        <div class="center">
            <h2>AULAS</h2>

            <div class="especialistas-aulas">
                <div class="aulas-categorias">
                    @foreach($categorias as $cat)
                    <a href="{{ route($route.'.aulas', $cat->slug) }}" @if($cat->slug == $categoria->slug) class="active" @endif>{{ $cat->titulo }}</a>
                    @endforeach
                </div>

                <div class="aulas-thumbs">
                    @foreach($aulas as $aula)
                        <a href="{{ route($route.'.aula', [$aula->categoria->slug, $aula->slug]) }}">
                            <img src="{{ asset('assets/img/aulas/'.$aula->capa) }}" alt="">
                            <div class="texto">
                                <h3>{{ $aula->titulo }}</h3>
                                <p>{!! $aula->chamada !!}</p>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
