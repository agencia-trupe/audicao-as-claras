@extends('frontend.common.template')

@section('content')

    <div class="especialistas-wrapper">
        <div class="center">
            <h2>AULAS</h2>

            <div class="especialistas-aulas">
                <div class="aulas-categorias">
                    @foreach($categorias as $cat)
                    <a href="{{ route('especialista.aulas', $cat->slug) }}" @if($cat->slug == $aula->categoria->slug) class="active" @endif>{{ $cat->titulo }}</a>
                    @endforeach
                </div>
                
                <div class="aulas-show">
                    <div class="video-wrapper">
                        <iframe src="{{ $aula->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$aula->video_codigo : 'https://player.vimeo.com/video/'.$aula->video_codigo }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
                    <h1>{{ $aula->titulo }}</h1>
                    <p class="chamada">{!! $aula->chamada !!}</p>
                    <div class="texto">
                        <span class="informacoes">MAIS INFORMAÇÕES</span>
                        {!! $aula->informacoes !!}
                    </div>

                    @if(count($aula->downloads))
                    <div class="downloads">
                        <span>MATERIAL PARA DOWNLOAD</span>
                        @foreach($aula->downloads as $download)
                        <a href="{{ asset('downloads/'.$download->arquivo) }}" target="_blank">
                            {{ $download->titulo }}
                        </a>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection