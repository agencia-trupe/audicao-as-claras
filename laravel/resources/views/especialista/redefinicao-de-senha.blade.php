@extends('frontend.common.template')

@section('content')

    <div class="redefinir-senha">
        <div class="center">
            <h1>REDEFINIÇÃO DE SENHA</h1>

            <form action="{{ route('especialista.redefinir') }}" method="POST">
                {!! csrf_field() !!}

                @if($errors->any())
                <div class="error">{!! $errors->first() !!}</div>
                @endif

                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}" required>
                <input type="password" name="password" placeholder="senha" required>
                <input type="password" name="password_confirmation" placeholder="repetir senha" required>
                <input type="submit" value="REDEFINIR">
            </form>
        </div>
    </div>

@endsection
