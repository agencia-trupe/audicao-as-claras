@extends('frontend.common.template')

@section('content')

    <div class="especialistas-wrapper">
        <div class="center">
            <div class="lightbox-forms login-especialistas">
                <h2>
                    LOGIN<br>
                    <small style="font-size: .5em">
                        MÉDICOS &middot;
                        ESPECIALISTAS
                    </small>
                </h2>

                {!! Form::open(['route' => 'especialista.auth']) !!}
                @if(session('error'))
                <div class="lightbox-response">{{ session('error') }}</div>
                @endif

                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="password" name="password" placeholder="senha" required>
                <input type="submit" value="ENTRAR">
                <a href="{{ route('especialista.esqueci') }}" class="btn-esqueci">esqueci minha senha &raquo;</a>
                {!! Form::close() !!}

                <a href="{{ route('especialista.cadastro') }}" class="btn-cadastro">NÃO TENHO CADASTRO</a>
            </div>
        </div>
    </div>

@endsection
