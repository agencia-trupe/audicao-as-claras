@extends('frontend.common.template')

@section('content')

    <div class="especialistas-wrapper">
        <div class="center">
            <div class="cadastro-especialistas">
                <h2>MEUS DADOS</h2>
                <p>Mantenha seus dados de cadastro atualizados.</p>

                <form action="{{ route('especialista.update') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    {!! method_field('PATCH') !!}

                    @if($errors->any())
                    <div class="aviso-erros">Preencha todos os campos corretamente.</div>
                    @endif
                    @if(session('erroCoord'))
                    <div class="aviso-erros">{{ session('erroCoord') }}</div>
                    @endif
                    @if(session('success'))
                    <div class="aviso-sucesso">{{ session('success') }}</div>
                    @endif

                    <div class="col">
                        <input type="text" name="nome" placeholder="nome completo" value="{{ $registro->nome }}" @if($errors->has('nome')) class="erro" @endif>
                        <input type="email" name="email" placeholder="e-mail" value="{{ $registro->email }}" @if($errors->has('email')) class="erro" @endif>
                        <input type="cpf" name="cpf" placeholder="CPF" value="{{ $registro->cpf }}" class="cpf-mask @if($errors->has('cpf')) erro @endif">
                        <select name="profissao_cargo" @if($errors->has('profissao_cargo')) class="erro" @endif>
                            <option value="" selected>profissão / cargo (selecione)</option>
                            @foreach(\App\Models\Especialista::profissoes() as $profissao)
                                <option value="{{ $profissao }}" @if($registro->profissao_cargo == $profissao) selected @endif>{{ $profissao }}</option>
                            @endforeach
                        </select>
                        <input type="text" name="telefone" placeholder="telefone" value="{{ $registro->telefone }}" @if($errors->has('telefone')) class="erro" @endif>
                        <div class="cep-consulta @if($errors->has('cep')) erro @endif">
                            <input type="text" name="cep" placeholder="CEP" value="{{ $registro->cep }}" class="cep-mask">
                            <a href="#">CONSULTAR</a>
                        </div>
                        <input type="text" name="endereco" placeholder="endereço" value="{{ $registro->endereco }}" @if($errors->has('endereco')) class="erro" @endif>
                        <input type="text" name="numero" placeholder="número" value="{{ $registro->numero }}" @if($errors->has('numero')) class="erro" @endif>
                        <input type="text" name="complemento" placeholder="complemento" value="{{ $registro->complemento }}" @if($errors->has('complemento')) class="erro" @endif>
                        <input type="text" name="bairro" placeholder="bairro" value="{{ $registro->bairro }}" @if($errors->has('bairro')) class="erro" @endif>
                        <input type="text" name="cidade" placeholder="cidade" value="{{ $registro->cidade }}" @if($errors->has('cidade')) class="erro" @endif>
                        <input type="text" name="uf" placeholder="UF" maxlength="2" value="{{ $registro->uf }}" @if($errors->has('uf')) class="erro" @endif>
                        <input type="text" name="nome_exibicao" placeholder="nome para aparecer no site (Resposta do Especialista)" value="{{ $registro->nome_exibicao }}" @if($errors->has('nome_exibicao')) class="erro" @endif>
                        <input type="text" name="descricao" placeholder="descritivo (especialista em...)" value="{{ $registro->descricao }}" @if($errors->has('descricao')) class="erro" @endif>
                        @if($registro->imagem)
                        <img src="{{ asset('assets/img/especialistas/'.$registro->imagem) }}" alt="">
                        @else
                        <img src="{{ asset('assets/img/layout/especialista.png') }}" alt="">
                        @endif
                        <div class="foto-input @if($errors->has('imagem')) erro @endif">
                            <span>
                                <em>foto</em>
                                (tamanho mínimo: 170 x 170px)
                            </span>
                            <input type="file" name="imagem" id="imagem">
                            <label for="imagem">ALTERAR FOTO</label>
                        </div>
                        <span class="alterar-senha">alterar minha senha</span>
                        <input type="password" name="senha" placeholder="nova senha" @if($errors->has('senha')) class="erro" @endif>
                        <input type="password" name="senha_confirmation" placeholder="repetir senha" @if($errors->has('senha')) class="erro" @endif>
                    </div>

                    <div class="submit">
                        <input type="submit" value="ATUALIZAR DADOS">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
