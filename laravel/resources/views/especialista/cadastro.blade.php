@extends('frontend.common.template')

@section('content')

    <div class="especialistas-wrapper">
        <div class="center">
            <div class="cadastro-especialistas">
                <h2>
                    PRIMEIRO CADASTRO<br>
                    <small style="font-size: .5em">
                        MÉDICOS &middot;
                        ESPECIALISTAS
                    </small>
                </h2>

                <form action="{{ route('especialista.cadastrar') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    @if($errors->any())
                    <div class="aviso-erros">Preencha todos os campos corretamente.</div>
                    @endif
                    @if(session('erroCoord'))
                    <div class="aviso-erros">{{ session('erroCoord') }}</div>
                    @endif

                    <div class="col">
                        <input type="text" name="nome" placeholder="nome completo" value="{{ old('nome') }}" @if($errors->has('nome')) class="erro" @endif>
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" @if($errors->has('email')) class="erro" @endif>
                        <input type="cpf" name="cpf" placeholder="CPF" value="{{ old('cpf') }}" class="cpf-mask @if($errors->has('cpf')) erro @endif">
                        <select name="profissao_cargo" @if($errors->has('profissao_cargo')) class="erro" @endif>
                            <option value="" selected>profissão / cargo (selecione)</option>
                            @foreach(\App\Models\Especialista::profissoes() as $profissao)
                                <option value="{{ $profissao }}" @if(old('profissao_cargo') == $profissao) selected @endif>{{ $profissao }}</option>
                            @endforeach
                        </select>
                        <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}" @if($errors->has('telefone')) class="erro" @endif>
                        <div class="cep-consulta @if($errors->has('cep')) erro @endif">
                            <input type="text" name="cep" placeholder="CEP" value="{{ old('cep') }}" class="cep-mask">
                            <a href="#">CONSULTAR</a>
                        </div>
                        <input type="text" name="endereco" placeholder="endereço" value="{{ old('endereco') }}" @if($errors->has('endereco')) class="erro" @endif>
                        <input type="text" name="numero" placeholder="número" value="{{ old('numero') }}" @if($errors->has('numero')) class="erro" @endif>
                        <input type="text" name="complemento" placeholder="complemento" value="{{ old('complemento') }}" @if($errors->has('complemento')) class="erro" @endif>
                        <input type="text" name="bairro" placeholder="bairro" value="{{ old('bairro') }}" @if($errors->has('bairro')) class="erro" @endif>
                        <input type="text" name="cidade" placeholder="cidade" value="{{ old('cidade') }}" @if($errors->has('cidade')) class="erro" @endif>
                        <input type="text" name="uf" placeholder="UF" maxlength="2" value="{{ old('uf') }}" @if($errors->has('uf')) class="erro" @endif>
                        <input type="text" name="nome_exibicao" placeholder="nome para aparecer no site (Resposta do Especialista)" value="{{ old('nome_exibicao') }}" @if($errors->has('nome_exibicao')) class="erro" @endif>
                        <input type="text" name="descricao" placeholder="descritivo (especialista em...)" value="{{ old('descricao') }}" @if($errors->has('descricao')) class="erro" @endif>
                        <div class="foto-input @if($errors->has('imagem')) erro @endif">
                            <span>
                                <em>foto</em>
                                (tamanho mínimo: 170 x 170px)
                            </span>
                            <input type="file" name="imagem" id="imagem">
                            <label for="imagem">ENVIAR FOTO</label>
                        </div>
                        <input type="password" name="senha" placeholder="senha" @if($errors->has('senha')) class="erro" @endif>
                        <input type="password" name="senha_confirmation" placeholder="repetir senha" @if($errors->has('senha')) class="erro" @endif>
                    </div>

                    <div class="submit">
                        <input type="submit" value="CADASTRAR">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
