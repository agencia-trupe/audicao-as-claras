@extends('frontend.common.template')

@section('content')

    <div class="especialistas-wrapper">
        <div class="center">
            <h2>AVISOS</h2>

            @if(!count($avisos))
                <div class="nenhum-registro">No momento não temos nenhum aviso.</div>
            @else
            <div class="especialistas-avisos">
                <div class="grid-sizer"></div>
                <div class="gutter-sizer"></div>
                @foreach($avisos as $aviso)
                <div class="aviso">
                    <h3>{{ $aviso->titulo }}</h3>
                    <div class="texto">{!! $aviso->texto !!}</div>
                    @if($aviso->botao_link)
                    <a href="{{ $aviso->botao_link }}" target="_blank" class="btn">
                        {{ $aviso->botao_texto }}
                    </a>
                    @endif
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>

@endsection