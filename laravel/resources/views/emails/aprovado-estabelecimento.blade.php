<!DOCTYPE html>
<html>
<head>
    <title>Audição às Claras &middot; Cadastro Aprovado</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Seu cadastro foi aprovado com sucesso. Acesse o link abaixo para fazer login.<br><br><a href="{{ route('estabelecimento.login') }}">Login Especialista</a></span>
</body>
</html>
