<!DOCTYPE html>
<html>
<head>
    <title>Audição às Claras &middot; Redefinição de senha</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='color:#000;font-size:14px;font-family:Verdana;'>
        <a href="{{ url('estabelecimento/redefinicao-de-senha', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">Clique aqui para redefinir sua senha.</a>
    </span>
</body>
</html>
