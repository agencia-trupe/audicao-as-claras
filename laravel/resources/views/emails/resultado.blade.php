<!DOCTYPE html>
<html>
<head>
    <title>Resultados da consulta de aparelhos auditivos</title>
    <meta charset="utf-8">
</head>
<body>
    <p>De acordo com as necessidades apontadas:</p>
    <ul>
        @if($necessidades['conversar_com_uma_pessoa'])
            <li>Conversar com uma pessoa</li>
        @endif
        @if($necessidades['ambientes_internos'])
            <li>Ambientes internos</li>
        @endif
        @if($necessidades['conversar_em_grupo'])
            <li>Conversar em grupo</li>
        @endif
        @if($necessidades['falar_ao_telefone'])
            <li>Falar ao telefone</li>
        @endif
        @if($necessidades['assistir_tv'])
            <li>Assistir tv</li>
        @endif
        @if($necessidades['ambientes_ao_ar_livre'])
            <li>Ambientes ao ar livre</li>
        @endif
        @if($necessidades['showsconcertos_e_teatro'])
            <li>Shows, concertos e teatro</li>
        @endif
        @if($necessidades['restaurantes'])
            <li>Restaurantes</li>
        @endif
        @if($necessidades['festas_e_ambientes_muito_ruidosos'])
            <li>Festas e ambientes muito ruidosos</li>
        @endif
    </ul>
    <p>Os aparelhos indicados são:</p>
    <ul>
        @foreach($aparelhos as $a)
        <li>{{ $a->marca ? $a->marca->titulo.'<br>' : '' }}{{ $a->nome }}</li>
        @endforeach
    </ul>
</body>
</html>
