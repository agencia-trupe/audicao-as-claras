@extends('frontend.common.template')

@section('content')

    <div class="especialistas-wrapper">
        <div class="center">
            <div class="cadastro-especialistas">
                <h2>
                    PRIMEIRO CADASTRO<br>
                    <small style="font-size: .5em">
                        ESTABELECIMENTOS &middot;
                        FORNECEDORES
                    </small>
                </h2>

                <form action="{{ route('estabelecimento.cadastrar') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    @if($errors->any())
                    <div class="aviso-erros">Preencha todos os campos corretamente.</div>
                    @endif
                    @if(session('erroCoord'))
                    <div class="aviso-erros">{{ session('erroCoord') }}</div>
                    @endif

                    <div class="col">
                        <input type="text" name="nome" placeholder="nome fantasia" value="{{ old('nome') }}" @if($errors->has('nome')) class="erro" @endif>
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" @if($errors->has('email')) class="erro" @endif>
                        <input type="text" name="razao_social" placeholder="razão social" value="{{ old('razao_social') }}" @if($errors->has('razao_social')) class="erro" @endif>
                        <input type="cnpj" name="cnpj" placeholder="CNPJ" value="{{ old('cnpj') }}" class="cnpj-mask @if($errors->has('cnpj')) erro @endif">
                        <input type="text" name="area_de_atuacao" placeholder="área de atuação" value="{{ old('area_de_atuacao') }}" @if($errors->has('area_de_atuacao')) class="erro" @endif>
                        <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}" @if($errors->has('telefone')) class="erro" @endif>
                        <input type="text" name="responsavel" placeholder="nome do responsável" value="{{ old('responsavel') }}" @if($errors->has('responsavel')) class="erro" @endif>
                        <div class="cep-consulta @if($errors->has('cep')) erro @endif">
                            <input type="text" name="cep" placeholder="CEP" value="{{ old('cep') }}" class="cep-mask">
                            <a href="#">CONSULTAR</a>
                        </div>
                        <input type="text" name="endereco" placeholder="endereço" value="{{ old('endereco') }}" @if($errors->has('endereco')) class="erro" @endif>
                        <input type="text" name="numero" placeholder="número" value="{{ old('numero') }}" @if($errors->has('numero')) class="erro" @endif>
                        <input type="text" name="complemento" placeholder="complemento" value="{{ old('complemento') }}" @if($errors->has('complemento')) class="erro" @endif>
                        <input type="text" name="bairro" placeholder="bairro" value="{{ old('bairro') }}" @if($errors->has('bairro')) class="erro" @endif>
                        <input type="text" name="cidade" placeholder="cidade" value="{{ old('cidade') }}" @if($errors->has('cidade')) class="erro" @endif>
                        <input type="text" name="uf" placeholder="UF" maxlength="2" value="{{ old('uf') }}" @if($errors->has('uf')) class="erro" @endif>
                        <input type="text" name="website" placeholder="website" value="{{ old('website') }}" @if($errors->has('website')) class="erro" @endif>
                        <input type="password" name="senha" placeholder="senha" @if($errors->has('senha')) class="erro" @endif>
                        <input type="password" name="senha_confirmation" placeholder="repetir senha" @if($errors->has('senha')) class="erro" @endif>
                    </div>

                    <div class="marcas">
                        <h4>marcas com que trabalha:</h4>
                        <div class="marcas-box">
                            @foreach($marcas as $marca)
                            <label>
                                <input type="checkbox" name="marcas[]" value="{{ $marca->id }}" @if(count(old('marcas')) && in_array($marca->id, old('marcas'))) checked @endif>
                                <span>{{ $marca->titulo }}</span>
                            </label>
                            @endforeach
                        </div>
                    </div>

                    <div class="submit">
                        <input type="submit" value="CADASTRAR">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
