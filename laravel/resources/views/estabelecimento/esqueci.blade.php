@extends('frontend.common.template')

@section('content')

    <div class="especialistas-wrapper">
        <div class="center">
            <div class="lightbox-forms login-especialistas">
                <h2>ESQUECI MINHA SENHA</h2>

                @if(session('sent'))
                    <form action="">
                        <div class="lightbox-response">{{ session('sent') }}</div>
                    </form>
                @else
                <form action="{{ route('estabelecimento.redefinicao') }}" method="POST">
                    {!! csrf_field() !!}
                    @if($errors->any())
                    <div class="lightbox-response">{{ $errors->first() }}</div>
                    @endif
                    @if(session('error'))
                    <div class="lightbox-response">{{ session('error') }}</div>
                    @endif
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="submit" value="REDEFINIR SENHA">
                </form>
                @endif
            </div>
        </div>
    </div>

@endsection
